<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([
            'balance' => '100',
            'verify_status' => '1',
            'email' => 'admin@admin.com',
        ]);
    }
}
