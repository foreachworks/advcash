<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->smallInteger('type_operation')->default(0);
            $table->smallInteger('input');
            $table->string('number_purse');
            $table->double('amount');
            $table->smallInteger('status');
            $table->string('from_email', 255)->default('roboglobal777@gmail.com');
            $table->string('transaction_id', 255)->nullable()->default(null);
            $table->timestamp('date_done');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
