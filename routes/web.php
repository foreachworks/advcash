<?php

Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::group(['prefix' => 'dashboard', 'middleware' => 'auth'], function() {
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::resource('transactions', 'PaymentController');
    Route::get('/account', 'DashboardController@editAccount')->name('edit.account');
    Route::post('/update', 'DashboardController@accountUpdate')->name('account.update');
});

//Account
Route::group(['prefix' => 'pages', 'middleware' => 'auth'], function() {
    Route::get('/deposit-funds', 'HomeController@refill')->name('refill');
    Route::get('/transfer/wallet', 'HomeController@transaction')->name('transaction');
    Route::get('/transaction', 'HomeController@historyTransactions')->name('history.transactions');
});
    Route::post('/show_more', 'HomeController@show_more');
