var History = {
	Advanced: {
		obj: false,
		Toggle: function() {
			$('.p-history-advanced-body').toggle();
		},
		Select: function(e) {
			this.obj = $(e).parents('.p-history-advanced-select');
			this.obj.addClass('this')
							.find('dd').toggle()
							.width($(e).parent('dt').width());
			$('.p-history-advanced-select:not(.this) dd').hide();
			$('.p-history-advanced-select').removeClass('this');
			$(document).on('click.history', function(t) {
				if (!$(t.target).parents('.p-history-advanced-select').length && !$(t.target).is('.p-history-advanced-select')) {
					this.obj = false;
					$('.p-history-advanced-select dd').hide();
					$(document).off('click.history');
				}
			});
		},
		Set: function(title, value, type) {
			var $e = this.obj.find('dt a');
			$e.text(title);
			// some operations with 'value' and 'type'...
			this.obj = false;
			$('.p-history-advanced-select dd').hide();
			if (value == 7 && type == 'period') {
				//$('.p-history-advanced-dates').show();
			  	$('.p-history-advanced-dates input:first').focus();
			} /*else if (value != 7 && type == 'period') {
				$('.p-history-advanced-dates').hide();
			}*/
		}
	}
};

function HighLight(el, color, el2, color2) {
	$('.block-operations > li').css('background', 'none');
	if (color == 'red') {
		$('.' + el).addClass('wallet-red');
	} else if (color == 'green') {
		$('.' + el).addClass('wallet-green');
	}
	if (el2 && color2) {
		if (color2 == 'red') {
			$('.' + el2).addClass('wallet-red');
		} else if (color2 == 'green') {
			$('.' + el2).addClass('wallet-green');
		}
	}
}

function txDatepicker() {
	if ($('.daterange').length) {
		jQuery(function($) {
			if (language == 'Russian') {
				$.datepicker.regional['ru'] = {
					closeText : 'Закрыть',
					prevText : 'Пред',
					nextText : 'След',
					currentText : 'Сегодня',
					monthNames : [ 'Январь', 'Февраль', 'Март', 'Апрель', 'Май',
							'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь',
							'Декабрь' ],
					monthNamesShort : [ 'Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
							'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек' ],
					dayNames : [ 'воскресенье', 'понедельник', 'вторник', 'среда',
							'четверг', 'пятница', 'суббота' ],
					dayNamesShort : [ 'вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт' ],
					dayNamesMin : [ 'Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб' ],
					weekHeader : 'Нед',
					dateFormat : 'dd.mm.yy',
					firstDay : 1,
					isRTL : false,
					showMonthAfterYear : false,
					yearSuffix : ''
				};
				$.datepicker.setDefaults($.datepicker.regional['ru']);
				$.timepicker.regional['ru'] = {
					timeText : 'Время',
					hourText : 'Часы',
					minuteText : 'Минуты',
					currentText : 'Сегодня'
				};
				$.timepicker.setDefaults($.timepicker.regional['ru']);
			}
		});
		$(function() {
			$('.daterange input:first').datetimepicker({
				showButtonPanel: false,
				defaultDate: null,
				maxDate: '0',
				numberOfMonths : 1,
				onSelect : function(selectedDate) {
					$('.daterange input:last').datetimepicker("option",
						"minDate", selectedDate);
				}
			});
			$('.daterange input:last').datetimepicker({
				showButtonPanel: false,
				defaultDate: null,
				maxDate: '0',
				numberOfMonths : 1,
				onSelect : function(selectedDate) {
					$('.daterange input:first').datetimepicker("option",
						"maxDate", selectedDate);
				}
			});
		});
	}
}

function txStatus() {
	if ($('.p-history-advanced-statuses input:first').attr('checked')) {
		$('.p-history-advanced-statuses input:not(:first,.status-new)').attr('checked', true).attr('disabled', true);
	}
	$('.p-history-advanced-statuses input:first').change(function() {
		if ($(this).attr('checked')) {
			$('.p-history-advanced-statuses input:not(:first,.status-new)').attr('checked', true).attr('disabled', true);
		} else {
			$('.p-history-advanced-statuses input:not(:first,.status-new)').attr('disabled', false);
		}
	});
}

var infoRed;
function errorUpdate() {
	if (infoRed != $('.info-red').html()) {
		submitForm();
		$('.popup-body').show();
		infoRed = $('.info-red').html();
	}
}

function startSearch() {
	$('#txListDataTable').hide();
	$('#cardTxListTable').hide();
	$('.p-main-loadtransactions').hide();
	$('#nothing').hide();
	$('.p-history-searching').show();
	$('.p-history-nothing').hide();
}

function endSearch() {
	$('.p-history-searching').hide();
	$('#txListDataTable').show();
	$('#cardTxListTable').show();
	$('.p-main-loadtransactions').show();
	$('#nothing').show();
}

function printTx() {
	if ($.browser.webkit) { // chrome
		window.frames['print_frame'].window.document.body.innerHTML = '';
		var tempDivDataTable = $('#txListDataTable').clone().wrap('<div></div>').parent();
		var tempDivHeader = $('.p-inside-transactions-header').clone().wrap('<div></div>').parent();
		var div = document.createElement("div");
		div.className += 'p-main-transactions tab';
		div.innerHTML = tempDivHeader.html() + tempDivDataTable.html();
		window.frames['print_frame'].window.document.body.appendChild(div);
	    window.frames['print_frame'].window.focus();
	    window.frames['print_frame'].window.print();
	} else {
		window.print();
	}
}

function showGlobalError() {
	showNotyError($('#globalMessages li').text());
}

function Read(target, txId) {
	if ($('tr').has(target).hasClass('unread')) {
		if (jQuery.browser.webkit) {
			$('tr').has(target).children('td').css('background', '#f5f5f5');
		}
		readTx(txId);
	}
}

function animateNewTx() {
	var table = $('.p-main').offset().top;
	var body = (window.opera) ? (document.compatMode=="CSS1Compat" ? $('html') : $('body')) : $('html,body');
	body.animate({scrollTop: table}, 300);
}

function transfer(event, el, id, value) {
	if (event.keyCode == 13) {
		$(el).blur();
		makeTransfer(id, value);
		return false;
    }
}

$(document).ready(function() {
	txDatepicker();
	txStatus();
	infoRed = $('.info-red').html();
	setInterval(errorUpdate, 200);
	$(document).bind('click', function(e) {
    	if (!$(e.target).parents('.select-input dt').length 
    			&& !$(e.target).parents('.select-input dd').length) {
            $('.select-input dd').hide();
		}
    });
});