var bugPercentStep = 0;
var bugMinTime;
var bugMaxTime;
var bugTimeoutProgressStep;
var bugFilesUploaded = false;
var bugFileSize = '';
var bugFileTypeError = false;
var bugFileSizeError = false;

$(document).ready(function() {
    // renderAttachmentsBugPanel();
    bugMaxlength();
});

Array.prototype.clean = function(deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

function bugMaxlength() {
    $('#bugComments').attr('maxlength', '500');
}

function bugShortText(text,number){
    var _text =  text;
    var newFileName = _text.replace(/\s/g, "\u17fd").split("\u17fd").clean("").join(" ");
    var fileNameArray = newFileName.split('.');
    var extention = fileNameArray[fileNameArray.length-1];
    var fileNameWithoutExt =  fileNameArray[0];
    if(fileNameWithoutExt.length > number){
        newFileName = fileNameWithoutExt.substr(0,number-3) + "... ."+extention;
    }
    return newFileName;
}

function ajaxFileUploadBug(id) {
    var isHtml5 = false;
    if (window.FormData) {
        isHtml5 = true;
    }
    $('#bugfile .p-bug-verify-item-desc .error-size').hide();
    $('#bugfile .p-bug-verify-item-desc .error-extension').hide();
    var fileName = (isHtml5) ? getFileName($('#' + id).get(0)) : getFileNameFromInputFileValue($('#' + id).val());
    if (checkFileType(fileName)) {
        var size = getSizeBug($('#' + id).get(0));
        var sizeMegaBytes = size / (1024 * 1024);
        fileSize = '';
        if (size > 8388608) { // 8 MB ~ 8388608 bytes
            bugFileSizeError = true;
            $('#bugfile .p-bug-verify-item-desc .error-size').show();
            renderAttachmentsBugPanel();
            return false;
        } else {
            bugFilesUploaded = true;
            $('#fileBugUpload').hide();

            $('#bugfile .p-bug-verify-item-progress').show();
            var newFileName = bugShortText(fileName,47);
            $('#bugfile .p-bug-verify-item-progress dt span:first-child').text(newFileName);
            if (size != 0) {
                if (size > 1024 * 1024) {
                    fileSize = (Math.round(size * 100 / (1024 * 1024)) / 100).toString() + ' MB';
                } else {
                    fileSize = (Math.round(size * 100 / 1024) / 100).toString() + ' KB';
                }
                $('#bugfile .p-bug-verify-item-progress dt span:last-child').text(fileSize);
            }
            var time = sizeMegaBytes;
            if (time != 0) {
                bugMinTime = time;
                bugMaxTime = time * 2;
            } else {
                bugMinTime = getRandomArbitaryBug(1, 2, 1);
                bugMaxTime = bugMinTime * 2;
            }
            bugTimeoutProgressStep = setTimeout(updateProgressStepForBug, getRandomArbitaryBug(bugMinTime, bugMaxTime, 100));
        }
    } else {
        bugFileTypeError = true;
        $('#bugfile .p-bug-verify-item-desc .error-extension').show();
        renderAttachmentsBugPanel();
        return false;
    }
    $.ajaxFileUpload({
        url : uploadUrl,
        secureuri : false,
        fileElementId : id,
        dataType : 'json',
        data : {
            is_ticket : 'true',
            name : 'logan',
            id : 'id',
            accept : 'jpg,jpeg,gif,png,pdf'
        },
        success : function(data, status) {
            if (data != null) {
                if (typeof (data.error) != 'undefined') {
                    if (data.error != '') {
                        alert('e:' + data.error);
                    } else {
                        alert('s: ' + data.msg);
                    }
                }
            }
        },
        error : function(data, status, e) {
            alert('#{msg.uploadFileError}');
        }
    });

    function updateProgressStepForBug() {
        $('#bugfile .p-bug-verify-item-progress .bar i').css('width',
            ++bugPercentStep + '%');
        if (bugPercentStep != 100) {
            bugTimeoutProgressStep = setTimeout(updateProgressStepForBug, getRandomArbitaryBug(bugMinTime, bugMaxTime, 100));
        } else {
            renderAttachmentsBugPanel();
            bugPercentStep = 0;
            //$('#file1 .p-profile-verify-item-progress').hide();
            var newFileName = bugShortText(fileName,47);
            $('#bugfile .ok b').text(newFileName);
            $('#bugfile .ok').show();
        }
    }

    return false;
}

function refreshBugAttachment() {
    if (bugFileSizeError) {
        $('#bugfile .p-bug-verify-item-desc .error-size').show();
        bugFileSizeError = false;
    }
    if (bugFileTypeError) {
        $('#bugfile .p-bug-verify-item-desc .error-extension').show();
        bugFileTypeError = false;
    }
    if (bugFileSize != '') {
        $('#bugfile .p-bug-verify-item-progress dt span:last-child').text(bugFileSize);
    }
    bugMaxlength();
}

function getSizeBug(el) {
    if (jQuery.browser.msie) {
        return 0;
    } else {
        try {
            return el.files[0].size;
        } catch (Exception) {
            return 0;
        }
    }
}

function getRandomArbitaryBug(min, max, weight) {
    return (Math.random() * (max - min) + min) * weight;
}

function removeUploadedFileBug() {
    bugFilesUploaded = false;

    clearTimeout(bugTimeoutProgressStep);
    bugPercentStep = 0;
    $('#bugfile .p-bug-verify-item-progress').hide();
    $('#bugfile .p-bug-verify-item-desc .ok').hide();
    $('#bugfile .p-bug-verify-item-progress .bar i').css('width', '0%');
    $('#fileBugUpload').show();
}



