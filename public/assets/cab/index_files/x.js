$(document).on('ready', function () {
  tippy('[data-tippy-template]', {
    appendTo: function () {
      return document.body
    },
    content(reference) {
      var id = reference.getAttribute('data-tippy-template');
      var template = document.getElementById(id);

      return template.innerHTML;
    },
    theme: 'interactive',
    trigger: 'click',
    allowHTML: true,
    maxWidth: 404,
    // showOnCreate: true,
  });

  tippy('[data-tippy-content]', {
    appendTo: function () {
      return document.body
    },
    onShow: function (instance) {
      instance.reference.classList.add('-active');
    },
    onHide: function (instance) {
      instance.reference.classList.remove('-active');
    },
    theme: 'default',
    trigger: 'click',
    maxWidth: 224,
    // showOnCreate: true,
  });

  $('.x-card-ref__toggler').on('click', function () {
    $('.x-card-ref').toggleClass('-is-open');
  });

  $('.x-big-tabs__tab[data-for]').on('click', function (e) {
    var button = e.currentTarget;
    var tabId = button.dataset.for;

    $([button, document.getElementById(tabId)])
      .addClass('-active')
      .siblings('.-active')
      .removeClass('-active');
  });

  $('.x-header__profile-handle').on('click', function () {
    $(this).next().toggleClass('-open');
  });

  $(window).on('click', function (e) {
    if ($(e.target).closest('.x-header__profile-handle').length || $(e.target).closest('.x-header__profile-dropdown').length) {
      return;
    }

    $('.x-header__profile-dropdown').removeClass('-open');
  });
});