function openSelectList(el){
	if(el.classList.contains('chzn-container-active')) {
		el.classList.remove('chzn-container-active');
		el.querySelector('.chzn-drop').style.left = `-9000px`
	}
	else {
		el.classList.add('chzn-container-active');
		el.querySelector('.chzn-drop').style.left = `0`
	}
	document.querySelectorAll(".chzn-results li").forEach(item => {
		item.addEventListener('click', function(el){

			document.querySelector('#_dfcntr_chzn .chzn-single').innerText = el.target.innerText;
		})
	})
}
function selectOff(el){
	el.classList.remove('chzn-container-active');
	el.querySelector('.chzn-drop').style.left = `-9000px`
}

function chooseTab(el){
	el.closest('ul').querySelectorAll('li').forEach(item => {
		item.classList.remove('active')
	})
	el.classList.add('active');
	var id = el.getAttribute('data-id');
	activeTab(id)
}
function activeTab(id){
	document.querySelectorAll('form#newsForm, form#txForm').forEach(item => {
		item.classList.remove('active')
	})
	document.querySelector(`#${id}`).classList.add('active')
}




$(document).ready(function() {

	showTx();
	// showMoreTransactions();
    // loadPaymentOrders();

    function callBack(response) {
    	if (response.status == 200) {
    		var message = response.responseBody;
    		if ('wallet' == message) {

    			setTimeout(updateWallet, 2000);
    			setTimeout(updateMonthLimit, 2000);

    			if ($('#txRowCount') != undefined && $('#txRowCount').val() == 0) {
    				window.location.reload(true);
    			} else {
    				setTimeout(updateTx, 2000);
    			}
    		} else if ('transactions' == message) {
    			if ($('#txRowCount') != undefined && $('#txRowCount').val() == 0) {
    				window.location.reload(true);
    			} else {
    				setTimeout(updateTx, 2000);
    			}
    		} else if ('news' == message) {
    			setTimeout(updateNews, 2000);
    		}
    	}
    }
    
    /*window.setTimeout(function() {
		$.atmosphere.subscribe(contextPath + '/pubsub/' + userId,
        	callBack, { transport: 'long-polling' });
        }, 1000);*/
    });

function checkCurrentTab() {
	if (curTab == '.p-main-news') {
		MainTab('.p-main-news', $('#newsLink')); $('#txFilterForm').hide();
	}
}

function showPaymentOrders() {
	if ($('#payment-orders-cache-table tbody').length) {
		$('#paymentOrdersDataTable').find('tr:last-child').after($('#payment-orders-cache-table tbody').html());
		$('#payment-orders-show-more').html($('#payment-orders-cache-show-more').html());
		$('.payment-orders').show();
	} else {
		$('.payment-orders').hide();
	}
}



// document.querySelector(".chzn-results").addEventListener('click', function(el){
// 	alert()
// })

