function initializeTooltipsForWithdrawalSection() {
    addTooltipToInactivePayMethods($(".p-main-methods dl.inactive"), 'right');
    addTooltipToInactivePayMethods($(".combobox-account-list li span.inactive"), 'right');
    addTooltipToInactivePayMethods($(".combobox-card-list li span.inactive"), 'right');
    addTooltipToInactivePayMethods($("table.p-transfer-nav tr td.inactive"), 'bottom');
}


function initializeTooltipsForDepositSection() {
    addTooltipToInactivePayMethods($(".p-input-tabs li.inactive"), 'right');
	// addTooltipToInactivePayMethods($(".p-input-list table tbody
	// tr.inactive"), 'top');
	// addTooltipToInactivePayMethods($(".p-input-method ul.p-input-method-nav
	// li.inactive"), 'bottom');
	addTooltipToInactivePayMethods($(".p2-payment label.inactive"), 'top');
	addTooltipToInactivePayMethods($(".p2-payment tr.inactive, .p2-payment tr.only-for-verified"), 'top');
}

function addTooltipToInactivePayMethods(element, position) {
    if (window.jQuery) {
        switch (position) {
            case 'top':
                $(element).each(function() {
                    if ($(this).offset().top - $(window).scrollTop()  <= 150) {
                        generateBottomTooltip($(this));
                    } else {
                        generateTopTooltip($(this));
                    }
                });
                break;
            case 'bottom':
                $(element).each(function() {
                    if ($(window).scrollTop() + $(window).height() - $(this).offset().top - $(this).outerHeight()  <= 150) {
                        generateTopTooltip($(this));
                    } else {
                        generateBottomTooltip($(this));
                    }
                });
                break;
            case 'left':
                $(element).each(function() {
                    if ($(this).offset().right <= 210) {
                        generateRightTooltip($(this));
                    } else {
                        generateLeftTooltip($(this));
                    }
                });
                break;
            case 'right':
                $(element).each(function() {
                    if ($(window).width() - ($(this).offset().left + $(this).outerWidth()) <= 210) {
                        generateLeftTooltip($(this));
                    } else {
                        generateRightTooltip($(this));
                    }
                });
                break;
        }
    } else {
        setTimeout(addTooltipToInactivePayMethods(element, position), 1000);
    }
}

function generateRightTooltip(element) {
    $(element).tooltip({
        position: { my: 'left center', at: 'right+10 center'},
        tooltipClass: "payment-method-unavailable right"
    });
}

function generateLeftTooltip(element) {
    $(element).tooltip({
        position: { my: 'right center', at: 'left-10 center'},
        tooltipClass: "payment-method-unavailable left"
    });
}

function generateTopTooltip(element) {
    $(element).tooltip({
        position: {my: 'center bottom', at: 'center top-10'},
        tooltipClass: "payment-method-unavailable top"
    });
}

function generateBottomTooltip(element) {
    $(element).tooltip({
        position: {my: 'center top', at: 'center bottom+10'},
        tooltipClass: "payment-method-unavailable bottom"
    });
}
