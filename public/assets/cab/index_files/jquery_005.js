(function( $ ){
	
	// Фикс для псевдокласса "contains" для обработки регистра символов
	
	jQuery.expr[':'].Contains = function(a, i, m) {
		return jQuery(a).text().toUpperCase()
			.indexOf(m[3].toUpperCase()) >= 0;
	};
	jQuery.expr[':'].contains = function(a, i, m) {
		return jQuery(a).text().toUpperCase()
			.indexOf(m[3].toUpperCase()) >= 0;
	};

	var methods = {

		// Метод по умлочанию
		
		init : function( options ) {
	
			settings = {
				//combobox					:	($(this).data().combobox == 'true') ? true : false,
				comboboxError			: 'Нет результатов по такому запросу',
				//transmitFlag			:	'false',
				flagPath					:	'images/flags/'
			}
	
			if ( options ) {
				$.extend( settings, options );
			}
			
			return this.each(function(){
				
				var $this = $(this);
	
				$this.hide();
				
				// Применяем тип селекта (комбобокс или обычный)
				// и добавляем классы от селекта к эмулятору
				selectInput = (settings.combobox = true) ? '<input type="text" />' : selectInput = '';
				selectClass = $this.attr('class').replace(/(^|\s+)custom(?=\s+|$)/g, '');
				selectClass = $this.attr('disabled') == 'disabled' ? selectClass+' disabled' : selectClass;
				var $list = $this.attr('disabled') == 'disabled' ? '' : '<div class="list"><ul></ul></div>';
				$('<div class="combo '+selectClass+'"><div class="combo-header">'+selectInput+'</div>'+$list+'</div>').insertAfter(this);
				
				// Присваиваем данные конкретному селекту
				$this.data({
					index			: $this.index('select.custom'), // Индекс элемента среди кастомных селектов
					id				: 'sel-'+$this.index('select.custom'), // id на основе индекса
					textValue	: $this.find('option[value="'+$this.val()+'"]').html(), // html выбранной опции
					value			: $this.find('option[value="'+$this.val()+'"]').val() // Значение выбранной опции
					//classes		: $this.attr('class').replace(/(^|\s+)custom(?=\s+|$)/g, ''),
				});
				
				var $data = $this.data();
				
/*				settings.transmitFlag  = (/\bflag\b/.test($data.ides)) ? true : false;
				settings.combobox			 = (/\bcombobox\b/.test($data.ides)) ? true : false;
				settings.comboboxError = ($this.attr('error')) ? $(this).attr('error') : settings.comboboxError;*/
				
	//console.log($this.data().combobox)
				
				$this.attr('index','sel-'+$data.index); // Добавляем селекту уникальный атрибут index
				$this.next('.combo').attr('id', $data.id); // Добавляем эмулятору атрибут id, равный индекса селекта
				
				// Определяем ширину селекта в CSS,
				// применяем её для эмулятора,
				// подгоняем ширину выпадушки к ширине заголовка
				$this.next('.combo').css('width', $this.css('width'));
				var emulWidth= $this.next('.combo').outerWidth(),
						emulRealWidth = $this.next('.combo').find('.list').outerWidth(),
						emulCssWidth	= $this.next('.combo').find('.list').width(),
						emulWidthDiff	= emulRealWidth - emulCssWidth;
				$this.next('.combo').find('.list').width(emulWidth-emulWidthDiff);
				
				// Вызываем соответствующие методы для обработки действий пользователя по умлочанию
				
				$this.customSelect('reload');
				
				$('#'+$data.id+' .combo-header').mousedown(function(e){
					$(this).customSelect('clickHeader', e);
				});
				
				$('input', '#'+$data.id).keyup(function(){
					$(this).customSelect('changeInput');
				});
				
				//console.log();
				
			});
			
		},
		
		// Метод (пере)загрузки эмулятора

		reload : function() {

			var $data = this.data(),
					optionItem = '',
					flag  = ($data.flag == true) ? '<img src="'+settings.flagPath+$data.value+'.png" />' : '';
			
			// Изменяем данные "textValue" для текущего селекта
			this.data('textValue', this.find('option[value="'+this.val()+'"]').html());
			
			// В зависимости от настроек, определяем содержимое заголовка эмулятора
			if ($data.combobox == true)
			  $('input', '#'+$data.id).val($data.textValue)
			else
			  $('.combo-header', '#'+$data.id).html(flag+$data.textValue);
			
			// Парсим каждый option выбранного селекта, создаём соответствующие пункты в эмуляторе
			this.find('option').each(function(){

				var OptionValue = $(this).val(),
						OptionHtml  = $(this).html();
						
				italic = (OptionHtml.indexOf('<i>')>0) ? ' style="font-style: italic;"' : '';
				if ($(this).parent().attr('id') == 'secretQuestion' && $(this).val() == -1) {
					italic = ' style="font-style: italic;"';
				}
				flag = ($data.flag == true) ? ' style="background-image: url('+settings.flagPath+OptionValue+'.png)"' : '';
				optionItem += '<li'+flag+italic+'>'+OptionHtml+'</li>';
				
			});
			
			// Применяем созданные данные к эмулятору
			$('ul', '#'+$data.id).html(optionItem);
			
			// Создаём событие onChange для селекта
			this.change();
				
			// Вешаем обработчик клика по опции в эмуляторе
			/*$('li', '#'+$data.id).click(function(e){
				$(this).customSelect('clickOption', e);
			});*/
			$('li', '#'+$data.id).mouseup(function(e){
				$(this).customSelect('clickOption', e);
			});
		
		},
		
		// Метод обработки клика по заголовку эмулятора
		
		clickHeader : function(e) {
			
			if ( $('.combo .list').is(':visible') ) {
				
				var $data = $('.combo .list:visible').parents('.combo').prev('select').data(),
					  $list = $('.list', '#'+$data.id);
				
				//processListClose();
				
				
			}
			
			// Прячем все открытые списки
			//$('.combo .list').hide();
			
			var $data = this.parent().prev('select').data(),
					$list = $('.list', '#'+$data.id);
			
			// Избавляемся от жирного текста
			$('li', '#'+$data.id).each(function(){
				$(this).html($(this).text());
			});
			
			//console.log($data);
			

			/*
			if ( $(e.target).attr('class') == 'combo-header') {
				if ( $(e.target).parents('.combo').prev('select').data().test != 'ok' ) {
					$(e.target).parents('.combo').find('.list').show();
				} else {
					$(e.target).parents('.combo').find('.list').hide();
					$(e.target).parents('.combo').prev('select').data('test','ok');
					
				}
			} else {
				$list.toggle();
			}*/
				$list.toggle();
				
			/*
			if ( $(e.target).attr('class') != 'combo-header' ){
				if ($(e.target).parents('.combo').find('.list').is(':hidden')  ) {
				  $(e.target).parents('.combo').find('.list').show();
					alert('h');
				} else {
					alert('d');
				}
	  	}
			else {
			if ($list.is(':hidden')) {
				  $list.show();
				}
				else {
					$list.hide();
				}
			}*/
/*			if ( $(e.target).attr('class') == 'combo-header' && $list.is(':hidden') ) {
				$list.show();
			}
			if ( $(e.target).attr('class') == 'combo-header' && $list.is(':visible') ) {
				$list.hide();
			}*/
/*			else if ( $(e.target).attr('class') != 'combo-header' && $list.is(':visible') ){
				return 0;
			}
			else {
				$list.toggle();
			}*/
			
			
			// Обрабатываем закрытие комбобокса			
/*			if ($data.combobox == true) {
				var text = $('input', '#'+$data.id).val();
				if (text == '')
					$('li', '#'+$data.id).show();
				else
					$('li', '#'+$data.id).hide().filter(':contains('+text+')').show();
			}*/
				
		},
		
		// Метод обработки клика по значению в эмуляторе
		
		clickOption : function(e) {

			var $data = $(e.target).parents('.combo').prev('select').data(),
					thisIndex = $(e.target).index(),
					text = $(e.target).text();
			
			//console.log(thisIndex);
			
			// Передаём значение в основной селект,
			// создаём событие onchange,
			// Записываем значение в переменную
			var selectValue = $('select[index='+$data.id+'] option:eq('+thisIndex+')')
													.attr('selected','selected')
													//.change()
													.val();
			
			$('#countrySelect[index='+$data.id+'] option:eq('+thisIndex+')');
			$('#countrySelect[index='+$data.id+']').change();

			// Если это комбобокс, внутрь input вставляем текст-значение. Если нет - html-код в заголовок эмулятора
			if ($data.combobox == true)
				$('input', '#'+$data.id).val(text);
			else if ($data.flag == true)
				$('.combo-header', '#'+$data.id).html('<img src="'+settings.flagPath+selectValue+'.png" />'+text);
			else
				$('.combo-header', '#'+$data.id).html(text);
			// Переключаем состояние эмулятора
			$('li', '#'+$data.id).show();
			$('.list', '#'+$data.id).toggle();
			
			if ($('.p-reg-rightcol').length != 0) $('.p-reg-form-item-tip').hide();
			
			if ($(this).parents('.myask').length && selectValue == -1) $('.myask').toggle();
					
		},
		
		// Метод обработки изменения инпута в комбобоксе
		
		changeInput : function() {
			
			var $data = this.parents('.combo').prev('select').data(),
					text = $(this).val();
			
			if (text != '') {
				$('li', '#'+$data.id)
					.hide()
					.filter(':contains('+text+')')
					.show()
					.each(function(){
						var str = $(this).text();
						var tpl = new RegExp('(\w*)('+text+')(\w*)', 'i');
						$(this).html(str.replace(tpl, "$1<b>$2</b>$3"));
				});
				
				if ($('li:visible, .combo-nothing', '#'+$data.id).size() == 0)
					$('.list', '#'+$data.id).prepend('<div class="combo-nothing">'+settings.comboboxError+'</div>');
				else if ($('li:visible', '#'+$data.id).size() == 0 && $('.combo-nothing', '#'+$data.id).size() != 0)
					return 0;
				else
					$('.combo-nothing').remove();
					
			} else {
				
				$('li', '#'+$data.id).show().each(function(){
					var str = $(this).text();
					$(this).html(str);
				});
				$('.combo-nothing').remove();
			}
					
		}
		
	};
	
	
	// Обрабатываем клик по документу на предмет закрытия открытых эмуляторов
	$(document).bind('mousedown', function(e){
		
		if ( $(e.target).parents('.combo').length ) {
			$data = $(e.target).parents('.combo').prev('select').data();
			$list = $('.list', '#'+$data.id);
		} 
		
		if( !$(e.target).parents('.combo').length && $('.combo .list').is(':visible') ) {
			
			processListClose();
			
			// Прячем список, включаем видимость элементов внутри него для последующего открытия
			$list.hide();
			$('li', '#'+$data.id).show();
			
		}
		
	});
	
	//$(document).bind('keypress', shortcuts);
	
	// Функция обработки закрытия эмулятора извне
	function processListClose() {
		
		// Если это комбобокс, в списке есть хотя бы одна опция и в поле что-то введено,
		// при закрытии выбираем первую опцию из списке
		if ($data.combobox == true && $('li:visible', $list).size() > 0 && $('input', $list).val() != '') {
			$('li:visible:first', $list).click();
		}
		
		// Иначе - очищаем инпут
		else if ($data.combobox == true && $('li:visible', '#'+$data.id).size() == 0) {
			$('.combo-nothing').remove();
			$('input', '#'+$data.id).val('');
		}
		
		if ($('.p-reg-rightcol').length != 0) $('.p-reg-form-item-tip').hide();
	}
	
/*	function shortcuts (e) {
		if (e.keyCode == 13) {
	  	e.preventDefault();
			if ( $(e.target).is('input') ) {
				processListClose();
				$(e.target).blur();
			};
		}
		if (e.keyCode == 9) {
	  	e.preventDefault();
			if ( $(e.target).is('input') ) {
				processListClose();
				$(e.target).blur();
			};
		}
	}*/


	$.fn.customSelect = function( method ) {

		if ( methods[method] ) {
			return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
		} else if ( typeof method === 'object' || ! method ) {
			return methods.init.apply( this, arguments );
		}   

	};

})( jQuery );
