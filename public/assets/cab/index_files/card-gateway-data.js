function renderCarDataOutcome(e) {
    $('.popup-body .p-card-created-data dl.card-error').remove();
    if (e != null) {
        var data = $.parseJSON(e);
        if (data != null) {
            if (data.pan != null) {
                var dd = $('.popup-body .p-card-created-data .card-info dd');
                dd[0].innerHTML = data.pan.match(/.{1,4}/g).join(' ');
                dd[1].innerHTML = data.expiryDate.split("-")[0] + "-" + data.expiryDate.split("-")[1];
                dd[2].innerHTML = data.cvv;
                $('.popup-body .p-card-created-data .card-info').show();

            } else if (data.pin != null) {
                var dd = $('.popup-body .p-card-created-data .card-pin dd');
                dd[0].innerHTML = data.pin;
                $('.popup-body .p-card-created-data .card-pin').show();
            } else {
                $('.popup-body .p-card-created-data')
                    .append('<dl class="card-error"><dt></dt><dd>No data available</dd></dl>');
            }
        }
    } else {
        $('.popup-body .p-card-created-data')
            .append('<dl class="card-error"><dt></dt><dd>Error: Empty response</dd></dl>');
    }

    if (timer == undefined) {
        timer = setTimeout(hideCardInfoGlobal,
            timeShowCardInfoSeconds * 1000);
    }

    $('.popup-body #loading-spiner').hide();
}

function renderIntercashVirtualCardInfo(e) {
    console.log(e);
    $('.popup-body .p-card-created-data dl.card-error').remove();
    if (e != null) {
        $('.popup-body .p-card-created-data .virtual-card-img').html('<img src="' + e + '" width="300px"/>');
        $('.popup-body .p-card-created-data').css('background', 'none');
        $('.popup-body .p-card-created-data .virtual-card-img img').css('margin-left', '30px');
        $('.popup-body .p-card-created-data .virtual-card-img img').css('margin-top', '10px');
        $('.popup-body .p-card-created-data .virtual-card-img').show();
    } else {
        $('.popup-body .p-card-created-data')
            .append('<dl class="card-error"><dt></dt><dd>Error: Empty response</dd></dl>');
    }

    if (timer == undefined) {
        timer = setTimeout(hideCardInfoGlobal,
            timeShowCardInfoSeconds * 1000);
    }

    $('.popup-body #loading-spiner').hide();
}

function renderTalkbankVirtualCardInfo(pan, expiryDate, phonNumber, notificationText) {
    $('.popup-body .p-card-created-data dl.card-error').remove();

    var dd = $('.popup-body .p-card-created-data .card-info dd');
    var dt = $('.popup-body .p-card-created-data .card-info dt');

    if (pan != null) {
        dd[0].innerHTML = pan.match(/.{1,4}/g).join(' ');
    }

    if (expiryDate != null) {
        dd[1].innerHTML = expiryDate;
    }

    dd[2].style.display = 'none';
    dt[2].innerHTML = notificationText + " " + phonNumber;

    $('.popup-body .p-card-created-data .card-info').show();

    if (timer == undefined) {
        timer = setTimeout(hideCardInfoGlobal,
            timeShowCardInfoSeconds * 1000);
    }

    $('.popup-body #loading-spiner').hide();
}