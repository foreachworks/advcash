// www.verstka.pro
String.prototype.insert = function(index, string) {
	if (index > 0) {
		return this.substring(0, index) + string
				+ this.substring(index, this.length);
	} else {
		return string + this;
	}
};

String.prototype.replaceAt = function(index, char) {
	return this.substring(0, index) + char + this.substring(index + char.length);
};

String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

function animateNewItem(el) {
	var table = el.offset().top;
	var body = (window.opera) ? (document.compatMode=="CSS1Compat" ? $('html') : $('body')) : $('html,body');
	body.animate({scrollTop: table}, 300);
}

function showTip(el) {
	$(el).parents('dd').find('.p-reg-form-item-error').hide();
	$(el).parents('dd').find('.error-tip').hide();
	$(el).parents('li').find('.p-ip-sec-form-item-error').hide();
}

function submitForm(el) {
	clearError();
	initRegSliderCaptcha();
	$('.p-reg-form-item-tip').hide();
	if (jQuery.browser.msie && jQuery.browser.version <= 8) {
	    $('.p-reg-form-item-error').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	    $('.p-reg-form-item-tip').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	    $('.p-reg-form-item-tip-ip').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	    $('.checkbox-tip').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	    $('.error-tip').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	}
}

function clearError() {
	var dest = displayError();
	if (dest != null) {
		var body = (window.opera) ? (document.compatMode=="CSS1Compat" ? $('html') : $('body')) : $('html,body');
		body.animate({scrollTop: dest}, 400);
	}
}

function clearErrorDepositFunds(id) {
	var dest = displayError();
	if (dest != null) {
		var body = (window.opera) ? (document.compatMode=="CSS1Compat" ? $('html') : $('body')) : $('html,body');
		body.animate({scrollTop: 0}, 400);
		document.getElementById(id).focus();
	}
}

function displayErrorNoScroll() {
	displayError();
}

function displayError() {
	var firstError = true;
	var dest = null;
	var div = null;
	if ($('.rf-msgs').html() != "" && $('.rf-msgs').children('.rf-msgs-err').length) {
		$('.rf-msgs').parents('.p-reg-form-item-error').show();
		$('.rf-msgs').parents('.error-tip').show();
		$('.rf-msgs').parents('.global-error-container').show();
		$('.rf-msgs').parents('.info-red').show();
	} else {
		$('.rf-msgs').parents('.p-reg-form-item-error').hide();
		$('.rf-msgs').parents('.error-tip').hide();
		$('.rf-msgs').parents('.global-error-container').hide();
		$('.rf-msgs').parents('.info-red').hide();
	}
	$('.rf-msgs-wrn').hide();
	if ($('.rf-msgs-err').length && $('.rf-msgs-err').parents('.global-error-container').length) {
	      div = $('.rf-msgs-err').parents('.global-error-container');
	      dest = div.offset().top;
	      firstError = false;
	}
	$(".rf-msg").each(function() {
		if ($(this).parents('.global-error-container').length) {
			div = $(this).parents('.global-error-container');
			if ($(this).html() != '') {
				div.css('display', 'block');
				if (firstError) {
					dest = div.offset().top;
					firstError = false;
				}
			}
		} else {
			if ($(this).parents('.p-reg-form-item-error').length) {
		    	div = $(this).parents('.p-reg-form-item-error');
				if ($(this).html() != '') {
				    div.css('display', 'block');
				    if (firstError) {
						dest = div.offset().top;
						firstError = false;
				    }
				} else {
					div.css('display', 'none');
				}
			} else if ($(this).parents('.error-tip').length) {
		    	div = $(this).parents('.error-tip');
				if ($(this).html() != '') {
				    div.css('display', 'block');
				} else {
					div.css('display', 'none');
				}
			} else {
				showNotyError($(this).html());
			}
		}
	});
	return dest;
}

function displayErrorWithId(id) {
	var el = $('#' + id);
	var div = el.parents('.p-reg-form-item-error');
	if (el.html() != '') {
	    div.css('display', 'block');
	} else {
		div.css('display', 'none');
	}
}

function getFileName(el) {
	return el.files[0].name;
}

function getSize(el) {
    if (jQuery.browser.msie) {
        return 0;
    } else {
        try {
            return el.files[0].size;
        } catch (Exception) {
            return 0;
        }
    }
}

function getFileNameFromInputFileValue(value)  {
	var fileName = '';
	if (value.indexOf('/') > -1) {
		fileName = value.substring(value.lastIndexOf('/') + 1);
	} else if (value.indexOf('\\') > -1) {
		fileName = value.substring(value.lastIndexOf('\\') + 1);
	} else {
		fileName = value;
	}
	return fileName;
}

function checkFileType(fileName, allowedExtensions) {
	var extension = fileName.substring(fileName.lastIndexOf('.') + 1).toLowerCase();
	if (typeof allowedExtensions != 'undefined') {
		return allowedExtensions.indexOf(extension) != -1;
	} else {
		if (extension == 'jpg' 
			|| extension == 'jpeg'
			|| extension == 'gif'
			|| extension == 'png'
			|| extension == 'pdf') {
			return true;
		} else {
			return false;
		}
	}
}

function checkFieldValidationError() {
	$(".rf-msg").each(function() {
		if ($(this).parents('.p-reg-form-item-error').length) {
	    	div = $(this).parents('.p-reg-form-item-error');
			if ($(this).html() != '') {
			    div.css('display', 'block');
			    div.parents('dd').children('.p-reg-form-item-tip').hide();
			} else {
				div.css('display', 'none');
			}
		}
	});
}

function hideErrorContainer() {
    $('.p-reg-form-item-error').hide();
}

function checkNotyErrorDepositFunds(id) {
    var displayed = checkNotyError();
    if (displayed) {
        var body = (window.opera) ? (document.compatMode=="CSS1Compat" ? $('html') : $('body')) : $('html,body');
        body.animate({scrollTop: 0}, 400);
		var elById = document.getElementById(id);
        if ($(elById).parents('.p2-payment__main').length) {
			elById.focus();
		}
    }
}

function checkNotyError(el) {
	
	$('.p-reg-form-item-tip').hide();
	
	var div = null;
	var isNotyErrorDisplayed = false; 
	var isErrorDisplayed = false; 
	if ($('.rf-msgs').html() != "" && $('.rf-msgs').children('.rf-msgs-err').length) {
		$('.rf-msgs').parents('.p-reg-form-item-error').show();
		$('.rf-msgs').parents('.info-red').show();
	} else {
		$('.rf-msgs').parents('.p-reg-form-item-error').hide();
		$('.rf-msgs').parents('.info-red').hide();
	}
	$('.rf-msgs-wrn').hide();
	$('.global-error-container').hide();
	
	$(".rf-msgs-sum").each(function() {
		if ($(this).parents('.rf-msgs-err').length && $(this).parents('.global-error-container').length && !isNotyErrorDisplayed) {
			if($(this).html() && !(0 === $(this).html().length)) {
				showNotyError($(this).html());
				isNotyErrorDisplayed = true;
			}
		}
	});
	
	$(".rf-msg").each(function() {
	      if ($(this).parents('.p-reg-form-item-error').length) {
		    	div = $(this).parents('.p-reg-form-item-error');
				if ($(this).html() != '') {
				    div.css('display', 'block');
				    isErrorDisplayed = true;
				} else {
				  div.css('display', 'none');
				}
	      } else {
	    	  if ($(this).find('.rf-msg-det').length) {
	    		  if ($(this).html() && !(0 === $(this).html().length) && !isNotyErrorDisplayed) {
	    			  showNotyError($(this).find('.rf-msg-det').html());
	    			  isNotyErrorDisplayed = true;
	    		  }
	    	  } else {
	    		  if ($(this).html() && !(0 === $(this).html().length) && !isNotyErrorDisplayed) {
	    			  showNotyError($(this).html());
	    			  isNotyErrorDisplayed = true;
	    		  }
	    	  }
	      }
	});
	return (isNotyErrorDisplayed || isErrorDisplayed);
}

function refreshPasswordField(idRemoved) {
	var toBeRemoved = document.getElementById(idRemoved);
    if (toBeRemoved.previousSibling) {
    	toBeRemoved.parentNode.removeChild(toBeRemoved);
    }
    refreshIphoneStyleForPasswords();
}

function CloseInfo(el) {
	$(el).parent().remove();
}

function ClosePopup() {
    if (!$('.popup-wrapper').hasClass('unclosable')) {
        $('.popup-wrapper').css('display', 'none');
    }
	// if (!$('.popup').hasClass('unclosable')) {
	// $('.popup').css('display', 'none');
	// }
    $('.popup').each(function(index) {
    	var popup = $(this);
    	if (!popup.hasClass('unclosable')) {
    		popup.css('display', 'none');
        }
	});
    $('a[onclick*="Popup(this)"]').removeClass('active');
    $('#destPurseIdComponentId').removeClass('active');
    $('.block-operations li, .block-cards-item').removeClass('wallet-red');
    $('.block-operations li, .block-cards-item').removeClass('wallet-red-hover');
	$('.block-operations li, .block-cards-item').removeClass('wallet-green');
	$('.block-operations li, .block-cards-item').removeClass('wallet-green-hover');
	$('.selector-wallet > a, .selector-recent > a, .selector-card > a').removeClass('active');
}

function Popup(target) {
	$('.block-operations li, .block-cards-item').removeClass('wallet-red');
	$('.block-operations li, .block-cards-item').removeClass('wallet-green');
	$('tr').has(target).removeClass('unread');
    $('a').not(target).next('.popup').css('display', 'none');
    $(target).next('.popup').toggle().end().toggleClass('active');
    if ($(target).attr('id') == 'accountNumberToggler') {
    	$('#destPurseIdComponentId').toggleClass('active');
    }
    $('.selector-wallet > a, .selector-recent > a, .selector-card > a').removeClass('active');
}

function ParentPopup(target) {
	$('.block-operations li, .block-cards-item').removeClass('wallet-red');
	$('.block-operations li, .block-cards-item').removeClass('wallet-green');
	$('tr').has(target).removeClass('unread');
	$('a').not(target).parent().next('.popup').css('display', 'none');
	$(target).parent().next('.popup').toggle().end().toggleClass('active');
	if ($(target).attr('id') == 'accountNumberToggler') {
		$('#destPurseIdComponentId').toggleClass('active');
	}
	$('.selector-wallet > a, .selector-recent > a, .selector-card > a').removeClass('active');
}

function Combo(target, $el) {
    $('.popup:not(' + target + ')').css('display', 'none');
    $('#destPurseIdComponentId').removeClass('active');
    $('.selector-wallet > a, .selector-recent > a, .selector-card > a').removeClass('active');
    disableProtecionCode();
    //$(target).css('display', 'block');
    if ($('#destPurseIdComponentId').length) {
    	var value = $('#destPurseIdComponentId').val();
    	$('#destPurseIdComponentId').val(value.replace(/\s/g, ''));
    }
    var destWalletId = $('#destWalletId');
    if (destWalletId.length) {
    	var value = destWalletId.val();
    	destWalletId.val(value.replace(/\s/g, ''));
    }
    var userLogin = $('#userLogin');
    if (userLogin.length) {
		userLogin.show();
		$('#selectedCardPan').hide();
    }
    if ($el.hasClass('selected')) {
        $el.removeClass('selected').focus().prev().text('').css('display', 'none');
    }
}

function showCardNumber() {
	var value = $('#destLogin div b a').text();
	if (value.length) {
		$('#userLogin').hide();
		$('#selectedCardPan').show();
	}
}

function disableProtecionCode() {
	$('#useProtection').attr('disabled', 'disabled');
    $('#protectionCodeComponentId').attr('disabled', 'disabled');
    $('#expirationDateComponentId').attr('disabled', 'disabled');
    $('.protection dt, .protection dd').css('opacity', '0.5');
    $('#useProtection').parent().css('cursor', 'default');
}

function enableProtecionCode() {
	$('#useProtection').removeAttr('disabled');
    $('#protectionCodeComponentId').removeAttr('disabled');
    $('#expirationDateComponentId').removeAttr('disabled');
    $('.protection dt, .protection dd').css('opacity', '1');
    $('#useProtection').parent().css('cursor', 'pointer');
}

function LoadTransactions() {
    $('.p-main-transactions-loader').css('display', 'block');
}

function SetProtection(box) {
    $(box).toggleClass('active');
}

function SetWallet(el, content, target) {
    ClosePopup();
    for (var i = 0; i < content.length; i++)
        content[i] = $(el).find(content[i]).text();
    $('input[name=' + target + ']').addClass('selected').val(content[1])
        .prev('div').css('display', 'block').text(content[0]);
}

function SetMyWallet(el, content, target) {
	ClosePopup();
	for (var i = 0; i < content.length; i++)
		content[i] = $(el).find(content[i]).text();
	$(target + ' span').text(content[0]);
	$(target + ' del').text(content[3]);
	$(target + ' b').text(content[2]);
	$(target + ' input').val(content[1]);
}

function SetMyCard(el, content, target) {
	ClosePopup();
	for (i = 0; i < content.length; i++)
		content[i] = $(el).find(content[i]).text();
	$(target).find('span').text(content[0]).end().find('del').text(content[1])
			.end().find('input').val(content[1]);
}

var curTab;

function MainTab(tab, el) {
    $('.tab').css('display', 'none');
    $(tab).css('display', 'block');
    $(el).parents('ul').find('li').removeClass('active');
    $(el).parents('li').addClass('active');
    curTab = tab;
}

function ToggleBlock(el) {
    $(el).toggle();
}

function SelectAll(cb, el) {
    var $cb = $('input[type="checkbox"][name="' + cb + '"]');
    if ($(el).attr('checked') == 'checked') {
        $cb.attr('checked', true);
        $cb.parents('tr').addClass('checked');
    }
    else {
        $cb.attr('checked', false);
        $cb.parents('tr').removeClass('checked');
    }
}

function SelectChain(cb, el) {
    var $cb = $('input[type="checkbox"][name="' + cb + '"]');
    if ($(el).attr('checked') == 'checked') {
        $cb.attr('checked', true);
        $cb.parents('.p-ticket-chain-header').addClass('checked');
    } else {
        $cb.attr('checked', false);
        $cb.parents('.p-ticket-chain-header').removeClass('checked');
    }
}
    
function ToggleChain(el) {
	var ul = $(el).parents('.p-ticket-chain-header');
    if (ul.hasClass('opened')) {
    	ul.removeClass('opened');
    	ul.next().addClass('hidden');
    } else {
    	ul.addClass('opened');
    	ul.next().removeClass('hidden');
    }
}

function PasswordToggle(el) {
	if ($(el).parents('dd').find('input').attr('type') == 'password') {
		$(el).parents('dd').prepend('<input type="text" maxlength="50" id="password" value="'+$(el).parents('dd').find('input[type="password"]').val()+'" />');
		$(el).parents('dd').find('input[type="password"]').remove();
	} else {
		$(el).parents('dd').prepend('<input type="password" maxlength="50" id="password" class="lastchar" value="'+$(el).parents('dd').find('input[type="text"]').val()+'" />');
		$(el).parents('dd').find('input[type="text"]').remove();
		
	}
}

function OpenHelp(el) {
	$(el).parents('dd').find('.p-verify-rightcol-a').toggle();
}
function OpenHelpList(el) {
	$(el).parents('li').find('.p-verify-rightcol-a').toggle();
}

function RegTogglePhone() {
	if ($('#phone-toggler').css('display') != 'none') {
		$('#secret-own-question-toggler').hide();
		$(".combo-header").text($("#secretQuestion option[value='1']").text());
		$("#secretQuestion option[value='-1']").removeAttr('selected', 'selected');
		$('#secret-question-toggler').show();
	}
	$('.phone-toggler').toggle();
}

function RegTogglePass(el) {
	if ($(el).text() == showPassword) {
		$(el).text(hidePassword);
	} else {
		$(el).text(showPassword);
	}
	$('#genPass').toggle();
	$('#asterix').toggle();
}

function GeneratePass(el, value) {
	securePasswordGenerated = true;
	$('input[name="'+el+'"]').hide();
	$('input[name="'+el+'"]').next('input').remove();
	$('input[name="'+el+'"]').parents('#i-pass').find('.i-pass-toggler').remove();
	$('input[name="'+el+'"]').val(value);
	$('input[name="'+el+'"]').after('<div class="i-pass-toggler"><a class="cp" onclick="RegTogglePass(this)">'+showPassword+'</a></div>');
	$('input[name="'+el+'"]').after('<input type="text" id="genPass" value="'+value+'" style="display: none" />');
	var cloakingCharacter;
	if (jQuery.browser.msie || jQuery.browser.mozilla) {
		cloakingCharacter = '\u25CF';
	} else {
		cloakingCharacter = '\u2022';
	}
	var hiddenValue = '';
	for (var i = 0; i < value.length; i++) {
		hiddenValue += unescape(cloakingCharacter);
	}
	$('input[name="'+el+'"]').after('<input type="text" id="asterix" value="'+hiddenValue+'" />');
	$('#p-reg-form-item-pw').hide();
	$('#i-pass .step1').hide();
	$('#i-pass .step2').show();
	$('#i-pass input').attr('readonly', 'readonly');
	$('.p-reg-form-item-error').hide();
}

function RegUserPass(el) {
	securePasswordGenerated = false;
	$('#p-reg-form-item-pw, #i-pass .step1').show();
	$('#i-pass .step2').hide();
	$('#i-pass  .i-pass-toggler, #genPass, #asterix').remove();
	$('#i-pass input[name="'+el+'"]').val('');
	$('#i-pass input[name="'+el+'"]').trigger(jQuery.Event("keyup", { keyCode: 0 }));
	$('#i-pass input[name="'+el+'"]').show();
	$('#i-pass input[name="'+el+'"]').focus();
	$('#i-pass input[name="'+el+'"]').removeAttr('readonly');
	refreshIphoneStyleForPasswords();
}

function SettFormResend(el) {
	$(el).parents('.p-reg-form-item-tip').before('<span class="inline-tip">'+smsSendAgain+'</span>');
	$(el).parents('.p-reg-form-item-tip').remove();
}

function RegSetName(name) {
	$('input[name="registrationLoginComponentId"]').val(name).attr('class', 'passed').parents('dd').find('.p-reg-form-item-error').hide();
	setCookie('reg-login', name);
}

function RegMailOfferClose() {
	$('.p-reg-form-item-mailoffer').css('display','none');
}

function RegMailOffer(mail) {
	$('input[name="registrationEmailId"]').val(mail);
	validateLoginEmail($('#registrationLoginComponentId').val(), $('#registrationEmailId').val());
	//$('.p-reg-form-item-mailoffer').parents('dd').find('.p-reg-form-item-error').hide();
	//setCookie('reg-email', mail);
	RegMailOfferClose();
}

function ResendSms() {
	$('.popup-reg-sms-form-hint').hide();
	$('.popup-reg-sms-form-sended').show();
}

function reApplyChznStyleForSelect() {
	if ($(".chzn").length /*&& typeof($(".chzn").chosen) == "function"*/) {
		if (jQuery.browser.msie && jQuery.browser.version == 7) $(".chzn").customSelect();
		else $(".chzn").chosen();
	}
}

function reApplyCustomStyleForSelect() {
	if ($('select.custom').length != 0)
	    $('select.custom').customSelect();
}

function testPassword(passwd, element){
	//console.log(passwd);
	var intScore = 0;
	var strVerdict = passwordSecurityLevelVeryWeak;
	var strClass = 'red';
	
	if (passwd.length < 6) {
		strVerdict = passwordSecurityLevelVeryWeak;
		strClass='red';
	} else {
	  
	    if (passwd.match(/[a-z]/)) intScore = (intScore+1);
	    if (passwd.match(/[A-Z]/)) intScore = (intScore+1);
	    // NUMBERS
	    if (passwd.match(/\d+/)) intScore = (intScore+1);
	    // SPECIAL CHAR
	    if (passwd.match(/[^a-zA-Z0-9\s]/gi)) intScore = (intScore+1);
	    
	    if (passwd.length>=6 && intScore >= 1) {
		    strVerdict = passwordSecurityLevelWeak;
		    strClass='orange';
	    }
	    if (passwd.length>=7 && intScore >= 2) {
		    strVerdict = passwordSecurityLevelMedium;
		    strClass='blue';
	    }
	    if (passwd.length>=8 && intScore == 4) {
		    strVerdict = passwordSecurityLevelStrong;
		    strClass='green';
	    }
	}
	
	if (passwd.length > 0) {
		$('#passwordDifficultyDescr').html(strVerdict);
		$(element).removeClass().addClass(strClass);
	} else {
		$('#passwordDifficultyDescr').html(passwordSecurityLevelString);
		$(element).removeClass();
	}
	$(element).find('dt span').css('width', passwd.length*6+'px');
}

function processSelectNotMobile() {
	$("#secretOwnQuestion").attr('placeholder', ownQuestion);
	if (window.saveState !== undefined && saveState) {
		regSecretOwnQuestion = getCookie('reg-secret-own-question');
		regSecretAnswer = getCookie('reg-secret-answer');
		if (regSecretOwnQuestion != null) {
			$('#secretOwnQuestion').val(regSecretOwnQuestion);
		}
		if (regSecretAnswer != null) {
			$('#secretAnswerId').val(regSecretAnswer);
		}
	}
	$('#secretOwnQuestion').blur();
	if (blurFieldId == 'secretOwnQuestion') {
		blurFieldId = null;
	}
}

function processSelectMobile() {
	regPhone = getCookie('reg-phone');
	if (regPhone != null) {
		$('#phoneNumberComponentId').val(regPhone);
	}
}

function setCookie(name, value, expires, path, domain, secure) {
	document.cookie = name + "=" + escape(value) +
		((expires) ? "; expires=" + expires : "") +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
}

function getCookie(name) {
	var cookie = " " + document.cookie;
	var search = " " + name + "=";
	var setStr = null;
	var offset = 0;
	var end = 0;
	if (cookie.length > 0) {
		offset = cookie.indexOf(search);
		if (offset != -1) {
			offset += search.length;
			end = cookie.indexOf(";", offset);
			if (end == -1) {
				end = cookie.length;
			}
			setStr = unescape(cookie.substring(offset, end));
		}
	}
	return(setStr);
}

function tip(el) {
		if ($(el).is('.p-reg-form-item input:not(".notip"), textarea:not(".notip")') || $(el).parents().is('.p-reg-form-item .chzn-container')) {
			$('.p-reg-form-item-tip.noblur').hide();
			$(el).parents('dd').find('.p-reg-form-item-tip').show();
		}
		if ($(el).parent().is('.p-reg-form-item .chzn-results') || $(el).is('option')) {
			$('.p-reg-form-item-tip:not(".noblur")').hide();
		}
		if ($(el).parents().is('#i-pass') && securePasswordGenerated) {
			$('#i-pass .step1').hide();
		}
		$('.p-reg-form-item-error').hide();
		showTip(el);
}

var securePasswordGenerated = false;
var regLogin;
var regEmail;
var regPhone;
var regSecretOwnQuestion;
var regSecretAnswer;
var smsSendAgain;

function smsCodeKeypress(evt) {
    if (evt.keyCode == 13) {
	      $('#smsCodeCheckButton').click();
	      return false;
    }
}

function suggestCity() {
	var selected = $('#cityComponentIdInput').val();
	$("#cityComponentIdItems div").each(function() {
		var value = $(this).html();
		value.replace('<em>', '').replace('</em>', '');
		m = value.substr(0, selected.length) + "</em>" + value.substr(selected.length);
		m = "<em>" + m;
		$(this).html(m);
	});
}

function refreshIphoneStyleForPasswords() {
	if ($('.lastchar').length != 0) {
		$('.lastchar').dPassword({interval: 100, duration: 1000});
	}
}

function checkErrorForNoty(msg) {
	
	if ($('.rf-msg-det').length && $('.rf-msg').parent('.sum').length && $('.rf-msg-det').html() != "") {
		msg = $('.rf-msg-det').html();
	}
	if(!(!msg || 0 === msg.length)) {
	noty({
		"text":msg,
		"layout":"center",
		"type":"error",
		"textAlign":"center",
		"easing":"swing",
		"animateOpen":{"height":"toggle"},
		"animateClose":{"height":"toggle"},
		"modal":true,
		"speed":"500",
		"timeout":"3000",
		"closable":true,
		"theme":"defaultTheme",
		"closeWith":["click","backdrop"]
		});
	}
}

function checkAlertForNoty(msg) {
	
	if ($('.rf-msg-det').length && $('.rf-msg').parent('.sum').length && $('.rf-msg-det').html() != "") {
		msg = $('.rf-msg-det').html();
	}
	if(!(!msg || 0 === msg.length)) {
		noty({
			"text":msg,
			"layout":"center",
			"type":"alert",
			"textAlign":"center",
			"easing":"swing",
			"animateOpen":{"height":"toggle"},
			"animateClose":{"height":"toggle"},
			"modal":true,
			"speed":"500",
			"timeout":"3000",
			"closable":true,
			"theme":"defaultTheme",
			"closeWith":["click","backdrop"]
		});
	}
}

function showNotyError(msg) {
	
	if(!(!msg || 0 === msg.length)) {
		noty({
			"text":msg,
			"layout":"center",
			"type":"error",
			"textAlign":"center",
			"animateOpen":{"height":"toggle"},
			"animateClose":{"height":"toggle"},
			"modal":true,
			"speed":"500",
			"timeout":"12000",
			"closable":true,
			"theme":"defaultTheme",
			"closeWith":["click","backdrop"]
		});
	}
}

function showNotyInfo(msg) {
	
	if(!(!msg || 0 === msg.length)) {
		noty({
			"text":msg,
			"layout":"center",
			"type":"information",
			"textAlign":"center",
			"animateOpen":{"height":"toggle"},
			"animateClose":{"height":"toggle"},
			"modal":true,
			"speed":"500",
			"timeout":"1200",
			"closable":true,
			"theme":"defaultTheme",
			"closeWith":["click","backdrop"]
		});
	}
}

function walletsSelect(el) {
	if ($('#addMerchantWalletLimit').length) {
		$('.chzn-single span').text(chooseWallet);
		$('#addMerchantWalletLimit').parent().addClass('disabled');
		$('#addMerchantWalletLimit').attr('disabled', 'disabled');
	}
}

function formatWallet() {
	if ($('#destPurseIdComponentId').length) {
		var value = $('#destPurseIdComponentId').val();
		if (new RegExp(/^[UERGHTB]\d{12}$/).test(value)) {
			var i = 1;
			var p = 4;
	        var result = value.substring(0, 1);
	        for (var j = 0; j < 3; j++) {
	            result = result.concat(" ").concat(value.substring(i, (i += p)));
	        }
	        $('#destPurseIdComponentId').val(result);
		}
	}
}

function formatWalletByElementId(id) {
	var el = $(id);
	if (el.length) {
		var value = el.val();
		if (new RegExp(/^[UERGHTB]\d{12}$/).test(value)) {
			var i = 1;
			var p = 4;
	        var result = value.substring(0, 1);
	        for (var j = 0; j < 3; j++) {
	            result = result.concat(" ").concat(value.substring(i, (i += p)));
	        }
	        el.val(result);
		}
	}
	if ($('#useProtection').prop('checked')) {
		$('.p-transfer-protection').show();
	}
}

function accountNumberAutocomplete(el) {
	var value = $(el).val();
	var length = $(el).val().length;
	var success = false;
	if (length > 0) {
		$('#accountNumberAutocomplete li').each(function(index) {
			valueClean = value.replace(/\s/g, '').toUpperCase();
			var li = $(this);
			li.css('border-bottom', '1px solid #EBEBEB');
			var b = li.children('a').children('b');
			var login = b.text();
			login = login.replace('<dfn class="marker">', '').replace(
					'</dfn>', '');
			var span = li.children('a').children('span');
			var accountNumber = li.children('a').children('span')
					.text();
			accountNumber = accountNumber.replace('<dfn class="marker">', '').replace('</dfn>', '');
			accountNumberClean = accountNumber.replace(/\s/g, '');
			var startLogin = login.search(value);
			var start = accountNumberClean.search(valueClean);
			var match = false;
			if (startLogin != -1) {
				var replaceLogin = login.replace(value,
						'<dfn class="marker">' + value + '</dfn>');
				b.html(replaceLogin);
				success = true;
				match = true;
			} else {
				b.html(login);
			}
			if (start != -1) {
				var replace = accountNumberClean.replace(valueClean,
						'<dfn class="marker">' + valueClean + '</dfn>');
				var j = 0;
				var inside = false;
				var wallet = '';
				for (var charIndex = 0; charIndex < replace.length; charIndex++) {
					var whitespace = '';
					if (replace[charIndex] == '<') {
						inside = true;
					} else if (replace[charIndex] == '>') {
						inside = false;
					} else {
					    if (!inside) {
							if (j == 1 || j == 5 || j == 9) {
								whitespace = ' ';
							}
							j++;
					    }
					}
					wallet += whitespace + replace[charIndex];
				}
				span.html(wallet);
				success = true;
				match = true;
			} else {
				span.html(accountNumber);
			}
			if (match) {
				li.show();
			} else {
				li.hide();
			}
		});
	}
	if (success) {
		$('.combobox-number-big').show();
		$('#accountNumberAutocomplete li:visible:last').css('border-bottom', 'none');
	} else {
		$('.combobox-number-big').hide();
	}
}

function accountCardsAutocomplete(el) {
	var value = $(el).val();
	var length = $(el).val().length;
	var success = false;
	if (length > 0) {
		$('#accountCardsAutocomplete li').each(
				function(index) {
					var li = $(this);
					li.css('border-bottom', '1px solid #EBEBEB');
					var b = li.children('a').children('b');
					var login = b.text();
					login = login.replace('<dfn class="marker">', '').replace(
							'</dfn>', '');
					var span = li.children('a').children('span');
					var email = span.text();
					email = email.replace('<dfn class="marker">', '').replace(
							'</dfn>', '');
					var email = li.children('a').children('span').text();
					email = email.replace('<dfn class="marker">', '').replace(
							'</dfn>', '');
					var startLogin = login.search(value);
					var startEmail = email.search(value);
					var match = false;
					if (startLogin != -1) {
						var replaceLogin = login.replace(value,
								'<dfn class="marker">' + value + '</dfn>');
						b.html(replaceLogin);
						success = true;
						match = true;
					} else {
						b.html(login);
					}
					if (startEmail != -1) {
						var replaceEmail = email.replace(value,
								'<dfn class="marker">' + value + '</dfn>');
						span.html(replaceEmail);
						success = true;
						match = true;
					} else {
						span.html(email);
					}
					if (match) {
						li.show();
					} else {
						li.hide();
					}
				});
	}
	if (success) {
		$('.combobox-number-big').show();
		$('#accountCardsAutocomplete li:visible:last').css('border-bottom',
				'none');
	} else {
		$('.combobox-number-big').hide();
	}
}

var timeout;
var SESSION_TIMEOUT_MIN = 15;
var SESSION_TIMEOUT = (SESSION_TIMEOUT_MIN * 60 * 1000) + 10000; // milliseconds

function sessionTimeout() {
	window.location.reload(true);
}

var a4jcommandButton;

function onstart(event) {
	a4jcommandButton = $(event.source);
	if (a4jcommandButton.is('input[type="submit"]')) {
		$('input[type="submit"]:not(#verificationButtonThirdLevel, .enrollToken)').attr('disabled', 'disabled');
		//setTimeout(function() {ajaxStatus(a4jcommandButton);}, 500);
		ajaxStatus(a4jcommandButton);
	} else {
		a4jcommandButton = null;
	}
}

var removeLoader = true;

var authorized  = 0;
function onstop() {
	$('input[type="submit"]:not(#verificationButtonThirdLevel, .enrollToken), input[type="button"]').removeAttr('disabled');
	if (a4jcommandButton != null) {
		var parent = a4jcommandButton.parent();
		if (parent.is('.button')) {
			parent.removeClass('disabled');
		}
	}
	//$('#ajax-status').hide();
	if (a4jcommandButton == null || removeLoader) {
		$('span.loader').hide();
	}
	if (authorized) {
    	// clearTimeout(timeout);
		// timeout = setTimeout(sessionTimeout, SESSION_TIMEOUT);
	}
}

function ajaxStatus(element) {
	/*if (element != null) {
		var parent = element.parent();
		if (parent.is('.button')) {
			parent.addClass('disabled');
			var offset = element.offset();
			var status = $('#ajax-status');
			status.css('left', offset.left + (parent.width() - 46) / 2);
			status.css('top', offset.top + (parent.height() / 2) - 23);
			status.show();
		}
	}*/
	if (element != null) {
		var parent = element.parent();
		if (!parent.has('span.loader').length) {
			parent.append('<span class="loader"></span>');
		}
		parent.children('span.loader').show();
	}
}

function ajaxStatusLoader(id) {
	var element = $('.' + id);
	if (element != null) {
		if (!element.has('span.loader').length) {
			element.append('<span class="loader"></span>');
		}
		element.children('span.loader').show();
	}
}

function showTx() {
	$('#txListDataTable').find('tr:last-child').after($('#tx-cache-table tbody').html());
	$('#tx-show-more').html($('#tx-cache-show-more').html());
}

function showNewTx() {
	$('#txListDataTable').find('tr:first-child').after($('#new-tx-cache-table tbody').html());
}

function inputNumberFormatChecker(id) {
	setInterval(function() {
		numberFormatChecker(id);
	}, 5);
}

var target = new Array();
function numberFormatChecker(id) {
	var el = $('#' + id);
	if (el.val() != undefined && target[id] != el.val()) {
		numberFormat(el);
		target[id] = el.val();
	}
}

function numberFormat(el) {
	var value = el.val();
	var newValue = '';
	var wrongSymbolDetected = false;
	if (value == '.') {
		newValue = '0.00';
	} else {
		var dotDetected = false;
		for (var i = 0; i < value.length; i++) {
			var symbol = value[i];
			if (isNumeric(symbol)) {
				newValue = newValue + symbol;
			} else if ((symbol == '.' || symbol == ',') && !dotDetected) {
				newValue = newValue + '.';
				dotDetected = true;
			} else if (symbol != ' ') {
				wrongSymbolDetected = true;
			}
		}
		if (newValue == '.') {
			newValue = '0.00';
		}
		var dot = '';
		var start = '';
		var end = '';
		if (newValue.indexOf('.') != -1) {
			var array = newValue.split('.');
			start = array[0];
			end = array[1];
			dot = '.';
		} else {
			start = newValue;
		}
		var length = start.length;
		var delimeterPosition = 0;
		if (length > 3) {
			var i = 0;
			var firstInsert = length % 3;
			if (firstInsert > 0) {
				start = start.insert(firstInsert, ' ');
				delimeterPosition += firstInsert;
				i = 1;
			}
			delimeterPosition += 3;
			while (delimeterPosition < length) {
				start = start.insert(delimeterPosition + i, ' ');
				delimeterPosition += 3;
				i++;
			}
		}
		newValue = start + dot + end;
	}
	var $this = el.get(0);
	var selectionStart = getSelectionStart($this);
	el.val(newValue);
	var spacesInValue = countSymbolsInString(value.substring(0, selectionStart), ' ');
	var spacesInNewValue = countSymbolsInString(newValue.substring(0, selectionStart), ' ');
	if (value.length > newValue.length) {
		var shift = 0;
		if (spacesInValue > spacesInNewValue) {
			shift = 1;
		}
		processInputSelection($this, selectionStart - shift);
	} else {
		var shift = 0;
		if (spacesInValue < spacesInNewValue) {
			shift = 1;
		}
		processInputSelection($this, selectionStart + shift);
	}
	if (wrongSymbolDetected) {
		showNotyError(invalidInputFormat);
	}
}

function countSymbolsInString(string, symbol) {
	return string.split(symbol).length - 1;
}

function getSelectionStart(el) {
	if (!jQuery.browser.msie) {
		return el.selectionStart;
	} else {
		el.focus();
        var sel = document.selection.createRange();
        var selLen = document.selection.createRange().text.length;
        sel.moveStart('character', -el.value.length);
        return sel.text.length - selLen;
	}
}

function processInputSelection(el, selection) {
	if (!jQuery.browser.msie) {
		el.selectionStart = selection;
		el.selectionEnd = selection;
	} else {
		var range = document.selection.createRange();
		range.moveStart('character', -(el.value.length - selection));
		range.moveEnd('character', -(el.value.length - selection));
		range.select();
	}
}

function isNumeric(value) {
	return !isNaN(value) && value != ' ';
}

function enter(event, id) {
	if (event.keyCode == 13) {
		$('.lastchar').blur();
		$('#' + id).click();
		return false;
    }
}

function blockLink() {
	$('.show-more-link').hide();
	$('.collapse-all-link').hide();
	$('#virtualCardForm .p-history-nothing, #plasticCardForm .p-history-nothing').hide();
	$('.show-more-link-fake').show();
	$('.collapse-all-link-fake').show();
	$('.virtual-card-link-USD').addClass('dn');
	$('.plastic-card-link-USD').addClass('dn');
	$('.virtual-card-link-EUR').addClass('dn');
	$('.plastic-card-link-EUR').addClass('dn');
	$('.virtual-card-link-fake-USD').removeClass('dn');
	$('.plastic-card-link-fake-USD').removeClass('dn');
	$('.virtual-card-link-fake-EUR').removeClass('dn');
	$('.plastic-card-link-fake-EUR').removeClass('dn');
}

function unblockLink() {
	$('.show-more-link').show();
	$('.collapse-all-link').show();
	$('#virtualCardForm .p-history-nothing, #plasticCardForm .p-history-nothing').show();
	$('.show-more-link-fake').hide();
	$('.collapse-all-link-fake').hide();
	$('.virtual-card-link-fake-USD').addClass('dn');
	$('.plastic-card-link-fake-USD').addClass('dn');
	$('.virtual-card-link-fake-EUR').addClass('dn');
	$('.plastic-card-link-fake-EUR').addClass('dn');
	$('.virtual-card-link-USD').removeClass('dn');
	$('.plastic-card-link-USD').removeClass('dn');
	$('.virtual-card-link-EUR').removeClass('dn');
	$('.plastic-card-link-EUR').removeClass('dn');
}

function formatAmount(input) {
	var value = input.val();
	// if(!value.length){
	// input.val("0.00");
	// return;
	// }
	value = value.replace('/r', '/').replace(/ /g, '').replace(/,/g, '.')
			.replace(/[A-Za-z$-]/g, "").replace(/[^0-9$.,]/g, '').replace(
					/^0+/, ''); // Remove spaces,comma, for blur again
	if (/^[0]+$/.test(value.replace(/\./g, '').replace(/,/g, ''))) {
		input.val("0.00");
		return;
	}

	if (!value.length) {
		input.val("0.00");
		return;
	}

	if (value.indexOf(".") != -1) {
		// remove extra dots
		var array_value = value.split(".");
		value = array_value[0].concat('.').concat(array_value[1]);
	} else {
		// add coins
		value = value.concat(".00");
	}

	if (value.indexOf(".") == 0) {
		value = "0" + value;
	}
	// slice end of amount and validate(remove other extra digits)
	var end = value.substring(value.indexOf("."), value.length);
	if (end.length > 3) {
		end = end.substring(0, 3);
	}
	if (end.length == 2) {
		end = end.concat("0");
	}
	if (end.length == 1) {
		end = end.concat("00");
	}
	// get real part of amount
	value = value.substring(0, value.indexOf("."));

	// format amount with spaces
	if (value.length > 3) {
		value = reverse(value);
		var result = value.replace(/(\d{3})/g, '$1 ')
				.replace(/(^\s+|\s+$)/, '');
		result = reverse(result);
		input.val(result.concat(end));
	} else {
		input.val(value.concat(end));
	}
}

var RequireFieldUtil = {
    latinInput : function(elem){
        var val = $(elem).val();
        if(RequireFieldUtil.latinInputCheck(val)){
            val = val.replace(/[^\u0000-\u007f]/gi,'');
            $(elem).val(val);
        }
    },
    latinInputCheck : function(val){
        return  /[^\u0000-\u007f]/.test(val);
    }
}

var ValidationMessageUtil = {
    showTip : function(id,tipId,options){
        $tip = $(id).parents('dd').find(tipId);
        if(options !== undefined){
            $tip.css(options);
        }
        $tip.show();
    },
    hideTip : function(id,tipId){
        $(id).parents('dd').find(tipId).hide();
    }
}

function reverse(s) {
    var o = '';
    for (var i = s.length - 1; i >= 0; i--)
        o += s[i];
    return o;
}

var chrome = /chrom(e|ium)/.test(navigator.userAgent.toLowerCase());
var printCount = 0;
function printPage() {
	window.print();
	// workaround for Chrome bug -
	// https://code.google.com/p/chromium/issues/detail?id=141633
	if (chrome && window.stop) {
		if (printCount == 0) {
			printCount++;
			location.reload(); // triggering unload (e.g. reloading the page)
			// makes
			// the print dialog appear
		}
		window.stop(); // immediately stop reloading
	}
	return false;
}

function subcards() {
	$('#cards-virtual-plastic li').removeClass('active');
	$('#cards-virtual-plastic li:first-child').addClass('active');
}

var timer = undefined;
var timeShowCardInfoSeconds = 30;

function hideCardInfoGlobal() {
	if (timer != undefined) {
		clearTimeout(timer);
		timer = undefined;
	}
    var cardInfoDd = $('.popup-body .p-card-created-data .card-info dd');
    cardInfoDd[0].innerHTML="";
    cardInfoDd[1].innerHTML="";
    cardInfoDd[2].innerHTML="";
    $('.popup-body .p-card-created-data .card-pin dd')[0].innerHTML="";
    RichFaces.$('cardInfo').hide();
}

function showCardInfoDialog() {
	$('.popup-body .p-card-created-data .card-info').hide();
	$('.popup-body .p-card-created-data .card-pin').hide();
	$('.popup-body .p-card-created-data .p-profile-verify-item-fast').hide();
	$('.popup-body .p-card-created-data .virtual-card-img').hide();
	RichFaces.$('cardInfo').show();
	$('.popup-body #loading-spiner').show();
}

function cardHolderInfoVirtualGlobal(cardHolderInfoLink, authenticationToken) {
	RichFaces.$('cardInfo').show();

    //show spinner
	var cardInfo = $('.popup-body .p-card-created-data .card-info');
	cardInfo.hide();
    $('.popup-body .p-card-created-data .card-pin').hide();
    $('.popup-body .p-card-created-data .p-profile-verify-item-fast').hide();
    $('.popup-body #loading-spiner').show();

    $.ajax({
        type: "GET",
        url: cardHolderInfoLink,
        headers: {"AuthenticationToken":authenticationToken,"Accept":"application/json","Content-Type":"application/json","PROGRAMNAME":"AdvanceCashUK"},
        success: function(data) {
            // Hide spinner
            $('.popup-body #loading-spiner').hide();
            var dd = $('.popup-body .p-card-created-data .card-info dd');
            dd[0].innerHTML=data.pan.match(/.{1,4}/g).join(' ');
            dd[1].innerHTML=data.expiryDate.split("-")[0]+"-"+data.expiryDate.split("-")[1];
            dd[2].innerHTML=data.cvc2;
			cardInfo.show();
			if (timer == undefined) {
				timer = setTimeout(hideCardInfoGlobal,
						timeShowCardInfoSeconds * 1000);
			}
        },
        error : function(jqXHR,textStatus,errorThrown){
        }
    });
}

var pinResponseData;

function getCardPINSessionTokenValidation(authenticationToken) {
	RichFaces.$('cardInfo').show();

    // show spinner
    $('.popup-body .p-card-created-data .card-info').hide();
    $('.popup-body .p-card-created-data .card-pin').hide();
    var otpPanel = $('.popup-body .p-card-created-data .p-profile-verify-item-fast');
    otpPanel.hide();
    $('.popup-body #loading-spiner').show();

    $.ajax({
        type: "POST",
        url: getCardPINSessionTokenUrl,
        data: JSON.stringify({"sessionToken":authenticationToken,"businessPartnerName":"AdvanceCash"}),
        crossDomain : true,
	    dataType: "json",
		contentType: "application/json",
		"Access-Control-Allow-Origin":"*",
        success: function(data) {
        	pinResponseData = data;
            // hide spinner
            $('.popup-body #loading-spiner').hide();
            otpPanel.show();
            $('#otpForPinRetrieving').focus();
        },
        error : function(jqXHR,textStatus,errorThrown){
        	showNotyError(cardPinRetrieveError);
        }
    });
}

function getCardPIN() {
	var otpForPinRetrieving = $('#otpForPinRetrieving');
	var otp = otpForPinRetrieving.val().trim();
	if (!new RegExp(/^\d{5,6}$/).test(otp)) {
		showNotyError(invalidPinOtpFromatErrorMessage);
		onstop();
		return;
	}
	$.ajax({
        type: "POST",
        url: getCardPINUrl,
        data: JSON.stringify({"code":otp,"sessionToken":pinResponseData.sessionToken,"emailId":pinResponseData.emailId,"userId":pinResponseData.userId,"proxy":pinResponseData.proxy}),
        crossDomain : true,
	    dataType: "json",
		contentType: "application/json",
		"Access-Control-Allow-Origin":"*",
        success: function(data) {
        	if (data.errorDetails[0].errorCode == 0) {
	        	// hide spinner
	            $('.popup-body #loading-spiner').hide();
	            $('.popup-body .p-card-created-data .p-profile-verify-item-fast').hide();
	            var dd = $('.popup-body .p-card-created-data .card-pin dd');
	            dd[0].innerHTML=data.pin;
	            $('.popup-body .p-card-created-data .card-pin').show();
	            otpForPinRetrieving.val('');
	            if (timer == undefined) {
					timer = setTimeout(hideCardInfoGlobal,
							timeShowCardInfoSeconds * 1000);
				}
        	} else {
        		showNotyError(invalidCardPinOtp);
        	}
            onstop();
        },
        error : function(jqXHR,textStatus,errorThrown) {
        	showNotyError(invalidCardPinOtp);
        	onstop();
        }
    });
}

$(document).ready(function() {

	$('[secretvalue]').live('focus', function() {
    	var input = $(this);
    	input.removeClass('secretvalue');
        if (input.val() == input.attr('secretvalue')) {
        	input.val('');
        }
	});
	$('[secretvalue]').live('blur', function() {
        var input = $(this);
        if ((input.val() == '' || input.val() == input.attr('secretvalue'))) {
        	input.addClass('secretvalue');
        	input.val(input.attr('secretvalue'));
        }
	});
	
	$('.lastchar').val('');
	
	if (authorized) {
		/*$("body").bind("mousemove", function() {
	    	clearTimeout(timeout);
			timeout = setTimeout(sessionTimeout, SESSION_TIMEOUT);
		});*/
		
		timeout = setTimeout(sessionTimeout, SESSION_TIMEOUT);
	}
	
	$('.p-profile-api-activation .chzn-drop').live('click', function(e) {
		$('#addMerchantWalletLimit').parent().removeClass('disabled');
		$('#addMerchantWalletLimit').removeAttr('disabled');
	});
	
	// hack for Opera rbm + paste
	if (jQuery.browser.opera) {
		$('.lastchar').live('contextmenu', function(e){
			setTimeout(function() {
				$('.lastchar').trigger(jQuery.Event("keyup", { keyCode: 0 }));	
			}, 2500);
		});
	}
	
	if (window.saveState !== undefined) {
		
		$('#registrationLoginComponentId').change(function() {
			setCookie('reg-login', $(this).val());
		});
		$('#registrationEmailId').change(function() {
			setCookie('reg-email', $(this).val());
		});
		$('#phoneNumberComponentId').live('change', function() {
			setCookie('reg-phone', $(this).val());
		});
		$('#secretOwnQuestion').live('change', function() {
			setCookie('reg-secret-own-question', $(this).val());
		});
		$('#secretAnswerId').live('change', function() {
			setCookie('reg-secret-answer', $(this).val());
		});
		
		if (saveState) {
			regLogin = getCookie('reg-login');
			regEmail = getCookie('reg-email');
			regPhone = getCookie('reg-phone');
			regSecretOwnQuestion = getCookie('reg-secret-own-question');
			regSecretAnswer = getCookie('reg-secret-answer');
			
			if (regLogin != null) {
				$('#registrationLoginComponentId').val(regLogin);
			}
			if (regEmail != null) {
				$('#registrationEmailId').val(regEmail);
			}
			if (regPhone != null) {
				$('#phoneNumberComponentId').val(regPhone);
			}
			if (regSecretOwnQuestion != null) {
				$('#secretOwnQuestion').val(regSecretOwnQuestion);
			}
			if (regSecretAnswer != null) {
				$('#secretAnswerId').val(regSecretAnswer);
			}
		} else {
			setCookie('reg-login', null, new Date(0).toUTCString());
			setCookie('reg-email', null, new Date(0).toUTCString());
			setCookie('reg-phone', null, new Date(0).toUTCString());
			setCookie('reg-secret-own-question', null, new Date(0).toUTCString());
			setCookie('reg-secret-answer', null, new Date(0).toUTCString());
		}
	}
	
	$("#own-question-toggler-link").live('click', function() {
		$("#secretQuestion option[value='1']").attr('selected', 'selected');
		$("#secretQuestion option[value='-1']").removeAttr('selected', 'selected');
		$("#secretQuestion").val('1');
		$(".combo-header").text($("#secretQuestion option[value='1']").text());
		$('#secret-question-toggler').show();
		$("#secret-own-question-toggler").hide();
		$(this).parents('dd').children('.p-reg-form-item-error').hide();
		return false;
	});
	
	$("#secretQuestion").live('change', function () {
	      $("#secretQuestion option[value='1']").attr('selected', 'selected');
	      $("#secretQuestion option[value='-1']").removeAttr('selected', 'selected');
	      $("#secretQuestion").val('1');
	      $(".combo-header").text($("#secretQuestion option[value='1']").text());
	});
	if (window.ownQuestion !== undefined) {
		$("#secretOwnQuestion").attr('placeholder', ownQuestion);
	}
	
	$("#cityComponentIdInput").attr('maxlength', '50');
	$("#companyDescriptionComponentId").attr('maxlength', '250');
	$("#phoneNumberComponentId").live('blur', function () {
	    $(this).parent().parent().children('.p-reg-form-item-tip').hide();
	});
	$("#secretAnswerId").live('blur', function () {
	    $(this).parent().children('.p-reg-form-item-tip').hide();
	});
	$(".phone-number").live('focus', function () {
		$(this).parents('.p-reg-form-item-phone').css('border', '1px solid #45AC26');
	});
	
	$(".phone-number").live('blur', function () {
		$(this).parents('.p-reg-form-item-phone').css('border', '1px solid #E3E2E7');
		$(this).parents('.p-reg-form-item-phone').css('border-top', '1px solid #ADADAF');
	});
	if (jQuery.browser.opera) {
		$(".chzn-search input").live('focus', function () {
			$(this).parents('.chzn-drop').prev().css('border', '1px solid #45AC26');
		});
		$(".chzn-search input").live('blur', function () {
			$(this).parents('.chzn-drop').prev().css('border', '1px solid #E3E2E7');
			$(this).parents('.chzn-drop').prev().css('border-top', '1px solid #ADADAF');
		});
	}

	if ($(".chzn").length) {
		if (jQuery.browser.msie && jQuery.browser.version == 7) $(".chzn").customSelect();
		else $(".chzn").chosen();
	}
	
	
	$('input[name="registrationEmailId"]').on('blur', function() {
		//var $e = $(this);
	 	var $s =  $(this).next('.p-reg-form-item-mailoffer');
		var email = $(this).val();
	 	if (email.length < 75 && !email.endsWith('@finance.pro')) {
			$(this).mailcheck({
		        suggested: function(el, suggestion){
		   			$s.find('a[id="closeLinkId"]').text(suggestion.full);
		   			$s.css('display', 'block');
		   		},
		 		empty: function(){
		  			$s.css('display', 'none');
				}
			});
	 	} else {
	 		$s.css('display', 'none');
	 	}
	});	

	if ($('.p-reg-form-item-combobox').length) {
		$('.p-reg-form-item-combobox a').on('click', function() {
			$(this).parents('.p-reg-form-item-combobox').hide().prev('input').val($(this).text());
		});
	}
		
	if ($('.p-reg-leftcol').length != 0) {
		$('.p-reg-leftcol ul b a').on('click', function(){
			$('.p-reg-leftcol ul li').removeClass('active');
			$(this).parents('li').addClass('active');
		});
	}
	
	if (jQuery.browser.msie) {
		$('[placeholder]').live('focus', function() {
	    	var input = $(this);
	    	input.removeClass('placeholder');
	        if (input.val() == input.attr('placeholder')) {
	        	input.val('');
	        }
		});
		$('[placeholder]').live('blur', function() {
	        var input = $(this);
	        if (input.val() == '' || input.val() == input.attr('placeholder')) {
	        	input.addClass('placeholder');
	        	input.val(input.attr('placeholder'));
	        }
		}).blur();
		$('#regSubmitButton, #makeTransferButtonId').mousedown(function () {
			$(document).find('.placeholder').each(function() {$(this).val('');});
		});
		if (jQuery.browser.version == 7) {
			var zIndexNumber = 100;
			$('.p-reg-form-item').each(function() {
				$(this).css('zIndex', zIndexNumber);
				zIndexNumber -= 1;
			});
		}
	}
	
	refreshIphoneStyleForPasswords();

	/*$('#i-pass input').live('focus', function(e){
		$(e.target).parents('dd').find('.p-reg-form-item-tip').show();
		if ($(e.target).parents().is('#i-pass') && securePasswordGenerated) {
			$('#i-pass .step1').hide();
		}
	});*/

	$('input, textarea').live('focus', function(e) {
		if ($(e.target).is('.p-reg-form-item input:not(".notip"), textarea:not(".notip")') || $(e.target).parents().is('.p-reg-form-item .chzn-container')) {
			$('.p-reg-form-item-tip.noblur').hide();
			$('.p-reg-form-item-error').hide();
			$(e.target).parents('dd, li').find('.p-reg-form-item-tip').show();
		}
		if ($(e.target).parent().is('.p-reg-form-item .chzn-results') || $(e.target).is('option')) {
			$('.p-reg-form-item-tip:not(".noblur")').hide();
		}
		if ($(e.target).parents().is('#i-pass') && securePasswordGenerated) {
			$('#i-pass .step1').hide();
		}
		if (!$(e.target).parents().is('.chzn-search')) {
			if (!$(e.target).parents().is('.input-currency') && !$(e.target).parents().is('.popup-reg-sms-body')) {
				$('.chzn-single').css('border', '1px solid #E3E2E7');
				$('.chzn-single').css('border-top', '1px solid #ADADAF');
			}
		} else {
			if (!$(this).parents().is('.input-currency')) {
				$(this).parents('.chzn-drop').prev().css('border', '1px solid #45AC26');
				if (window.fieldOrder !== undefined) {
					focusFieldId = 'countryComponentId';
					for (id in fieldOrder) {
						if (fieldOrder[focusFieldId] > fieldOrder[id]) {
							validate(id);
						}
					}
				}
			}
		}
		showTip(this);
	});
	$('input, textarea').live('blur', function(e){
		if ($(e.target).parents().is('.p-reg-form-item')) {
			$('.p-reg-form-item-tip:not(".noblur")').hide();
		}
		if ($(e.target).parents().is('.chzn-search') && !$(e.target).parents().is('.input-currency')) {
			$(e.target).parents('.chzn-container').children('.chzn-single').css('border', '1px solid #E3E2E7');
			$(e.target).parents('.chzn-container').children('.chzn-single').css('border-top', '1px solid #ADADAF');
		}
	});
	
	$(document).live('click', function(e) {
		/*if ($(e.target).is('.p-reg-form-item input:not(".notip"), textarea:not(".notip")') || $(e.target).parents().is('.p-reg-form-item .chzn-container')) {
			$('.p-reg-form-item-tip.noblur').hide();
			$(e.target).parents('dd').find('.p-reg-form-item-tip').show();
		}
		if ($(e.target).parent().is('.p-reg-form-item .chzn-results') || $(e.target).is('option')) {
			$('.p-reg-form-item-tip:not(".noblur")').hide();
		}
		if ($(e.target).parents().is('#i-pass') && securePasswordGenerated) {
			$('#i-pass .step1').hide();
		}*/
	});

	initRegSliderCaptcha();

    if ($('select.custom').length != 0)
        $('select.custom').customSelect();

    if ($('.bf').length != 0)
        $('.bf').inputBF();

    // </3 ie
    if (jQuery.browser.msie && jQuery.browser.version <= 8) {
        $('.footer-menu li:nth-child(3n-1)').css('width', '123px');
        $('#p-reg-form-item-pw dt').css('height', '4px');
        $('#p-reg-form-item-pw dt span').css('height', '4px');
    }

    if (jQuery.browser.msie && jQuery.browser.version == 7) {
        var zIndexNumber = 1000;
        $('.p-main-transactions td.type > div').each(function() {
            $(this).css('zIndex', zIndexNumber);
            zIndexNumber -= 1;
        });
    }

    $(document).bind('click', function(e) {
    	if (!$(e.target).parents('.popup').length
    			&& !$(e.target).parents('.selector-wallet, .selector-recent, .selector-card').length
    			&& $(e.target).not('*[onclick*=Popup]').length 
    			&& $(e.target).not('input[onfocus*=Combo]').length 
    			&& $(e.target).not('*[onclick*=Combo]').length
    			&& !$('#noty_center_layout_container').length) {
            ClosePopup();
		}
		if ($(e.target).parents('#walletList li').length) {
		    $(e.target).parents('#walletList li').addClass('wallet-grey');
		}
    });

    $('input[type="checkbox"]').live('change', function() {
        var $checker = $('input[type="checkbox"][name="' + $(this).attr('name') + '_all"]');
        if ($checker.length != 0) {
            if ($(this).attr('name') == 'chain')
                $(this).parents('.p-ticket-chain-header').toggleClass('checked');
            else
                $(this).parents('tr').toggleClass('checked');
            if ($('input[type="checkbox"][name="' + $(this).attr('name') + '"]:checked').size() < $('input[type="checkbox"][name="' + $(this).attr('name') + '"]').size())
                $checker.attr('checked', false);
            else
                $checker.attr('checked', true);
        }
    });
    
    $('#walletList li, .block-cards-item').live('mouseover', function() {
		if ($(this).hasClass('wallet-red')) {
			$(this).removeClass('wallet-red');
			$(this).addClass('wallet-red-hover');
		} else if ($(this).hasClass('wallet-green')) {
			$(this).removeClass('wallet-green');
			$(this).addClass('wallet-green-hover');
		} else {
			$(this).addClass('wallet-grey');
		}
	});
	$('#walletList li, .block-cards-item').live('mouseout', function() {
		if ($(this).hasClass('wallet-red-hover')) {
			$(this).removeClass('wallet-red-hover');
			$(this).addClass('wallet-red');
		} else if ($(this).hasClass('wallet-green-hover')) {
			$(this).removeClass('wallet-green-hover');
			$(this).addClass('wallet-green');
		} else {
			$(this).removeClass('wallet-grey');
		}
	});
	if (jQuery.browser.webkit) {
		$('#txListDataTable tr').live('mouseover', function() {
			if ($(this).hasClass('unread')) {
				$(this).children('td').css('background', '#d8ebd4');
			} else {
				$(this).children('td').css('background', '#f5f5f5');
			}
		});
		$('#txListDataTable tr').live('mouseout', function() {
			if ($(this).hasClass('unread')) {
				$(this).children('td').css('background', '#e8f7e5');
			} else {
				$(this).children('td').css('background', '#ffffff');
			}
		});
	}

    $(".chzn-results").attr('tabindex', -1);

    $(".lastchar").keypress(function(e) {
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code == 13) {
        	$(this).blur();
        }
    });

    $('#merchant-edit-panel input').live('focus', function() {
		$(this).siblings('.checkbox-tip').show();
		$(this).siblings('.error-tip').hide();
		$('#limit-error').hide();
	}).live('blur', function() {
		$(this).siblings('.checkbox-tip').hide();
	});
    
    if (jQuery.browser.msie && jQuery.browser.version <= 8) {
	    $('.toggler label input:radio').live('change', function() {
			if ($(this).val() == 1) {
				$('.toggler label input:radio[name="' + $(this).attr('name') +'"][value="0"]').removeClass('checked');
				$('.toggler label input:radio[name="' + $(this).attr('name') +'"][value="1"]').addClass('checked');
			} else {
				$('.toggler label input:radio[name="' + $(this).attr('name') +'"][value="1"]').removeClass('checked');
				$('.toggler label input:radio[name="' + $(this).attr('name') +'"][value="0"]').addClass('checked');
			}
		});
	    if ($('.p-settings-main').length) {
	    	$('.p-settings-main').children().last().css('border-bottom', 'none');
	    }
	    $('.p-reg-form-item-error').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	    $('.p-reg-form-item-tip').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	    $('.p-reg-form-item-tip-ip').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	    $('.checkbox-tip').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	    $('.error-tip').prepend('<i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i>');
	}

	$('body').on('focus', '.input-amount', function (event) {
    	var input = $(this);
    	var val = input.val();
    	if (val == '0.00') {
    		input.val('');
    	}
    });

	$('body').on('blur', '.input-amount', function (event) {
    	var input = $(this);
    	if (!input.hasClass('btc')) {
    		formatAmount(input);
    	}
    });

    $('body').on('keypress', '.input-amount', function (event) {
    	var charCode = event.charCode;
    	var keyCode = event.keyCode;
    	if (charCode == undefined) {
    		charCode = keyCode;
    	}
    	if ((charCode >= 48 && charCode <= 57) // - numbers
    			|| (keyCode >= 37 && keyCode <= 40) // - arrows
    			|| ((event.ctrlKey || event.metaKey) && charCode == 97) // - ctrl + a
    			|| ((event.ctrlKey || event.metaKey) && charCode == 99) // - ctrl + c
    			|| ((event.ctrlKey || event.metaKey) && keyCode == 45) // - ctrl + insert
    			|| ((event.ctrlKey || event.metaKey) && charCode == 118) // - ctrl + v
    			|| ((event.ctrlKey || event.metaKey) && charCode == 122) // - ctrl + z
    			|| keyCode == 8 // - backspace
    			|| keyCode == 46 // - delete
    			|| keyCode == 35 // - end
    			|| keyCode == 36 // - home
    			|| keyCode == 9) { // - tab
    	} else if (charCode == 44 || charCode == 46) { // 44 - comma, 46 - dot
    		var val = $(this).val();
    		if (val.indexOf('.') != -1) {
    			event.preventDefault();
    		} else {
    			var selectionStart = this.selectionStart;
    			$(this).val(val.insert(selectionStart, '.'));
    			this.selectionStart = selectionStart + 1;
    			this.selectionEnd = selectionStart + 1;
    			event.preventDefault();
    		}
    	} else {
    		event.preventDefault();
    	}
	});

    $('body').on('keypress', '.input-number', function (event) {
//		var key = event.charCode || event.keyCode || 0;
//		// allow backspace, tab, delete, arrows,
//		// numbers and keypad numbers ONLY
//		// home, end, period, and numpad decimal
//		if (!event.shiftKey && ((event.ctrlKey || event.metaKey) || key == 46 || key == 8 || key == 9
//				|| key == 110
//				|| (key >= 35 && key <= 40)
//				|| (key >= 48 && key <= 57))) {
//		} else {
//			event.preventDefault();
//		}
	});

    $('input[type="checkbox"][name="chain_sent"]').live('click', function(event) {
    	if (!$(this).prop("checked")) {
    		document.getElementById('main_sent').checked = false;
    	}
    	if ($('input[type="checkbox"][name="chain_sent"]').not(':checked').length > 0) {
		
	} else {
		document.getElementById('main_sent').checked = true;
	}
    });
    
    $('input[type="checkbox"][name="chain_inbox"]').live('click', function(event) {
    	if (!$(this).prop("checked")) {
    		document.getElementById('main_inbox').checked = false;
    	}
    	if ($('input[type="checkbox"][name="chain_inbox"]').not(':checked').length == 0) {
    		document.getElementById('main_inbox').checked = true;
    	}
    });
    
    $('.block-link').live('click', function(event) {
    	return false;
    });
    
    $('body').on('change', '.p-transfer-form .f-item.legal input', function () {
		var input = $(this);
		input.parents('.legal').find('.active').removeClass('active');
		input.parents('li').addClass('active');
		if (input.val() == 'true') {
			$('#legal').click();
			$('#male').click();
		} else {
			$('#individual').click();
			$('#female').click();
		}
	});

    $('body', this).on('input paste', '#cardNumber1, .cardNumber1', function(e) {
    	e.preventDefault();
        var pastedText = undefined;
        if (e.originalEvent.clipboardData) {
        	pastedText = (e.originalEvent || e).clipboardData.getData('text/plain');
        } else if (window.clipboardData) {
        	pastedText = window.clipboardData.getData('Text');
        }
        if (pastedText) {
        	pastedText = pastedText.replace(/ /g, "");
        	if (new RegExp(/^(\d{14}|\d{15}|\d{16}|\d{18}|\d{19})$/).test(pastedText)) {
        		var array = pastedText.match(/.{1,4}/g);
        		var input = $(this);
        		var prefix = input.attr('id') == 'cardNumber1' ? '#' : '.';
        		$(prefix + 'cardNumber1').val(array[0]);
        		$(prefix + 'cardNumber2').val(array[1]);
        		$(prefix + 'cardNumber3').val(array[2]);
        		if (array.length == 4) {
        			$(prefix + 'cardNumber4').val(array[3]);
        		} else if (array.length == 5) {
        			$(prefix + 'cardNumber4').val(array[3] + array[4]);
        		}
        		cardNumberFocus(prefix + 'cardNumber4', true);
        	}
        }
    });

    $('body', this).on('keyup', '#cardNumber1', function(event) {
    	var input = $(this);
    	if (input.val().length == 4) {
    		var keyCode = event.keyCode;
    		if (keyCode != 37 && keyCode != 39) {
    			cardNumberFocus('#cardNumber2', false);
    		}
    	}
    });
    
    $('body', this).on('keyup', '#cardNumber2', function(event) {
    	var input = $(this);
    	if (input.val().length == 4) {
    		var keyCode = event.keyCode;
    		if (keyCode != 37 && keyCode != 39) {
    			cardNumberFocus('#cardNumber3', false);
    		}
    	} else if (input.val().length == 0) {
    		var keyCode = event.keyCode;
        	if (keyCode == 8 // - backspace
        			|| keyCode == 46 ) { // - delete
        		cardNumberFocus('#cardNumber1', true);
        	}
    	}
    });
    
    $('body', this).on('keyup', '#cardNumber3', function(event) {
    	var input = $(this);
    	if (input.val().length == 4) {
    		var keyCode = event.keyCode;
    		if (keyCode != 37 && keyCode != 39) {
    			cardNumberFocus('#cardNumber4', false);
    		}
    	} else if (input.val().length == 0) {
        	var keyCode = event.keyCode;
        	if (keyCode == 8 // - backspace
        			|| keyCode == 46 ) { // - delete
        		cardNumberFocus('#cardNumber2', true);
        	}
    	}
    });
    
    $('body', this).on('keyup', '#cardNumber4', function(event) {
    	var input = $(this);
    	if (input.val().length == 0) {
	    	var keyCode = event.keyCode;
	    	if (keyCode == 8 // - backspace
	    			|| keyCode == 46 ) { // - delete
	    		cardNumberFocus('#cardNumber3', true);
	    	}
    	}
    });

    function cardNumberFocus(selector, end) {
		var input = $(selector);
		// if (input.val().length != 4) {
		// input.focus();
		// }
		input.focus();
		if (end === true) {
			input.val(input.val());
		}
	}

    $('body', this).on('keyup', '.cardNumber1', function(event) {
    	var input = $(this);
    	if (input.val().length == 4) {
    		var keyCode = event.keyCode;
    		if (keyCode != 37 && keyCode != 39) {
    			cardNumberFocus('.cardNumber2', false);
    		}
    	}
    });

    $('body', this).on('keyup', '.cardNumber2', function(event) {
    	var input = $(this);
    	if (input.val().length == 4) {
    		var keyCode = event.keyCode;
    		if (keyCode != 37 && keyCode != 39) {
    			cardNumberFocus('.cardNumber3', false);
    		}
    	} else if (input.val().length == 0) {
    		var keyCode = event.keyCode;
        	if (keyCode == 8 // - backspace
        			|| keyCode == 46 ) { // - delete
        		cardNumberFocus('.cardNumber1', true);
        	}
    	}
    });

    $('body', this).on('keyup', '.cardNumber3', function(event) {
    	var input = $(this);
    	if (input.val().length == 4) {
    		var keyCode = event.keyCode;
    		if (keyCode != 37 && keyCode != 39) {
    			cardNumberFocus('.cardNumber4', false);
    		}
    	} else if (input.val().length == 0) {
        	var keyCode = event.keyCode;
        	if (keyCode == 8 // - backspace
        			|| keyCode == 46 ) { // - delete
        		cardNumberFocus('.cardNumber2', true);
        	}
    	}
    });
    
    $('body', this).on('keyup', '.cardNumber4', function(event) {
    	var input = $(this);
    	if (input.val().length == 0) {
	    	var keyCode = event.keyCode;
	    	if (keyCode == 8 // - backspace
	    			|| keyCode == 46 ) { // - delete
	    		cardNumberFocus('.cardNumber3', true);
	    	}
    	}
    });
    
    $('body', this).on('keyup', '#loginInput', function(event) {
    	var input = $(this);
    	if (input.val().length == 0) {
	    	$('#cardChooserPanel').hide();
    	} else {
    		$('#cardChooserPanel').show();
    	}
    });
    
    $("#helpLimitInfo").click(function(){
        $("#helpLimitInfo").parent().find(".p-reg-form-item-tip").toggle();
    });

    $('body', this).on('change', '#agreement_checkbox, #agreement_checkbox_corp', function() {
        if($(this).is(":checked")) {
            $('.p-card-creating-submit .button, .p-card-creating-sms  .button').removeClass("disabled")
        } else {
            $('.p-card-creating-submit .button, .p-card-creating-sms  .button').addClass("disabled")
        }
    });

    $('body', this).on('click', '.p-inside-transactions-header-filter a.active', function(event) {
    	var element = $(this);
    	element.parents('ul').find('li').removeClass('active');
    	element.parents('ul').find('li a').addClass('dn');
    	element.parents('ul').find('li a.fake').removeClass('dn');
    	element.parents('li').addClass('active');
    });
    
    $('body', this).on('click', 'input:radio[name=commission_pays]', function(event) {
    	$('#merchantPaysFee').val($(this).val());
    });
    
    $('body', this).on('click', '#otpForPinRetrievingSubmit', function(event) {
    	a4jcommandButton = $(this);
    	if (a4jcommandButton.is('input[type="submit"]')) {
    		$('input[type="submit"]:not(#verificationButtonThirdLevel, .enrollToken)').attr('disabled', 'disabled');
    		ajaxStatus(a4jcommandButton);
    	} else {
    		a4jcommandButton = null;
    	}
    	getCardPIN();
    });

});

function blockSpecialChar(e){
	var k;
	document.all ? k = e.keyCode : k = e.which;
	return ((k > 64 && k < 91) || (k > 96 && k < 123) || k == 8 || k == 32 || (k >= 48 && k <= 57));
}

