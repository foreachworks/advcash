;(function () {
  var shouldHeaderBeTransparent = $('body').hasClass('promo') || $('body').hasClass('business');
  var classNameTransparent = '-transparent';

  if (shouldHeaderBeTransparent) {
    var $headerContainer = $('.header-container');
    var isHeaderTransparent = $headerContainer.hasClass(classNameTransparent);
    var offset = 100;

    $(window).on('scroll', function () {
      var scrollPos = $(this).scrollTop();

      if (isHeaderTransparent && scrollPos >= offset) {
        $headerContainer.removeClass(classNameTransparent);
        isHeaderTransparent = false;
      }
      if (!isHeaderTransparent && scrollPos < offset) {
        $headerContainer.addClass(classNameTransparent);
        isHeaderTransparent = true;
      }
    });
  } else {
    $('.header-container').removeClass(classNameTransparent);
  }
})();

// РљР°Р»СЊРєСѓР»СЏС‚РѕСЂ РєСЂРёРїС‚С‹
;(function () {
  var $calc = $('.crypto__calc');
  var $errorContainer = $('.crypto__calc-error');
  var $resultContainer = $('.crypto__calc-result');

  var sourceInput = $('.crypto__calc-form-input-field[data-action="SELL"]');
  var resultInput= $('.crypto__calc-form-input-field[data-action="BUY"]');

  var currentTab = 'buy';
  var model = {
    from: 'USD',
    to: 'BTC',
    amount: 100,
    action: 'SELL',
  };

  var API = 'https://wallet.advcash.com/ws/crypto-currency-exchange-calculator?';
  var request;

  if (!$calc.length) {
    return;
  }

  function resetError() {
    $('.crypto__calc-form-input').removeClass('-is-error');
    $errorContainer.html('');
  }

  function setError(text) {
    sourceInput.parent().addClass('-is-error');
    $errorContainer.html(text);
    resetForm();
  }

  function resetForm() {
    $resultContainer.addClass('-hidden');
    resultInput.val('').parent().removeClass('-is-filled');
  }

  function setResult(data) {
    var amount = 0;

    if (currentTab === 'buy') {
      if (model.action === 'SELL') {
        amount = data.amount;
      } else {
        amount = data.amountExchanged;
      }
    } else {
      if (model.action === 'SELL') {
        amount = data.amountExchanged;
      } else {
        amount = data.amount;
      }
    }

    var fiatValue = $('.crypto__calc-form-select[data-type="fiat"] .crypto__calc-form-select-handler-value').html();
    var cryptoValue = $('.crypto__calc-form-select[data-type="crypto"] .crypto__calc-form-select-handler-value').html();

    $('[calc-result-amount]').inputmask('setvalue', amount);
    $('[calc-result-exchanged]').inputmask('setvalue', data.amountExchanged);
    // if (data.amountBankCard || data.amountAdvCard) {
    //   $('[calc-result-card]').inputmask('setvalue', data.amountBankCard || data.amountAdvCard);
    //   $('[calc-result-card]').parent().removeClass('-hidden');
    // } else {
    //   $('[calc-result-card]').parent().addClass('-hidden');
    // }
    // $('[calc-result-other]').inputmask('setvalue', data.amountOtherMethods);
    $('[calc-result-rate]').inputmask('setvalue', data.rate);
    $('[calc-result-from]').html(cryptoValue);
    $('[calc-result-to]').html(fiatValue);
    resultInput.inputmask('setvalue', data.amountExchanged);
    resultInput.parent().addClass('-is-filled');
  }

  function loadRate() {
    resetError();

    if (request && typeof request.abort === 'function') {
      request.abort();
    }

    if (!sourceInput.val().length) {
      return $resultContainer.addClass('-hidden');
    }

    request = $.get(API, model, function (res) {
      if (res.status === 'ERROR') {
        return setError(res.message);
      }

      setResult(res);
      $resultContainer.removeClass('-hidden');
    });
  }

  $('.crypto__subhead button', $calc).on('click', function (e) {
    var nextTab = $(this).data().tab;

    if (nextTab === currentTab) {
      return;
    }

    var fiatValue = $('.crypto__calc-form-select[data-type="fiat"] .crypto__calc-form-select-handler-value').html();
    var cryptoValue = $('.crypto__calc-form-select[data-type="crypto"] .crypto__calc-form-select-handler-value').html();
    var sellInput = $('.crypto__calc-form-input-field[data-action="SELL"]');
    var buyInput= $('.crypto__calc-form-input-field[data-action="BUY"]');

    if (nextTab === 'buy') {
      model = {
        from: fiatValue,
        to: cryptoValue,
        amount: 100,
        action: 'SELL',
      };
      sellInput.val(100).addClass('-is-filled');
    } else {
      model = {
        from: cryptoValue,
        to: fiatValue,
        amount: 1,
        action: 'SELL',
      };
      sellInput.val(1).addClass('-is-filled');
    }

    sourceInput = sellInput;
    resultInput = buyInput;
    buyInput.val('').removeClass('-is-filled');
    $resultContainer.addClass('-hidden');
    $calc.removeClass('-tab-' + currentTab).addClass('-tab-' + nextTab);
    currentTab = nextTab;

    setTimeout(loadRate);
  });

  $('.crypto__calc-form-input-field').on('focus', function (e) {
    var input = $(this);

    model.action = input.data().action;
    sourceInput = input;
    resultInput = $('.crypto__calc-form-input-field').not(':focus');
  });

  $('.crypto__calc-form-input-field').on('focusin focusout', function (e) {
    var action = e.type === 'focusin' ? 'addClass' : 'removeClass';

    $(this).parent()[action]('-is-focused');
  });

  $('.crypto__calc-form-input-field').on('input', function (e) {
    var value = e.target.value;
    var isEmpty = !value.length;
    var action = isEmpty ? 'removeClass' : 'addClass';

    if (isEmpty) {
      resetForm();
    }

    model.amount = value.replace(/[^0-9\.]/g, '');
    $(this).parent()[action]('-is-filled');
    loadRate();
  });

  $('.crypto__calc-form-select-handler').on('click', function () {
    var $select = $(this).parent();

    if ($select.hasClass('-active')) {
      $select.removeClass('-active');
    } else {
      $('.crypto__calc-form-select.-active').removeClass('-active');
      $select.addClass('-active');
    }
  });

  $('.crypto__calc-form-select button').on('click', function () {
    var $button = $(this);
    var $select = $button.parents('.crypto__calc-form-select');
    var type = $select.data().type;
    var value = $button.html();

    if (currentTab === 'buy') {
      if (type === 'fiat') {
        model.from = value;
      } else {
        model.to = value;
      }
    } else {
      if (type === 'crypto') {
        model.from = value;
      } else {
        model.to = value;
      }
    }

    $button.parent().addClass('-active').siblings('.-active').removeClass('-active');
    $select.removeClass('-active');
    $('.crypto__calc-form-select-handler-value', $select).html(value);
    loadRate();
  });

  $(document).on('focusin click', function (e) {
    var $select = $(e.target).parents('.crypto__calc-form-select');

    if ($select.length) {
      return;
    }

    $('.crypto__calc-form-select.-active').removeClass('-active');
  });

  $('.crypto__calc-form-input-field, [calc-result-amount], [calc-result-card], [calc-result-exchanged], [calc-result-rate]', $calc).inputmask({
    alias: 'numeric', 
    groupSeparator: ' ',
    allowMinus: false,  
    digits: 6,
    max: 999999999.999999,
    rightAlign: false,
    showMaskOnHover: false,
    showMaskOnFocus: false,
    onBeforePaste: function (pastedValue) {
      var processedValue = pastedValue.replace(/\,/g, '.').replace(/[^\.0-9]/g, '');

      sourceInput = this.$el;
      sourceInput.addClass('-is-filled');
      model.action = sourceInput.data().action;
      model.amount = processedValue;
      resultInput = $('.crypto__calc-form-input-field').not(':focus');
      resultInput.removeClass('-is-filled');
      
      setTimeout(loadRate);

      return processedValue;
    }
  });

  loadRate();
})();

// РўР°СЂРёС„С‹
;(function () {
  $('.fees__tabs-tab').on('click', function () {
    var btn = $(this);
    var content = $('.fees__content.-' + btn.data().for);

    btn.addClass('-active');
    btn.siblings().removeClass('-active');
    content.addClass('-active');
    content.siblings().removeClass('-active');
  });
})();

// ADV + Binance
;(function () {
  var canvasWidth = 1440;
  var canvasHeight = 876;
  var isMouseOnCanvas = false;
  var isMouseMoving = false;
  var isDebounce = false;
  var stopTimeout = null;

  var circles = {
    green: $('.binance-collab__bg-green'),
    yellow: $('.binance-collab__bg-yellow'),
    black: $('.binance-collab__bg-black'),
  }

  var ratio = {
    green: { x: -0.5, y: 1.2 },
    yellow: { x: 0.8, y: 1.5 },
    black: { x: 1.2, y: 1 },
  };

  var x = 1125;
  var y = 475;
  var directionX = 1;
  var directionY = 1;

  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  function updateCircles(positions) {
    ['green', 'yellow', 'black'].forEach(function (color) {
      circles[color].css(
        'transform',
        'translate(' + Math.round(positions[color].x) + 'px, ' + Math.round(positions[color].y) + 'px)'
      );
    });
  }

  function pointerMoved() {
    var minMouseX = 0;
    var maxMouseX = window.innerWidth;
    var mouseX = Math.max(minMouseX, Math.min(x, maxMouseX));
    var mouseY = Math.min(y - window.scrollY, canvasHeight);

    updateCircles({
      green: {
        x: (mouseX - window.innerWidth / 2) * ratio.green.x,
        y: (canvasHeight / 2 - mouseY) * ratio.green.y,
      },
      yellow: {
        x: (mouseX * 0.9 - window.innerWidth / 2) * ratio.yellow.x,
        y: mouseY - canvasHeight / 2 * ratio.yellow.y,
      },
      black: {
        x: window.innerWidth / 2 - 250,
        y: (canvasHeight / 2 - mouseY) * ratio.black.y,
      },
    });
  }

  function handleMouseMove(e) {
    isMouseMoving = true;
    clearTimeout(stopTimeout);

    x = e.pageX;
    y = e.pageY;

    if (isMouseOnCanvas && !isDebounce) {
      pointerMoved();
      isDebounce = true;
      setTimeout(function () {
        isDebounce = false;
      }, 150);
    }

    stopTimeout = setTimeout(function () {
      isMouseMoving = false;
    }, 500);
  }

  function handleHover(e) {
    if (e.type === 'mouseenter') {
      isMouseOnCanvas = true;
    }
    if (e.type === 'mouseleave') {
      isMouseOnCanvas = false;
    }
  }

  $(window).on('mousemove', handleMouseMove);
  $('.binance-collab').on('mouseenter mouseleave', handleHover);

  setInterval(function () {
    if (!isMouseMoving || !isMouseOnCanvas) {
      var minMouseX = 0;
      var maxMouseX = window.innerWidth;
      var minMouseY = 0;
      var maxMouseY = canvasHeight;

      x += Math.sin(Math.random()) * directionX * 150;
      y += Math.cos(Math.random()) * directionY * 150;

      if (x > maxMouseX) {
        directionX = -1;
      }
      if (x < minMouseX) {
        directionX = 1;
      }
      if (y > maxMouseY) {
        directionY = -1;
      }
      if (y < minMouseY) {
        directionY = 1;
      }

      pointerMoved();
    }
  }, 1000);
})();