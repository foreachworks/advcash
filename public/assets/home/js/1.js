;(function () {
  var isPromo = $('body').hasClass('promo');
  var classNameTransparent = '-transparent';

  if (isPromo) {
    var $headerContainer = $('.header-container');
    var isHeaderTransparent = $headerContainer.hasClass(classNameTransparent);
    var offset = 100;

    $(window).on('scroll', function () {
      var scrollPos = $(this).scrollTop();

      if (isHeaderTransparent && scrollPos >= offset) {
        $headerContainer.removeClass(classNameTransparent);
        isHeaderTransparent = false;
      }
      if (!isHeaderTransparent && scrollPos < offset) {
        $headerContainer.addClass(classNameTransparent);
        isHeaderTransparent = true;
      }
    });
  } else {
    $('.header-container').removeClass(classNameTransparent);
  }
})();