<?php

namespace App\Http\Controllers;

use App\Http\Requests\PaymentStoreRequest;
use App\Http\Requests\PaymentUpdateRequest;
use App\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller
{
    public function index()
    {
        $transactions = Payment::paginate(10);

        return view('dashboard.transactions.index', compact('transactions'));
    }

    public function create()
    {
        return view('dashboard.transactions.create');
    }

    public function edit($paymentId)
    {
        $payment = Payment::find($paymentId);
        return view('dashboard.transactions.edit', compact('payment'));
    }

    /**
     * @param PaymentUpdateRequest $request
     * @param $paymentId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function update(PaymentUpdateRequest $request, $paymentId)
    {
        DB::beginTransaction();
        try {
            $payment = Payment::find($paymentId);
            $data = $request->except('_token', '_method');
            $data['date_done'] = Carbon::parse($request->date_done);
            $payment->update($data);

            DB::commit();

            return redirect()->route('transactions.index');
        } catch (\Throwable $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * @param PaymentStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function store(PaymentStoreRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $request->except('_token');
            if (isset($data['date_from']) && isset($data['date_to'])){
                $dateFrom = Carbon::parse($data['date_from'] . ' ' . $data['time']);
                $dateTo = Carbon::parse($data['date_to'] . ' ' . $data['time']);
                $diff = $dateFrom->diffInDays($dateTo);
                $j = 0;
                for($i = 0; $i<=$diff; $i++){
                    $data['date_done'] = $dateFrom->add($j, 'day');
                    $data['transaction_id'] = $this->getToken(8, rand(1, 999)) . '-' . $this->getToken(4, rand(1, 999)) . '-' . $this->getToken(4, rand(1, 999)) . '-' . $this->getToken(4, rand(1, 999)) . '-' . $this->getToken(12, rand(1, 999));
                    if ($j == 0){
                        $j++;
                    }
                    Payment::create($data);
                }

            } else {
                $data['date_done'] = Carbon::parse($request->date_done);
                $data['transaction_id'] = $this->getToken(8, rand(1, 999)) . '-' . $this->getToken(4, rand(1, 999)) . '-' . $this->getToken(4, rand(1, 999)) . '-' . $this->getToken(4, rand(1, 999)) . '-' . $this->getToken(12, rand(1, 999));
                Payment::create($data);
            }

            DB::commit();

            return redirect()->route('transactions.index');
        } catch (\Throwable $e) {
            DB::rollback();

            throw $e;
        }
    }

    /**
     * @param $paymentId
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public  function destroy($paymentId)
    {
        DB::beginTransaction();
        try {
            $payment = Payment::find($paymentId);
            $payment->delete();

            DB::commit();

            return redirect()->route('transactions.index');
        } catch (\Throwable $e) {
            DB::rollback();

            throw $e;
        }
    }

    private function getToken($length, $seed){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "0123456789";

        mt_srand($seed);      // Call once. Good since $application_id is unique.

        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
        }
        return mb_strtolower($token);
    }
}
