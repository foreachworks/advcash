<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->check()){
            $query = \request()->query;
            $count = count($query) > 0 ? (int)$query->get('q') : 10;
//            $transactions = Payment::orderBy('date_done', 'desc')->take($count)->get();
			$trans = DB::table('payments')->count();
			$transactions = DB::table('payments')->orderBy('date_done', 'desc')->get();

//            return view('account.index', ['transactions' => $transactions, 'trans' => $trans]);
            return view('account.history-transactions', ['transactions' => $transactions, 'trans' => $trans, 'count' => $count]);
        } else {
            return view('welcome1');
        }
    }

    public function refill(){
        return view('account.refill');
    }

    public function transaction(){
        return view('account.transaction');
    }

    public function historyTransactions(){
        $query = \request()->query;
        $count = count($query) > 0 ? (int)$query->get('q') : 10;
//        $transactions = Payment::orderBy('date_done', 'desc')->take($count)->get();
		$trans = DB::table('payments')->count();
		$transactions = DB::table('payments')->orderBy('date_done', 'desc')->get();

        return view('account.history-transactions', ['transactions' => $transactions, 'trans' => $trans, 'count' => $count]);
    }

	
}
