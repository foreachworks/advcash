<?php

namespace App\Http\Controllers;


use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard.index');
    }

    public function editAccount()
    {
        $user = auth()->user();

        return view('dashboard.account.index', compact('user'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Throwable
     */
    public function accountUpdate(Request $request)
    {
        DB::beginTransaction();
        try {
            $user = User::find(auth()->user()->id);
            $user->update($request->except('_token'));

            DB::commit();

            return back();
        } catch (\Throwable $e) {
            DB::rollback();

            throw $e;
        }

    }
}