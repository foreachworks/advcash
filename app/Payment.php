<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
            'type_operation',
            'input',
            'number_purse',
            'amount',
            'status',
            'from_email',
            'transaction_id',
            'date_done',
            'comment',
        ];
}
