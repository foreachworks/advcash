<!DOCTYPE html>
<html class="ua-windows_nt ua-windows_nt-10 ua-windows_nt-10-0 ua-gecko ua-gecko-68 ua-gecko-68-0 ua-firefox ua-firefox-68 ua-firefox-68-0 ua-desktop ua-desktop-windows js js opacity csscolumns cssgradients csstransforms csstransitions" lang="ru">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>advcash платежная платформа: онлайн-кошельки, массовые выплаты, prepaid-карты</title>
    <meta name="description" content="Универсальная платежная платформа. Криптовалюты, карты, бесплатные внутренние переводы. Решения для мерчантов. Низкие комиссии. Создайте аккаунт бесплатно.">
    <meta name="og:description" content="Универсальная платежная платформа. Криптовалюты, карты, бесплатные внутренние переводы. Решения для мерчантов. Низкие комиссии. Создайте аккаунт бесплатно.">
    <meta name="og:image" content="index_files/social.png">
    <meta name="og:type" content="website">
    <meta name="og:title" content="Advanced Cash">
    <link rel="image_src" href="{{ asset('assets/home/index_files/social.png') }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@advcash">
    <meta name="twitter:title" content="Advanced Cash">
    <meta name="twitter:description" content="Универсальная платежная платформа. Криптовалюты, карты, бесплатные внутренние переводы. Решения для мерчантов. Низкие комиссии. Создайте аккаунт бесплатно.">
    <meta name="twitter:image" content="img/social.png">
    <!-- <meta name="viewport" content="width=1440, initial-scale=1"> -->


    <link rel="stylesheet" type="text/css" href="{{ asset('assets/home/css/common.css') }}">
    <link href="{{ asset('assets/home/css/css.css') }}" rel="stylesheet" type="text/css">
    <!--[if (gte IE 6)&(lte IE 8)]><script type="text/javascript" src="/js/selectivizr.js"></script><![endif]-->
    <!--[if lt IE 9]><script src="/js/html5shiv.js"></script><![endif]-->
    <link rel="shortcut icon" href="{{ asset('assets/home/img/favicon.ico') }}">
</head>

<body class="promo">
<div class="wrapper">
    <div class="header-container -transparent">
        <header>
            <a class="logo"><img src="{{ asset('assets/home/img/logo-new.svg') }}" alt="ADVCash"></a>
            <nav>
                <ul>
                    <li class="multi-level"><a href="https://advcash.com/about/">О компании</a>
                        <ul>
                            <li><a href="https://advcash.com/about/company/">Всегда впереди</a></li>
                            <li><a href="https://advcash.com/about/exchange/">Обменные пункты</a></li>
                            <li><a href="https://advcash.com/about/partners/">Партнёры</a></li>
                        </ul>
                    </li>
					<li class="new"><a href="https://advcash.com/business/">Бизнесу</a></li>
                    <li><a href="https://advcash.com/solutions/">Решения</a></li>
                    <li></li>
                    <li><a href="https://advcash.com/fees/">Тарифы</a></li>
                    <li><a href="https://advcash.com/solutions/card/">Карта</a></li>
                    <li><a href="https://advcash.com/faq/">FAQ</a></li>
                    <li><a href="https://advcash.com/contacts/">Контакты</a></li>
                </ul>
            </nav>
            <div class="button">
                <ul class="stocks">
                    <li><span class="name">USD/EUR</span><span class="value down">0.93</span></li>
                    <li><span class="name">USD/GBP</span><span class="value up">0.8</span></li>
                    <li><span class="name">USD/RUB</span><span class="value down">65.13</span></li>
                    <li><span class="name">EUR/USD</span><span class="value down">1.13</span></li>
                    <li><span class="name">EUR/GBP</span><span class="value down">0.88</span></li>
                    <li><span class="name">EUR/RUB</span><span class="value up">71.78</span></li>
                    <li><span class="name">GBP/USD</span><span class="value up">1.31</span></li>
                    <li><span class="name">GBP/EUR</span><span class="value up">1.19</span></li>
                    <li><span class="name">GBP/RUB</span><span class="value up">83.6</span></li>
                    <li class="btc"><span class="name">BTC/USD</span><span class="value up">7278.2667</span></li>
                    <li class="btc"><span class="name">BTC/EUR</span><span class="value up">6617.785</span></li>
                    <li class="btc"><span class="name">BTC/RUB</span><span class="value up">464725.09</span></li>
                    <li><span class="name">ETH/USD</span><span class="value up">152.1283</span></li>
                    <li><span class="name">ETH/EUR</span><span class="value up">137.92</span></li>
                    <li><span class="name">LTC/USD</span><span class="value up">47.6045</span></li>
                    <li><span class="name">BCH/USD</span><span class="value up">212.0533</span></li>
                    <li><span class="name">XRP/USD</span><span class="value up">0.2327</span></li>
                    <li><span class="name">ZEC/USD</span><span class="value up">29.5873</span></li>
				</ul><a href="https://advcash.com/en/" class="lang">Eng</a></div><a href="{{ route('login') }}" class="header__login">Вход</a><a href="#" class="header__register">Регистрация</a></header>
				
    </div>
    <main>
        <div class="adv-card-released">
            <div class="adv-card-released__notice">Электронные кошельки, карты, массовые выплаты, прием платежей</div>
            <h1 class="adv-card-released__headline">Фрилансерам, вебмастерам,<br>онлайн-бизнесу</h1>
            <div class="adv-card-released__cards">
                <a href="https://advcash.com/solutions/card/" class="adv-card-image">
                    <img src="{{ asset('assets/home/img/plastic2x.png') }}"   alt="" width="413">
                </a>
                <a href="https://advcash.com/solutions/card/" class="adv-card-image">
                    <img src="{{ asset('assets/home/img/virtual2x.png') }}" alt="" width="413">
                </a>
            </div>
            <div class="adv-card-released__more"><a href="https://advcash.com/solutions/card/">Подробнее <img src="{{ asset('assets/home/img/arrow-more.svg') }}" alt="→"></a></div>
        </div>
        <!--div class="adv-card-soon"><div class="adv-card-soon__cards"><a href="/solutions/card/" class="adv-card-image"><img src="/i/1.1/card/plastic.png" srcset="/i/1.1/card/plastic.png 1x, /i/1.1/card/plastic@2x.png 2x, /i/1.1/card/plastic@3x.png 3x" alt=""></a><a href="/solutions/card/" class="adv-card-image"><img src="/i/1.1/card/virtual.png" srcset="/i/1.1/card/virtual.png 1x, /i/1.1/card/virtual@2x.png 2x, /i/1.1/card/virtual@3x.png 3x" alt=""></a></div><div class="adv-card-soon__more"><a href="/solutions/card/">Подробнее <img src="/i/1.1/arrow-more.svg" alt="→"></a></div></div-->
        <div class="promo-intro">
            <h2>Деньги + интернет<br>просто и безопасно</h2>
            <p>Управляйте вашими онлайн-финансами с помощью advcash. Получайте выплаты, пользуйтесь картами, пополняйте счет и выводите средства множеством удобных способов. Быстро. Выгодно. Без проблем.</p><img src="img/intro.svg" alt="" width="937" height="299"><a href="https://wallet.advcash.com/ru/register">Стать клиентом</a></div>
        <section class="abilities">
            <h2>Возможности advcash</h2>
            <ul>
                <li>
                    <figure><img src="{{ asset('assets/home/img/3.svg') }}" alt="Выгодно пополнить"></figure>
                    <h3>Выгодно пополнить</h3>
                    <p>Банковские карты и переводы, онлайн-банкинг, е-валюты, наличные и многое другое. Всегда найдется способ быстро и выгодно пополнить ваш счет в advcash, где бы вы ни были.</p>
                </li>
                <li>
                    <figure><img src="{{ asset('assets/home/img/2.svg') }}" alt="Удобно использовать"></figure>
                    <h3>Удобно использовать</h3>
                    <p>Несколько валют в одном аккаунте. Бесплатные внутренние переводы. Мгновенное пополнение ADV-карт. Многоуровневая система безопасности. Быстрая верификация и поддержка.</p>
                </li>
                <li>
                    <figure><img src="{{ asset('assets/home/img/1.svg') }}" alt="Легко вывести"></figure>
                    <h3>Легко вывести</h3>
                    <p>Виртуальные и пластиковые prepaid-карты в разных валютах. Быстрые выплаты на локальные и международные карты Visa/MC.</p>
                </li>
            </ul><a class="register" href="https://wallet.advcash.com/ru/register">Стать клиентом</a></section>
        <div class="partners"><img src="{{ asset('assets/home/img/visa.svg') }}" alt="Visa"><img src="{{ asset('assets/home/img/swift.svg') }}" alt="Swift"><img src="{{ asset('assets/home/img/mir.svg') }}" alt="Мир"><img src="{{ asset('assets/home/img/sepa.svg') }}" alt="Sepa"><img src="{{ asset('assets/home/img/qiwi.svg') }}" alt="Qiwi"><img src="{{ asset('assets/home/img/mastercard.svg') }}" alt="Master Card"><img src="{{ asset('assets/home/img/yandexmoney.svg') }}" alt="Yandex Money"></div>
        <aside class="support">
            <h2>Ваша служба поддержки<br>Пн-Пт 10:00-19:00 МСК</h2><span class="phone">+7 800 777 4725</span></aside>
    </main>
    <footer>
        <nav>
            <ul>
                <li><a href="https://advcash.com/about/">О компании</a></li>
                <li><a href="https://advcash.com/solutions/">Решения</a></li>
                <li><a href="https://advcash.com/fees/">Тарифы</a></li>
                <li><a href="https://advcash.com/solutions/card/">Карта</a></li>
                <li><a href="https://advcash.com/faq/">FAQ</a></li>
                <li><a href="https://advcash.com/contacts/">Контакты</a></li>
            </ul>
        </nav>
        <aside>
            <a href="https://t.me/advcash_official_ru" target="_blank" rel="noopener noreferrer"><img src="{{ asset('assets/home/img/telegram.svg') }}" alt="Telegram"></a>
            <a href="https://www.facebook.com/advancedcash/" target="_blank" rel="noopener noreferrer"><img src="{{ asset('assets/home/img/fb.svg') }}" alt="Facebook"></a>
            <a href="https://twitter.com/advcash_ru" target="_blank" rel="noopener noreferrer"><img src="{{ asset('assets/home/img/tw.svg') }}" alt="Twitter"></a>
        </aside>
        <p class="terms"><small class="copyright">© 2019 Advanced Cash</small><small class="links"><a href="https://advcash.com/aml/">AML программа</a><a href="https://advcash.com/confidential/">О защите данных</a><a href="https://advcash.com/agreement/">Пользовательское соглашение</a></small></p>
        <p class="issued">Payment platform powered by Advanced Cash (60/237MT/19, 60/237PPS/19).
            <br>Advanced Cash is headquartered at Suite 16, Block 5, Watergardens, GX11 1AA, Gibraltar.
            <br>ООО Центр обработки информации
            <br>445057, Самарская область, город Тольятти, улица Юбилейная дом 40 офис 2003</p>
    </footer>
</div>

<script type="text/javascript" src="{{ asset('assets/home/js/jquery-1.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/home/js/common.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/home/js/1.js') }}"></script>


</body>

</html>