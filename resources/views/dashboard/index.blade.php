@extends('layouts.dashboard')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Dashboard</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <section class="col-lg-12 connectedSortable">

                        {{--<div class="card direct-chat direct-chat-primary">--}}
                            {{--<div class="card-header">--}}
                                {{--<h3 class="card-title">Direct Chart</h3>--}}

                                {{--<div class="card-tools">--}}
                                    {{--<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>--}}
                                    {{--Remove--}}
                                    {{--<button type="button" class="btn btn-tool" data-widget="remove"><i class="fas fa-times"></i></button>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.card-header -->--}}
                            {{--<div class="card-body">--}}
                                {{--<div class="container my-2">--}}
                                    {{--<h3>Title</h3>--}}
                                    {{--<p>Test test vlfl;dfbvdfb;kd</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<!-- /.card-body -->--}}
                        {{--</div>--}}

                    </section>
                </div>
            </div>
        </section>
    </div>

@endsection