@extends('layouts.dashboard')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Edit account</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <section class="col-lg-12 connectedSortable">

                        <div class="card direct-chat direct-chat-primary">
                            <div class="card-header">
                                <h3 class="card-title">Edit account</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                                    {{--Remove--}}
                                    {{--<button type="button" class="btn btn-tool" data-widget="remove"><i class="fas fa-times"></i></button>--}}
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-3">
                                <form role="form" method="post" action="{{ route('account.update') }}">
                                    @csrf
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="from_email" name="email" placeholder="Enter email" value="{{$user->email}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{$user->name}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="number_purse">Number purce</label>
                                            <input type="text" class="form-control" id="number_purse" name="number_purse" placeholder="Number purce" value="{{$user->number_purse}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="balance">Last login</label>
                                            <input type="text" class="form-control" id="last_login" name="last_login" placeholder="Last login" value="{{$user->last_login}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="scor">Scor</label>
                                            <input type="text" class="form-control" id="scor" name="scor" placeholder="Scor" value="{{$user->scor}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="ip">Last login</label>
                                            <input type="text" class="form-control" id="ip" name="ip" placeholder="Ip" value="{{$user->ip}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="balance">Balance USD</label>
                                            <input type="text" class="form-control" id="balance" name="balance" placeholder="Balance USD" value="{{$user->balance}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="balance_e">Balance EUR</label>
                                            <input type="text" class="form-control" id="balance_e" name="balance_e" placeholder="Balance EUR" value="{{$user->balance_e}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="balance_r">Balance RUR</label>
                                            <input type="text" class="form-control" id="balance_r" name="balance_r" placeholder="Balance RUR" value="{{$user->balance_r}}" required>
                                        </div>
                                        <div class="form-check">
                                            <input type="hidden" name="verify_status" value="0" {{$user->verify_status == 0 ? 'checked' : ''}} class="form-check-input" id="exampleCheck1">
                                            <input type="checkbox" name="verify_status" value="1" {{$user->verify_status == 1 ? 'checked' : ''}} class="form-check-input" id="status">
                                            <label class="form-check-label" for="status">Verify status?</label>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->

                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </section>
                </div>
            </div>
        </section>
    </div>

@endsection