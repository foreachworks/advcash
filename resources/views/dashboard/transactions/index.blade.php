@extends('layouts.dashboard')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Transactions</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <section class="col-lg-12 connectedSortable">

                        <div class="card direct-chat direct-chat-primary">
                            <div class="card-header">
                                <h3 class="card-title">Transactions</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                                    {{--Remove--}}
                                    {{--<button type="button" class="btn btn-tool" data-widget="remove"><i class="fas fa-times"></i></button>--}}
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th scope="col">ДАТА</th>
                                        <th scope="col">ТИП ОПЕРАЦИИ</th>
                                        <th scope="col">КОРРЕСПОНДЕНТ</th>
                                        <th scope="col">СУММА</th>
                                        <th scope="col" colspan="2"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(count($transactions) > 0 )
                                        @foreach($transactions as $transaction)
                                        <tr>
                                            <th scope="row">{{\Carbon\Carbon::parse($transaction->date_done)->format('d-m-Y H:i')}}</th>
                                            <td>{{$transaction->type_operation == 0 ? 'Внутренняя транзакция' : 'Інша'}}</td>
                                            <td>{{$transaction->number_purse}}</td>
                                            <td>{{$transaction->input == 0 ? '-' : ''}}{{$transaction->amount}}</td>
                                            <td><a href="{{ route('transactions.edit', ['payment' => $transaction->id]) }}">Edit</a></td>
                                            <td>
                                                <form method="post" action="{{route('transactions.destroy', ['id' => $transaction->id])}}">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <th scope="row" colspan="6">Нет доступных транзакций</th>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                                <div>
                                    {{$transactions->links()}}
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </section>
                </div>
            </div>
        </section>
    </div>

@endsection