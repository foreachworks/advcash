@extends('layouts.dashboard')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Dashboard</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Create transaction</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <section class="col-lg-12 connectedSortable">

                        <div class="card direct-chat direct-chat-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create one transaction</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                                    {{--Remove--}}
                                    {{--<button type="button" class="btn btn-tool" data-widget="remove"><i class="fas fa-times"></i></button>--}}
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-3">
                                <form role="form" method="post" action="{{ route('transactions.store') }}">
                                    @csrf
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="from_email">Email кореспондента</label>
                                            <input type="email" class="form-control" id="from_email" name="from_email" placeholder="Enter email" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="date_done">Дата платежа</label>
                                            <input type="text" class="form-control" id="date_done" name="date_done" placeholder="dd-mm-yyyy h:i" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="number_purse">Номер кошелька</label>
                                            <input type="text" class="form-control" id="number_purse" name="number_purse" placeholder="Number purse" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="amount">Сумма</label>
                                            <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="input">Тип операции</label>
                                            <select class="custom-select" id="input" name="input" required>
                                                <option value="0">Вивод</option>
                                                <option value="1">Начисление</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="comment">Примечание</label>
                                            <input type="text" class="form-control" id="comment" name="comment" placeholder="Comment" required>
                                        </div>
                                        <div class="form-check">
                                            <input type="hidden" name="status" value="0" class="form-check-input" id="exampleCheck1">
                                            <input type="checkbox" name="status" value="1" class="form-check-input" id="status">
                                            <label class="form-check-label" for="status">Status active?</label>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->

                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>

                        <div class="card direct-chat direct-chat-primary">
                            <div class="card-header">
                                <h3 class="card-title">Create transaction for range</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fas fa-minus"></i></button>
                                    {{--Remove--}}
                                    {{--<button type="button" class="btn btn-tool" data-widget="remove"><i class="fas fa-times"></i></button>--}}
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body p-3">
                                <form role="form" method="post" action="{{ route('transactions.store') }}">
                                    @csrf
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="from_email">Email кореспондента</label>
                                            <input type="email" class="form-control" id="from_email" name="from_email" placeholder="Enter email" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="date_from">Период начисления платежа с:</label>
                                            <input type="text" class="form-control" id="date_from" name="date_from" placeholder="dd-mm-yyyy" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="date_to">Период начисления платежа по:</label>
                                            <input type="text" class="form-control" id="date_to" name="date_to" placeholder="dd-mm-yyyy" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="time">Время начисления платежа по:</label>
                                            <input type="text" class="form-control" id="time" name="time" placeholder="H:i" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="number_purse">Номер кошелька</label>
                                            <input type="text" class="form-control" id="number_purse" name="number_purse" placeholder="Number purse" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="amount">Сумма</label>
                                            <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount" required>
                                        </div>
                                        <div class="form-group">
                                            <label for="input">Тип операции</label>
                                            <select class="custom-select" id="input" name="input" required>
                                                <option value="0">Вивод</option>
                                                <option value="1">Начисление</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="comment">Примечание</label>
                                            <input type="text" class="form-control" id="comment" name="comment" placeholder="Comment" required>
                                        </div>
                                        <div class="form-check">
                                            <input type="hidden" name="status" value="0" class="form-check-input" id="exampleCheck1">
                                            <input type="checkbox" name="status" value="1" class="form-check-input" id="status">
                                            <label class="form-check-label" for="status">Status active?</label>
                                        </div>
                                    </div>
                                    <!-- /.card-body -->

                                    <div class="card-footer">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                            </div>
                            <!-- /.card-body -->
                        </div>

                    </section>
                </div>
            </div>
        </section>
    </div>

@endsection