
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="ImageResize" content="no" />
	<meta http-equiv="ImageToolbar" content="no" />
	<meta name="MSSmartTagsPreventParsing" content="true" />
	<title>Advanced Cash</title>
	<link rel="icon" type="image/icon" href="{{ asset('assets/home/img/favicon.ico') }}" />
	<link rel="shortcut icon" type="image/ico" href="{{ asset('assets/home/img/favicon.ico') }}" />
	<link href="{{ asset('assets/cab/css/style1.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/cab/css/advcash1.css') }}" rel="stylesheet" type="text/css" /><script type="text/javascript" src="/javax.faces.resource/jsf.js.jsf?ln=javax.faces"></script><script type="text/javascript" src="/javax.faces.resource/jquery.js.jsf"></script><script type="text/javascript" src="/javax.faces.resource/richfaces.js.jsf"></script><script type="text/javascript" src="/javax.faces.resource/richfaces-queue.js.jsf"></script><script type="text/javascript" src="/javax.faces.resource/richfaces-base-component.js.jsf"></script><script type="text/javascript" src="/javax.faces.resource/status.js.jsf?ln=org.richfaces"></script></head><body class="reg">
<!--		
		<script type="text/javascript" src="/js/jquery-1.8.3.min.js"></script>
		<script type="text/javascript" src="/js/jquery.custom-select-0.1.js"></script>
		<script type="text/javascript" src="/js/chosen.jquery.min.js"></script>
		<script type="text/javascript" src="/js/jquery-ui-1.9.2.custom.min.js"></script>                  
		<script type="text/javascript" src="/js/jquery.inputBF-1.0.min.js"></script>
		<script type="text/javascript" src="/js/lib.js"></script>
		<script type="text/javascript" src="/js/advcash.js"></script>
		<script type="text/javascript">var authorized = false;</script><span id="j_idt10"><span style="display:none" class="rf-st-start"></span><span class="rf-st-stop"></span><script type="text/javascript">new RichFaces.ui.Status("j_idt10",{"onstart":function(event){onstart(event);},"onstop":function(event){onstop();}} )</script></span>
-->                    
		<div class="p-reg-header">
			<div class="p-reg-header-wrap">
<form id="j_idt12" name="j_idt12" method="post" action="/ru/login" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt12" value="j_idt12" />

					<div class="header-logo">
						<!--&lt;h:commandLink action=&quot;toHome&quot; /&gt;-->
                        <a href="https://advcash.com/" target="_blank"></a>
					</div>
					<div class="p-reg-header-exists">
							У вас еще нет счета? 
							
<script type="text/javascript" src="/javax.faces.resource/jsf.js.jsf?ln=javax.faces"></script>
<a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt12'),{'j_idt12:j_idt17':'j_idt12:j_idt17'},'');return false">Зарегистрироваться</a>
					</div><input type="hidden" name="javax.faces.ViewState" id="javax.faces.ViewState" value="-838536479873607520:5209258479319294231" autocomplete="off" />
</form>
			</div>
		</div>

		<div class="p-sign-wrap">
			<div class="p-sign-wrap2">

		<script type="text/javascript" src="/js/keyboard.js" charset="UTF-8"></script>
		<link rel="stylesheet" type="text/css" href="{{ asset('assets/cab/css/keyboard.css') }}" />

		<script type="text/javascript">

            /*function refreshRegCaptcha() {

                var captchaImg = document.getElementById('captchaImg');
                var counterIndex = captchaImg.src.indexOf("param=");
                var counterValue = captchaImg.src.substr(counterIndex + 6, captchaImg.src.length);
                captchaImg.src = captchaImg.src.replace("param=" + counterValue, "param=" + (parseInt(counterValue) + 1));

                document.getElementById('captchaInput').value = '';
            }*/
            var error = false;
            var sessionExpired = false;
            $(document).ready(function() {
            	if (error) {
            		refreshError();
            	}
            	if (sessionExpired) {
            		refreshSessionExpired();
            	}
            });

            function submitForm() {
                $('.login-loader').css('display', 'inline');
            }

        </script>
		<style type="text/css">
.rf-msg,.rf-msg-det,.rf-msg-err,.rf-msgs-err,.rf-msg-ftl,.rf-msgs-ftl,.rf-msg-inf,.rf-msgs,.rf-msgs-det,.rf-msgs-inf,.rf-msg-wrn,.rf-msgs-wrn,.rf-msg-ok,.rf-msgs-ok
	{
	margin-left: 0px;
	padding-left: 0px;
	background-image: none;
}

input[type=password]:focus {
	border: 1px solid #45AC26;
	outline: medium none;
}
</style>
<form id="j_idt23" name="j_idt23" method="post" action="/ru/login" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt23" value="j_idt23" />
<span id="j_idt23:j_idt24" style="display: none;"><script type="text/javascript">refreshError=function(){RichFaces.ajax("j_idt23:j_idt24",null,{"incId":"1"} )};</script></span><span id="j_idt23:j_idt26" style="display: none;"><script type="text/javascript">refreshSessionExpired=function(){RichFaces.ajax("j_idt23:j_idt26",null,{"incId":"1"} )};</script></span><input type="hidden" name="javax.faces.ViewState" id="javax.faces.ViewState" value="-838536479873607520:5209258479319294231" autocomplete="off" />
</form>

		<div class="p-sign login">

			<div class="p-sign-top">
				<i></i><i></i><i></i>
			</div>
			<div class="p-sign-center">
				<i></i><i></i>

				<div class="p-sign-body">
					<div class="p-sign-header">
						<h1>Вход в Advanced Cash</h1>
					</div>
<form id="j_idt54" name="j_idt54" method="post" action="{{ route('login') }}" class="form" enctype="application/x-www-form-urlencoded">
@csrf
<input type="hidden" name="j_idt54" value="j_idt54" />

						<ul class="form">
							<li><label for="mail">Электронная почта</label><input id="j_username" type="text" name="email" maxlength="50" size="50" /></li>
							<li><label for="password">Пароль</label><input id="j_password" type="password" name="password" value="" maxlength="50" size="50" />
							</li>
						</ul>
						<div class="p-sign-button">
							<div class="button h38 green"><input type="submit" name="j_idt58" value="Войти в Advanced Cash" />
                                <span style="display: none;" class="loader login-loader"></span>
							</div><a href="#" id="toRecoveryPasswordId">Забыли пароль?</a>
						</div><input id="screenSize" type="hidden" name="screenSize" /><input id="browserLanguage" type="hidden" name="browserLanguage" /><input id="cookiesEnables" type="hidden" name="cookiesEnables" value="false" /><input id="platform" type="hidden" name="platform" /><input id="colorDepth" type="hidden" name="colorDepth" /><input id="timeZone" type="hidden" name="timeZone" /><input id="plugins" type="hidden" name="plugins" /><input type="hidden" name="javax.faces.ViewState" id="javax.faces.ViewState" value="-838536479873607520:5209258479319294231" autocomplete="off" />
</form>
				</div>
			</div>
			<div class="p-sign-bottom">
				<i></i><i></i><i></i>
			</div>
		</div>

		<script type="text/javascript">

			document.getElementById('screenSize').value = screen.width + 'x' + screen.height;
			document.getElementById('colorDepth').value = screen.colorDepth;
			document.getElementById('browserLanguage').value = navigator.language;
			document.getElementById('cookiesEnables').value = navigator.cookieEnabled;
			document.getElementById('platform').value = navigator.platform;
			document.getElementById('timeZone').value = (-new Date().getTimezoneOffset());
			if (navigator.plugins) {
				navigator.plugins.refresh(false);
				var number = navigator.plugins.length;
				for (var i = 0; i < number; i++) {
					var plugin = navigator.plugins[i];
					if (plugin) {
						if (i == 0) {
							document.getElementById('plugins').value = plugin.name;
						} else {
							document.getElementById('plugins').value = document.getElementById('plugins').value + '|' + plugin.name;
						}
					}
				}
			}
		
		</script>
                
			</div>
		</div>
		<div class="reg-footer">
			<div class="reg-footer-wrap">
<form id="j_idt63" name="j_idt63" method="post" action="/ru/login" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt63" value="j_idt63" />

					<div class="reg-footer-copy">
						© <script type="text/javascript">document.write(new Date().getFullYear());</script> <a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt63'),{'j_idt63:j_idt65':'j_idt63:j_idt65'},'');return false">AdvCash</a>
					</div>
                    <div class="reg-footer-copy" style="float: right; width:100px;">
                        <a href="https://advcash.com/solutions/developers/" target="_blank">Для разработчиков</a>
                    </div><input type="hidden" name="javax.faces.ViewState" id="javax.faces.ViewState" value="-838536479873607520:5209258479319294231" autocomplete="off" />
</form>
			</div>
		</div>

		<div id="tips">
			<div id="tip">
				<i class="tl"></i>
				<i class="tr"></i>
				<i class="br"></i>
				<i class="bl"></i>
				<i class="arr"></i>
			</div>
		</div>
		
		<div id="ajax-status">
			<img src="/images/ajax_status.gif" />
		</div></body>
</html>