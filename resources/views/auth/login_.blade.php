<!DOCTYPE html>
<html class="ua-windows_nt ua-windows_nt-10 ua-windows_nt-10-0 ua-gecko ua-gecko-68 ua-gecko-68-0 ua-firefox ua-firefox-68 ua-firefox-68-0 ua-desktop ua-desktop-windows js js opacity csscolumns cssgradients csstransforms csstransitions" lang="ru">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Login</title>
    <meta name="description" content="Универсальная платежная платформа. Криптовалюты, карты, бесплатные внутренние переводы. Решения для мерчантов. Низкие комиссии. Создайте аккаунт бесплатно.">
    <meta name="og:description" content="Универсальная платежная платформа. Криптовалюты, карты, бесплатные внутренние переводы. Решения для мерчантов. Низкие комиссии. Создайте аккаунт бесплатно.">
    <meta name="og:image" content="index_files/social.png">
    <meta name="og:type" content="website">
    <meta name="og:title" content="Advanced Cash">
    <link rel="image_src" href="{{ asset('assets/cab/index_files/social.png') }}">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="@advcash">
    <meta name="twitter:title" content="Advanced Cash">
    <meta name="twitter:description" content="Универсальная платежная платформа. Криптовалюты, карты, бесплатные внутренние переводы. Решения для мерчантов. Низкие комиссии. Создайте аккаунт бесплатно.">
    <meta name="twitter:image" content="img/social.png">
    <!-- <meta name="viewport" content="width=1440, initial-scale=1"> -->

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/cab/css/common.css') }}">
    <link href="{{ asset('assets/cab/css/css.css') }}" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" href="{{ asset('assets/cab/img/favicon.ico') }}">
    <link href="{{ asset('assets/cab/css/style.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/cab/css/advcash.css') }}" rel="stylesheet" type="text/css">
</head>

<body class="reg">




<div class="p-reg-header">
    <div class="p-reg-header-wrap">
        <form id="j_idt11" name="j_idt11" method="post" action="/login" enctype="application/x-www-form-urlencoded">

            <div class="header-logo">
                <!--&lt;h:commandLink action=&quot;toHome&quot; /&gt;-->
                <a href="https://advcash.com/" target="_blank"></a>
            </div>
            <div class="p-reg-header-exists">
                У вас еще нет счета?

                <a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt14'),{'j_idt14:j_idt19':'j_idt14:j_idt19'},'');return false">Зарегистрироваться</a>
            </div>
            {{--<div class="p-reg-header-exists">--}}
                {{--<a href="/logout">Выход</a>--}}
            {{--</div>--}}
        </form>
    </div>
</div>

<div class="p-sign-wrap">
    <div class="p-sign-wrap2">



        <style type="text/css">
            .rf-msg,.rf-msg-det,.rf-msg-err,.rf-msgs-err,.rf-msg-ftl,.rf-msgs-ftl,.rf-msg-inf,.rf-msgs,.rf-msgs-det,.rf-msgs-inf,.rf-msg-wrn,.rf-msgs-wrn,.rf-msg-ok,.rf-msgs-ok
            {
                margin-left: 0px;
                padding-left: 0px;
                background-image: none;
            }

            input[type=password]:focus {
                border: 1px solid #45AC26;
                outline: medium none;
            }
        </style>


        <div class="p-sign login">

            <div class="p-sign-top">
                <i></i><i></i><i></i>
            </div>
            <div class="p-sign-center">
                <i></i><i></i>

                <div class="p-sign-body">
                    <div class="p-sign-header">
                        <h1>Вход в Advanced Cash</h1>
                    </div>
                    <form id="j_idt51"  method="POST" action="{{ route('login') }}" class="form" >
                        @csrf

                        <ul class="form">
                            <li>
                                <label for="mail">Электронная почта</label>
                                <input id="j_username" type="text" name="email" maxlength="50" size="50" required>
                            </li>
                            <li>
                                <label for="password">Пароль</label>
                                <input id="j_password" type="password" name="password" value="" maxlength="50" size="50" required>
                            </li>
                        </ul>
                        <div class="p-sign-button">
                            <div class="button h38 green"><input type="submit" name="j_idt55" value="Войти в Advanced Cash">
                                <span style="display: none;" class="loader login-loader"></span>
                            </div>
                            <a id="toRecoveryPasswordId" href="#" >Забыли пароль?</a>
                        </div>

                    </form>
                </div>
            </div>
            <div class="p-sign-bottom">
                <i></i><i></i><i></i>
            </div>
        </div>


    </div>
</div>
<div class="reg-footer">
    <div class="reg-footer-wrap">
        <form id="j_idt60" name="j_idt60" method="post" action="/login" enctype="application/x-www-form-urlencoded">


            <div class="reg-footer-copy">
                © <script type="text/javascript">document.write(new Date().getFullYear());</script>&nbsp;<a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt60'),{'j_idt60:j_idt62':'j_idt60:j_idt62'},'');return false">AdvCash</a>
            </div>
            <div class="reg-footer-copy" style="float: right; width:100px;">
                <a href="https://advcash.com/solutions/developers/" target="_blank">Для разработчиков</a>
            </div>
        </form>
    </div>
</div>

<div id="tips">
    <div id="tip">
        <i class="tl"></i>
        <i class="tr"></i>
        <i class="br"></i>
        <i class="bl"></i>
        <i class="arr"></i>
    </div>
</div>

<div id="ajax-status">
    <img src="{{ asset('assets/cab/img/ajax_status.gif') }}">
</div>
<div id="tips"><div id="tip"><i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i><i class="arr"></i></div></div>
</body>

</html>