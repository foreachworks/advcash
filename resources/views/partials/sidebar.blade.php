<div class="leftcol" id="leftCol">
    <form id="leftcolForm" name="leftcolForm" method="post" action="/index_page" enctype="application/x-www-form-urlencoded">

        <div class="block-personal">
            <div class="block-personal-acctype personal">
                <a>Персональный счет
                </a>
            </div>
            <div class="block-personal-user">
                <b> <i></i>
                    <a>{{auth()->user()->name}}
                    </a>

                </b> <span> <span>{{auth()->user()->scor}}
						</span>
					</span>
                <div class="{{auth()->user()->verify_status ? 'block-personal-user-verify green' : 'block-personal-user-verify'}}">
                    @if(auth()->user()->verify_status)
                        <a class="cp" >Верифицирован</a>
                    @else
                        <a class="cp" onclick="ClosePopup();Popup(this);">Не верифицирован</a>
                    @endif

                    <div id="recommendProfile" class="popup" style="z-index: 2000;">
                        <i class="popup-arrow"></i>

                        <div class="popup-header">
                            <i></i><i></i><i></i>
                        </div>
                        <div class="popup-content">
                            <i></i> <i></i>

                            <div class="popup-body" style="padding: 13px 14px;">
                                <p style="padding: 0px;">
                                    Мы рекомендуем пройти верификацию, чтобы пользоваться всеми возможностями системы без ограничений.
                                </p>
                                <p style="padding: 8px 0px 0px 0px;">
                                    Чтобы начать верификацию, нажмите
                                    <a id="toVerificationId" href="#" onclick="mojarra.jsfcljs(document.getElementById('leftcolForm'),{'toVerificationId':'toVerificationId'},'');return false">здесь</a>
                                </p>
                            </div>
                        </div>
                        <div class="popup-footer">
                            <i></i><i></i><i></i>
                        </div>
                    </div>
                    <div id="limit-verification-check" class="popup">
                        <i class="popup-arrow"></i>

                        <div class="popup-header">
                            <i></i><i></i><i></i>
                        </div>
                        <div class="popup-content">
                            <i></i> <i></i>

                            <div class="popup-body">
                                <p>Для снятия лимитов необходимо пройти верификацию.</p>
                            </div>
                        </div>
                        <div class="popup-footer">
                            <i></i><i></i><i></i>
                        </div>
                    </div>
                </div>

            </div>

            <div id="walletsBlock" class="block-wallets">
                <span id="walletErrorMessage" class="rf-msg dn"></span>
                <div class="block-wallets-title">Кошельки</div>
                <ul class="block-operations" id="walletList">
                    <ul class="rf-ulst" id="walletDataList">
                        <li id="walletDataList:0" class="wallet-U758724115185 rf-ulst-itm">
                            <div class="sum">
                                <b> <a class="wallet-currency">Доллар
                                    </a>
                                </b> <b>{{number_format(auth()->user()->balance, 2, '.', '')}}
                                </b>
                            </div>
                            <div class="wallet">
								<span>{{auth()->user()->number_purse}}
								</span> <span>USD</span>
                            </div>
                        </li>
                        <li id="walletDataList:1" class="wallet-E656024657015 rf-ulst-itm">
                            <div class="sum">
                                <b> <a class="wallet-currency">Евро
                                    </a>
                                </b> <b>{{number_format(auth()->user()->balance_e, 2, '.', '')}}
                                </b>
                            </div>
                            <div class="wallet">
							<span>E 6560 2465 7015
							</span> <span>EUR</span>
                            </div>
                        </li>
                        <li id="walletDataList:2" class="wallet-R925155918615 rf-ulst-itm">
                            <div class="sum">
                                <b> <a class="wallet-currency">Рубль
                                    </a>
                                </b> <b>{{number_format(auth()->user()->balance_r, 2, '.', '')}}
                                </b>
                            </div>
                            <div class="wallet">
						<span>R 9251 5591 8615
						</span> <span>RUR</span>
                            </div>
                        </li>
                    </ul>
                </ul>
            </div>
            <div id="addWalletBlock" class="block-cards-add" style="display: block">
                <div class="link">
                    <div class="button green card-add-button">
                        <a id="add-wallet" href="javascript:void(0)" onclick="ParentPopup(this);">
                            <img src="{{ asset('assets/cab/index_files/add-icon.png') }}" align="top">Добавить кошелек
                        </a>
                    </div>
                    <div class="popup add-wallet-popup">
                        <i class="popup-arrow"></i>

                        <div class="popup-header">
                            <i></i><i></i><i></i>
                        </div>
                        <div class="popup-content">
                            <i></i> <i></i>
                            <div class="popup-body">
                                <div class="popup-addwallet-header">
                                    <b>Добавление нового кошелька</b>
                                    <a href="javascript:void(0)" onclick="ClosePopup();"></a>
                                </div>
                                <!-- &lt;div class=&quot;popup-addwallet-text&quot;&gt;&lt;/div&gt; -->
                                <dl class="popup-addwallet-adder">
                                    <dt>Валюта</dt>
                                    <dd>
                                        <select id="walletCurrency" name="walletCurrency" class="custom" size="1" style="display: none;" index="sel-0">
                                            <option value="GBP" selected="selected">GBP</option>
                                            <option value="UAH">UAH</option>
                                            <option value="KZT">KZT</option>
                                            <option value="BRL">BRL</option>
                                        </select>

                                        <div class="button green">
                                            <input id="createPurseButtonId" name="createPurseButtonId" onclick='RichFaces.ajax("createPurseButtonId",event,{"incId":"1"} );return false;' value="Добавить кошелёк" type="submit">
                                        </div>
                                    </dd>
                                </dl>
                            </div>
                        </div>
                        <div class="popup-footer">
                            <i></i><i></i><i></i>
                        </div>
                    </div>
                </div>
            </div>

            <div class="block-cards">
                <div class="block-cards-title">Карты</div>
                <span id="cards"></span>
                <div class="block-cards-add">
                    <div class="button green card-add-button">
                        <a href="#" id="j_idt585" name="j_idt585" onclick='RichFaces.ajax("j_idt585",event,{"incId":"1"} );return false;'>
                            <img src="{{ asset('assets/cab/index_files/add-icon.png') }}" align="top">Создать карту
                        </a>
                    </div>

                </div>
            </div>

        </div>
    </form>
    <div id="cardCurrencyPopup" style="visibility: hidden;">
        <div class="rf-pp-shade" id="cardCurrencyPopup_shade" style="z-index:100;">
            <button class="rf-pp-btn" id="cardCurrencyPopupFirstHref"></button>
        </div>
        <div class="rf-pp-cntr " id="cardCurrencyPopup_container" style="position: fixed; z-index:100; ">
            <div class="rf-pp-shdw" id="cardCurrencyPopup_shadow" style="opacity: 0.1;"></div>
            <div class="rf-pp-cnt-scrlr" id="cardCurrencyPopup_content_scroller">
                <div class="rf-pp-cnt" id="cardCurrencyPopup_content">

                    <div class="popup-header">
                        <i></i><i></i><i></i>
                    </div>
                    <div class="popup-content">
                        <i></i> <i></i>
                        <div class="popup-body">
                            <div class="popup-profile-body">
                                <div class="fr">
                                    <a onclick="RichFaces.$('cardCurrencyPopup').hide();" class="close-popup"></a>
                                </div>
                                <form id="j_idt591" name="j_idt591" method="post" action="/index_page" enctype="application/x-www-form-urlencoded">


                                    <div class="p-profile-verify-item-fast">
                                        <dl class="p-profile-verify-item-desc">
                                            <dt>Валюта карты</dt>
                                        </dl>
                                        <ul>
                                            <li>
                                                <div class="p-settings-main-item-button">
                                                    <div id="cardCurrencyPanel" class="p-main-secbuttons-tabs">
                                                        <ul>
                                                            <li class="active"><span><a href="#" id="j_idt594" name="j_idt594" onclick='RichFaces.ajax("j_idt594",event,{"incId":"1"} );return false;'>USD</a>
														</span></li>
                                                            <li><span><a href="#" id="j_idt596" name="j_idt596" onclick='RichFaces.ajax("j_idt596",event,{"incId":"1"} );return false;'>EUR</a>
														</span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="cb"></div>
                                            </li>
                                            <li class="button green">
                                                <input id="j_idt599" name="j_idt599" onclick='RichFaces.ajax("j_idt599",event,{"incId":"1"} );return false;' value="Продолжить" type="submit">
                                            </li>
                                        </ul>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="popup-footer">
                        <i></i><i></i><i></i>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>