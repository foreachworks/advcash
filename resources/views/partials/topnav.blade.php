<script type="text/javascript" src="{{ asset('assets/cab/index_files/popper.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/cab/index_files/tippy-bundle.umd.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/cab/index_files/x.js') }}"></script>
<div class="x-header">

    <style type="text/css">
        #fileBugUpload {
            margin-bottom: 26px;
            width: 350px;
        }

        #fileBugUpload span {
            padding-top: 0;
        }
    </style>


<form id="j_idt25" name="j_idt25" method="post" action="/pages/transaction" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="j_idt25" value="j_idt25" />

				<div class="x-header__container">
					<div class="x-grid-row -gap-24 -valign-middle">
						<div class="x-grid-cell -size-auto">
							<div class="x-header__logo"><a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'j_idt27':'j_idt27'},'');return false">
									<img src="{{ asset('assets/cab/images/logo_cab.svg') }}" /></a>
							</div>
						</div>
						<? $segment1 = Request::segment(1); ?>
						<? isset($segment1) ? $opacity = 1 : $opacity = 0.5 ?>
						<div class="x-grid-cell -size-auto">
							<div class="x-header__subnav"><a id="toTxListId" style="opacity: {{$opacity}}" href="{{ route('home') }}" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'toTxListId':'toTxListId'},'');return false" class="x-header__subnav-link -current">Счет</a><a id="toMessageIncomeHeader" href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'toMessageIncomeHeader':'toMessageIncomeHeader'},'');return false" class="x-header__subnav-link ">Тикеты</a><a id="toNewsHeader" href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'toNewsHeader':'toNewsHeader'},'');return false" class="x-header__subnav-link ">Новости
								<!--		<span class="x-header__subnav-link-badge">3</span></a> -->
							</div>
						</div>
						<div class="x-grid-cell -size-fill">
							<div class="x-header__subnav"><a id="toReferralListHeader" href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'toReferralListHeader':'toReferralListHeader'},'');return false" class="x-header__subnav-link ">Реферальная программа</a><a id="toApiAndSciHeader" href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'toApiAndSciHeader':'toApiAndSciHeader'},'');return false" class="x-header__subnav-link ">Бизнесу</a><a id="toSupportService" href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'toSupportService':'toSupportService'},'');return false" class="x-header__subnav-link ">Помощь</a>
							</div>
						</div>
						<div class="x-grid-cell -size-auto">
							<div class="x-header__profile">
								<button type="button" class="x-header__profile-handle">
									<img src="{{ asset('assets/cab/images/profile.svg') }}" /> <b>{{auth()->user()->name}}</b>
									<img src="{{ asset('assets/cab/images/arrow.svg') }}" />
								</button>
								<div class="x-header__profile-dropdown">
									<div class="x-header__profile-dropdown-link"><a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'j_idt42':'j_idt42'},'');return false">Верификация</a>
									</div>
									<div class="x-header__profile-dropdown-link"><a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'j_idt44':'j_idt44'},'');return false">Персональные данные</a>
									</div>
									<div class="x-header__profile-dropdown-link"><a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'j_idt46':'j_idt46'},'');return false">Настройки безопасности</a>
									</div>
									<div class="x-header__profile-dropdown-link"><a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'j_idt48':'j_idt48'},'');return false">Смена пароля</a>
									</div>
									<div class="x-header__profile-dropdown-link"><a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'j_idt50':'j_idt50'},'');return false">Уведомления</a>
									</div>
									<div class="x-header__profile-dropdown-link"><a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'j_idt52':'j_idt52'},'');return false">Для разработчиков</a>
									</div>
									<div class="x-header__profile-dropdown-link">
										<a href="javascript:void(0)" onclick="$('#foundBugPopup').addClass('-is-open');">Нашли баг?</a>
									</div>
									<div class="x-header__profile-dropdown-link -logout">
										<a  href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Выход </a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<? Request::segment(2) == 'deposit-funds' ? $color_depo = '#FFF;' : $color_depo = '#00AF7E;' ?>
					<? Request::segment(3) == 'wallet' ? $color_wallet = '#FFF;' : $color_wallet = '#00AF7E;' ?>
					<div class="x-header__menu"><a id="toDepositFunds" style="color: {{$color_depo}}" href="{{ route('refill') }}">Пополнение счета</a>
						<i></i><a id="toTransferExchange" href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'toTransferExchange':'toTransferExchange'},'');return false">Обмен валюты</a>
						<i></i><a id="toTransferWallet" style="color: {{$color_wallet}}" href="{{ route('transaction') }}">Перевод средств</a>
						<i></i><a id="toBuyCrypto" href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'toBuyCrypto':'toBuyCrypto'},'');return false">Покупка криптовалют</a>
						<i></i><a id="toSellCrypto" href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt25'),{'toSellCrypto':'toSellCrypto'},'');return false">Продажа криптовалют</a>
					</div>
				</div><input type="hidden" name="javax.faces.ViewState" id="javax.faces.ViewState" value="-1363063349348036298:-7078208733877780900" autocomplete="off" />
</form>
</div>