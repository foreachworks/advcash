<style>
 .knopka01 {
  position: fixed;
  right: 21px;
  bottom: 13px;
  display: inline-block;
  width: 100px;
  height: 47px;
  line-height: 50px;
  font-size: 1.07143rem;
  text-decoration: none;
/*  text-shadow: 0 1px rgba(255,255,255,.2), 0 -1px rgba(0,0,0,.8);*/
  outline: none;
  border: none;
  border-radius: 100px;

    font-family: Verdana, Geneva, sans-serif;
	box-shadow: none;
    background-color: #009900 !important;
    color: #071F07 !important;
    fill: #071F07 !important;  color: rgb(0,79,86);
	
  user-select: none;
}

.container-3PFIa {
  display: inline-block; }
  .container-3PFIa:hover {
    color: currentColor; 
}

.icon-3E9qF {
  padding-right: 0.57143rem; }
  [dir='rtl'] .icon-3E9qF {
    padding-left: 0.57143rem;
    padding-right: 0; 
}

.Icon-2SEmO {
  padding-right: 0.85714rem; }
  [dir='rtl'] .Icon-2SEmO {
    padding-right: 0;
    padding-left: 0.85714rem; }
  .Icon-2SEmO svg {
    min-width: 1.42857rem;
    /* 1 */
    min-height: 1.42857rem;
    /* 1 */
    height: 1.42857rem;
    width: 1.42857rem; 
}

.Arrange-sizeFill, .Arrange-sizeFit {
display: table-cell;
padding: 0;
/* 1 */
vertical-align: top;
/* 2 */ 
}

.Arrange-sizeFill {
width: 100%; 
}
/*
.u-textInheritColor {
  color: inherit !important; 
}
*/
.u-inlineBlock {
	display: inline-block !important;
	max-width: 100%;
	/* 1 */ 
}




</style>

<button class="knopka01"><span class="container-3PFIa u-userColor icon-3E9qF Icon-2SEmO Arrange-sizeFit u-textInheritColor u-inlineBlock  Icon--chat" style="	padding-top: 4px;
" type="Icon--chat"><svg id="Layer_1" x="0" y="0" viewBox="0 0 18 19" xml:space="preserve" aria-hidden="true"><title></title><path d="M1.3,16c-0.7,0-1.1-0.3-1.2-0.8c-0.3-0.8,0.5-1.3,0.8-1.5c0.6-0.4,0.9-0.7,1-1c0-0.2-0.1-0.4-0.3-0.7c0,0,0-0.1-0.1-0.1 C0.5,10.6,0,9,0,7.4C0,3.3,3.4,0,7.5,0C11.6,0,15,3.3,15,7.4s-3.4,7.4-7.5,7.4c-0.5,0-1-0.1-1.5-0.2C3.4,15.9,1.5,16,1.5,16 C1.4,16,1.4,16,1.3,16z M3.3,10.9c0.5,0.7,0.7,1.5,0.6,2.2c0,0.1-0.1,0.3-0.1,0.4c0.5-0.2,1-0.4,1.6-0.7c0.2-0.1,0.4-0.2,0.6-0.1 c0,0,0.1,0,0.1,0c0.4,0.1,0.9,0.2,1.4,0.2c3,0,5.5-2.4,5.5-5.4S10.5,2,7.5,2C4.5,2,2,4.4,2,7.4c0,1.2,0.4,2.4,1.2,3.3 C3.2,10.8,3.3,10.8,3.3,10.9z"></path></svg></span><span class="label-3kk12 Arrange-sizeFit u-textInheritColor u-inlineBlock" style="font-size: 0.92489rem; font-family: Gill Sans, sans-serif;
 font-weight: 700;">&nbsp;Чат</span></button>
