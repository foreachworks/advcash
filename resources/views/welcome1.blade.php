<!DOCTYPE html>
<html lang="ru">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
  <title>advcash платежная платформа: массовые выплаты, prepaid-карты</title>
  <meta name="yandex-verification" content="9c5ea705b3a36169" />
  <meta name="description" content="Универсальная платежная платформа. Криптовалюты, карты, бесплатные внутренние переводы. Решения для мерчантов. Низкие комиссии. Создайте аккаунт бесплатно." />
  <meta name="og:description" content="Универсальная платежная платформа. Криптовалюты, карты, бесплатные внутренние переводы. Решения для мерчантов. Низкие комиссии. Создайте аккаунт бесплатно." />
  <meta name="og:image" content="index_files/social.png" />
  <meta name="og:type" content="website" />
  <meta name="og:title" content="Advanced Cash" />
  <link rel="image_src" href="{{ asset('assets/home/index_files/social.png') }}">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:site" content="@advcash">
  <meta name="twitter:title" content="Advanced Cash">
  <meta name="twitter:description" content="Универсальная платежная платформа. Криптовалюты, карты, бесплатные внутренние переводы. Решения для мерчантов. Низкие комиссии. Создайте аккаунт бесплатно.">
  <meta name="twitter:image" content="img/social.png">
  <link rel="alternate" hreflang="en" href="/en/">
  <!-- <meta name="viewport" content="width=1440, initial-scale=1"> -->
  <link rel="stylesheet" type="text/css" href="{{ asset('assets/home/css/common.css') }}" />
  <link href='//fonts.googleapis.com/css?family=Open+Sans:400,700,300,600&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
  <!--[if (gte IE 6)&(lte IE 8)]><script type="text/javascript" src="/js/selectivizr.js"></script><![endif]-->
  <!--[if lt IE 9]><script src="/js/html5shiv.js"></script><![endif]-->
  <link rel="shortcut icon" href="{{ asset('assets/home/img/favicon.ico') }}" />
</head>

<body class="promo">
  <div class="wrapper">
    <div class="header-container -transparent">
      <header>
        <a class="logo" style="margin-left: 45px;"><img src="{{ asset('assets/home/img/logo-new.svg') }}" height="40" alt="ADV" /></a>
        <nav style="margin-left: -50px;">
          <ul>
            <li class="multi-level"><a href="https://advcash.com/about/">О компании</a>
              <ul>
                <li><a href="https://advcash.com/about/company/">Всегда впереди</a></li>
                <li><a href="https://advcash.com/about/partners/">Партнёры</a></li>
              </ul>
            </li>
            <li class="new"><a href="https://advcash.com/business/">Бизнесу</a></li>
            <li><a href="https://advcash.com/solutions/">Решения</a></li>
            <li></li>
            <li><a href="https://advcash.com/fees/">Тарифы</a></li>
            <li><a href="https://advcash.com/solutions/card/">Карта</a></li>
            <li><a href="https://advcash.com/faq/">FAQ</a></li>
            <li><a href="https://advcash.com/contacts/">Контакты</a></li>
          </ul>
        </nav>
        <div class="button">
          <ul class="stocks">
            <li><span class="name">USD/EUR</span><span class="value down">0.92</span></li>
            <li><span class="name">USD/GBP</span><span class="value up">0.83</span></li>
            <li><span class="name">USD/RUB</span><span class="value down">71.56</span></li>
            <li><span class="name">EUR/USD</span><span class="value up">1.14</span></li>
            <li><span class="name">EUR/GBP</span><span class="value up">0.92</span></li>
            <li><span class="name">EUR/RUB</span><span class="value up">79.46</span></li>
            <li><span class="name">GBP/USD</span><span class="value down">1.26</span></li>
            <li><span class="name">GBP/EUR</span><span class="value down">1.14</span></li>
            <li><span class="name">GBP/RUB</span><span class="value down">88.33</span></li>
            <li class="btc"><span class="name">BTC/USD</span><span class="value up">9437.7267</span></li>
            <li class="btc"><span class="name">BTC/EUR</span><span class="value up">8494.24</span></li>
            <li class="btc"><span class="name">BTC/RUB</span><span class="value up">662066.15</span></li>
            <li><span class="name">ETH/USD</span><span class="value up">229.68</span></li>
            <li><span class="name">ETH/EUR</span><span class="value up">206.72</span></li>
            <li><span class="name">LTC/USD</span><span class="value up">45.7653</span></li>
            <li><span class="name">BCH/USD</span><span class="value up">238.7</span></li>
            <li><span class="name">XRP/USD</span><span class="value down">0.1999</span></li>
            <li><span class="name">ZEC/USD</span><span class="value up">47.83</span></li>
          </ul><a href="https://advcash.com/en/" class="lang">Eng</a></div><a href="{{ route('login') }}" class="header__login">Вход</a><a href="#" class="header__register">Регистрация</a></header>
    </div>
    <main>
	

      <div class="crypto">
        <div class="crypto__container">
          <h1>Покупайте и&nbsp;продавайте криптовалюту</h1>
          <form class="crypto__calc -tab-buy">
            <div class="crypto__subhead">
              <p>
                <button type="button" data-tab="buy"><span>Покупка</span></button>
              </p>
              <p>
                <button type="button" data-tab="sell"><span>Продажа</span></button>
              </p>
            </div>
            <div class="crypto__calc-form">
              <div class="crypto__calc-form-input -is-filled">
                <div class="crypto__calc-form-input-placeholder">Отдаете</div>
                <input type="text" class="crypto__calc-form-input-field" data-action="SELL" value="100">
              </div>
              <div class="crypto__calc-form-select" data-type="fiat">
                <div class="crypto__calc-form-select-handler">
                  <div class="crypto__calc-form-select-handler-value">USD</div>
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 7" width="13" height="7">
                    <path fill="#FFF" fill-rule="evenodd" d="M0 .699L6.488 7 13 .699 12.278 0 6.479 5.61.722 0z" />
                  </svg>
                </div>
                <ul>
                  <li class="-active">
                    <button type="button">USD</button>
                  </li>
                  <li>
                    <button type="button">EUR</button>
                  </li>
                  <li>
                    <button type="button">RUR</button>
                  </li>
                  <li>
                    <button type="button">GBP</button>
                  </li>
                  <li>
                    <button type="button">UAH</button>
                  </li>
                  <li>
                    <button type="button">KZT</button>
                  </li>
                  <li>
                    <button type="button">BRL</button>
                  </li>
                  <li>
                    <button type="button">TRY</button>
                  </li>
                </ul>
              </div>
              <div class="crypto__calc-form-input -is-filled">
                <div class="crypto__calc-form-input-placeholder">Получаете</div>
                <input type="text" class="crypto__calc-form-input-field" data-action="BUY" value="0.009658">
              </div>
              <div class="crypto__calc-form-select" data-type="crypto">
                <div class="crypto__calc-form-select-handler">
                  <div class="crypto__calc-form-select-handler-value">BTC</div>
                  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 7" width="13" height="7">
                    <path fill="#FFF" fill-rule="evenodd" d="M0 .699L6.488 7 13 .699 12.278 0 6.479 5.61.722 0z" />
                  </svg>
                </div>
                <ul>
                  <li class="-active">
                    <button type="button">BTC</button>
                  </li>
                  <li>
                    <button type="button">LTC</button>
                  </li>
                  <li>
                    <button type="button">ETH</button>
                  </li>
                  <li>
                    <button type="button">XRP</button>
                  </li>
                  <li>
                    <button type="button">BCH</button>
                  </li>
                  <li>
                    <button type="button">ZEC</button>
                  </li>
                </ul>
              </div><a href="#" class="crypto__calc-form-submit"><span>Купить</span><span>Продать</span></a></div>
            <div class="crypto__calc-error"></div>
            <div class="crypto__calc-result">
              <div class="crypto__calc-result-methods">
                <p><span class="crypto__calc-result-method -amount -for-sell"><span calc-result-amount></span>&nbsp;<span>на ADV-аккаунт</span></span>
                  <!--span class="crypto__calc-result-method -card"><span calc-result-card></span>&nbsp;<span calc-result-to class="-for-buy"></span><span>&nbsp;</span><span class="-for-buy">покупка с банковской карты</span><span class="-for-sell">на ADV-карту</span></span--><span class="crypto__calc-result-method -card -for-buy"><span calc-result-exchanged></span>
                  <!-- <span calc-result-to></span><span>&nbsp;</span> -->0.009658 покупка с банковской карты</span>
                  <!--span class="crypto__calc-result-method -other"><span calc-result-other></span>&nbsp;<span class="-for-buy">и более после пополнения ADV-аккаунта другими удобными способами</span><span class="-for-sell">или менее, если выводите на карту VISA/MC или другим удобным способом</span></span--></p>
                <p class="-for-buy">Все варианты пополнения ADV-аккаунта смотрите <a href="https://advcash.com/fees/">здесь</a>.</p>
                <p class="-for-sell">Все варианты вывода из ADV-аккаунта смотрите <a href="https://advcash.com/fees/">здесь</a>.</p>
              </div>
              <div class="crypto__calc-result-rate">
                <p>1 <span calc-result-from></span> = <span calc-result-rate>9 942.3345</span> <span calc-result-to>USD</span></p>
              </div>
            </div>
          </form>
          <div class="crypto__cards">
            <div class="crypto__subhead">
              <p>Карты</p>
            </div>
            <a href="https://advcash.com/solutions/card/" class="crypto__cards-preview"><img src="{{ asset('assets/home/img/plastic-small@2x.png') }}" srcset="{{ asset('assets/home/img/plastic-small@2x.png') }} 1x, {{ asset('assets/home/img/plastic-small@2x.png') }} 2x, {{ asset('assets/home/img/plastic-small@2x.png') }} 3x" alt="" width="240"><img src="{{ asset('assets/home/img/virtual-small@2x.png') }}" srcset="{{ asset('assets/home/img/virtual-small@2x.png') }} 1x, {{ asset('assets/home/img/virtual-small@2x.png') }} 2x, {{ asset('assets/home/img/virtual-small@2x.png') }} 3x" alt="" width="240"></a>
            <div class="crypto__cards-more"><a href="https://advcash.com/solutions/card/">Подробнее <img src="{{ asset('assets/home/img/arrow-more.svg') }}" alt="→"></a></div>
          </div>
        </div>
      </div>
	  
      <div class="promo-intro">
        <h2>Деньги + интернет<br>просто и безопасно</h2>
        <p>Управляйте вашими онлайн-финансами с помощью advcash. Получайте выплаты, пользуйтесь картами, пополняйте счет и выводите средства множеством удобных способов. Быстро. Выгодно. Без проблем.</p><img src="{{ asset('assets/home/img/intro.svg') }}" width="937" height="299" alt=""><a href="https://wallet.advcash.com/ru/register">Стать клиентом</a></div>
      <section class="abilities">
        <h2>Возможности advcash</h2>
        <ul>
          <li>
            <figure><img src="{{ asset('assets/home/img/3.svg') }}" alt="Выгодно пополнить" /></figure>
            <h3>Выгодно пополнить</h3>
            <p>Банковские карты и переводы, онлайн-банкинг, е-валюты, наличные и многое другое. Всегда найдется способ быстро и выгодно пополнить ваш счет в advcash, где бы вы ни были.</p>
          </li>
          <li>
            <figure><img src="{{ asset('assets/home/img/2.svg') }}" alt="Удобно использовать" /></figure>
            <h3>Удобно использовать</h3>
            <p>Несколько валют в одном аккаунте. Бесплатные внутренние переводы. Мгновенное пополнение ADV-карт. Многоуровневая система безопасности. Быстрая верификация и поддержка.</p>
          </li>
          <li>
            <figure><img src="{{ asset('assets/home/img/1.svg') }}" alt="Легко вывести" /></figure>
            <h3>Легко вывести</h3>
            <p>Виртуальные и пластиковые prepaid-карты в разных валютах. Быстрые выплаты на локальные и международные карты Visa/MC.</p>
          </li>
        </ul><a class="register" href="https://wallet.advcash.com/ru/register">Стать клиентом</a></section>
		
		
		<div class="binance-collab">
			<div class="binance-collab__bg">
				<div class="binance-collab__bg-green" style="transform: translate(-358px, 638px);"></div>
				<div class="binance-collab__bg-yellow" style="transform: translate(454px, -750px);"></div>
				<div class="binance-collab__bg-black" style="transform: translate(518px, 531px);"></div>
				<div class="binance-collab__bg-noise"></div>
				<div class="binance-collab__bg-overlay"></div>
			</div>
			<div class="binance-collab__body">
				<img src="{{ asset('assets/home/img/binance-collab.svg') }}" alt="">
				<h1>Крупнейшая криптобиржа <br>в&nbsp;вашем аккаунте</h1>
				<p>Привяжите аккаунт на&nbsp;бирже Binance к&nbsp;ADV и&nbsp;управляйте вашим крипто-портфолио, экономя время и&nbsp;деньги. <br>Уникальные возможности и&nbsp;выгодные курсы в&nbsp;нашем новом разделе «Крипто».</p>
				<a href="#">Создать аккаунт</a>
				<small>Благодаря нашему партнерству с&nbsp;крупнейшей в&nbsp;мире криптобиржей Binance в&nbsp;вашем ADV-аккаунте <br>теперь есть широкие возможности по&nbsp;работе с&nbsp;криптовалютой.</small>
			</div>
		</div>	
		
		
      <div class="partners"><img src="{{ asset('assets/home/img/visa.svg') }}" alt="Visa"><img src="{{ asset('assets/home/img/swift.svg') }}" alt="Swift"><img src="{{ asset('assets/home/img/mir.svg') }}" alt="Мир"><img src="{{ asset('assets/home/img/sepa.svg') }}" alt="Sepa"><img src="{{ asset('assets/home/img/qiwi.svg') }}" alt="Qiwi"><img src="{{ asset('assets/home/img/mastercard.svg') }}" alt="Master Card"><img src="{{ asset('assets/home/img/yandexmoney.svg') }}" alt="Yandex Money"></div>
	  
	  
      <aside class="support">
        <h2>Ваша служба поддержки<br>Пн-Пт 09:00-18:00 МСК</h2><span class="phone">+7 800 777 4725</span></aside>
		
	  
	  
	  
      <!--div class="adv-card-released"><div class="adv-card-released__notice">Карты, массовые выплаты, прием платежей</div><h1 class="adv-card-released__headline">Фрилансерам, вебмастерам,<br>онлайн-бизнесу</h1><div class="adv-card-released__cards"><a href="/solutions/card/" class="adv-card-image"><img src="/i/1.1/card/plastic@2x.png" srcset="/i/1.1/card/plastic@2x.png 1x, /i/1.1/card/plastic@2x.png 2x, /i/1.1/card/plastic@3x.png 3x" alt="" width="413"></a><a href="/solutions/card/" class="adv-card-image"><img src="/i/1.1/card/virtual@2x.png" srcset="/i/1.1/card/virtual@2x.png 1x, /i/1.1/card/virtual@2x.png 2x, /i/1.1/card/virtual@3x.png 3x" alt="" width="413"></a></div><div class="adv-card-released__more"><a href="/solutions/card/">Подробнее <img src="/i/1.1/arrow-more.svg" alt="→"></a></div></div-->
      <!--div class="adv-card-soon"><div class="adv-card-soon__cards"><a href="/solutions/card/" class="adv-card-image"><img src="/i/1.1/card/plastic.png" srcset="/i/1.1/card/plastic.png 1x, /i/1.1/card/plastic@2x.png 2x, /i/1.1/card/plastic@3x.png 3x" alt=""></a><a href="/solutions/card/" class="adv-card-image"><img src="/i/1.1/card/virtual.png" srcset="/i/1.1/card/virtual.png 1x, /i/1.1/card/virtual@2x.png 2x, /i/1.1/card/virtual@3x.png 3x" alt=""></a></div><div class="adv-card-soon__more"><a href="/solutions/card/">Подробнее <img src="/i/1.1/arrow-more.svg" alt="→"></a></div></div-->

		
    </main>
    <footer>
      <nav>
        <ul>
          <li><a href="/about/">О компании</a></li>
          <li><a href="/business/">Бизнесу</a></li>
          <li><a href="/solutions/">Решения</a></li>
          <li><a href="/fees/">Тарифы</a></li>
          <li><a href="/solutions/card/">Карта</a></li>
          <li><a href="/faq/">FAQ</a></li>
          <li><a href="/contacts/">Контакты</a></li>
        </ul>
      </nav>
      <aside>
        <a href="https://t.me/advcash_official_ru" target="_blank" rel="noopener noreferrer"><img src="{{ asset('assets/home/img/telegram.svg') }}" alt="Telegram"></a>
        <a href="https://www.facebook.com/advcashru/" target="_blank" rel="noopener noreferrer"><img src="{{ asset('assets/home/img/fb.svg') }}" alt="Facebook"></a>
        <a href="https://twitter.com/advcash_ru" target="_blank" rel="noopener noreferrer"><img src="{{ asset('assets/home/img/tw.svg') }}" alt="Twitter"></a>
      </aside>
      <p class="terms"><small class="copyright">© 2021 Advanced Cash</small><small class="links"><a href="#">Политика использования файлов cookie</a><a href="https://advcash.com/aml/">AML программа</a><a href="https://advcash.com/confidential/">О защите данных</a><a href="/agreement/">Пользовательское соглашение</a></small></p>
      <p class="issued">Payment platform powered by Advanced Cash (Licenses MT 000107/97, PPS 000107/98).
        <br>Advanced Cash is headquartered at 35 Barrack Road, Third Floor, Belize City, Belize.
		<div class="footer__security" style="display: flex; align-items: center; margin-top: 39px;">
			<img src="{{ asset('assets/home/img/pci.svg') }}" style="flex: 0 0 auto; margin-right: 36px;" alt="PCI DSS">
			<img src="{{ asset('assets/home/img/trustwave.svg') }}" style="flex: 0 0 auto; margin-right: 36px;" alt="Trustwave">
			<p style="font-size: 10px; line-height: 12px; letter-spacing: 0.5px; color: #58727c; margin: 0;">PCI DSS<br>Certificate of Compliance<br>7361-2703-5F56-4CFE</p>
		</div>

      <!---<p class="issued">Интернет-эквайринг предоставлен <a href=https://register.fca.org.uk/ShPo_FirmDetailsPage?id=001b000000n3TOxAAM target="_blank">Cauri LTD</a> (регистрационный номер 09507138, 102 HAMILTON HOUSE, 1 TEMPLE AVENUE, LONDON, EC4Y 0HA, UNITED KINGDOM)</p>-->
    </footer>
  </div>

  <script type="text/javascript" src="{{ asset('assets/home/js/jquery-1.10.2.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/home/js/jquery.inputmask.min.js') }}"></script>
  <!-- <script type="text/javascript" src="{{ asset('assets/home/js/common.js') }}"></script> -->
  <script type="text/javascript" src="{{ asset('assets/home/js/common1.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/home/js/1.js') }}"></script>
  <script type="text/javascript" src="{{ asset('assets/home/js/1.1.js') }}"></script>
  
  <!-- Yandex.Metrika counter -->
  <script type="text/javascript">
    (function(m, e, t, r, i, k, a) {
      m[i] = m[i] || function() {
        (m[i].a = m[i].a || []).push(arguments)
      };
      m[i].l = 1 * new Date();
      k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");
    ym(53021959, "init", {
      clickmap: true,
      trackLinks: true,
      accurateTrackBounce: true,
      webvisor: true
    });
  </script>
  <noscript>
    <div><img src="https://mc.yandex.ru/watch/53021959" style="position:absolute; left:-9999px;" alt="" /></div>
  </noscript>
  <!-- /Yandex.Metrika counter -->
  <script>
    (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js ', 'ga');
    ga('create', 'UA-54356805-1', 'auto');
    ga('send', 'pageview');
  </script>
  <script async type="text/javascript" src="/_Incapsula_Resource?SWJIYLWA=719d34d31c8e3a6e6fffd425f7e032f3&ns=1&cb=750307576"></script>
</body>

</html>