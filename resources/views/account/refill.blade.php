@extends('layouts.account')

@section('content')

    <span id="j_idt44">
        <span style="display: none;" class="rf-st-start"></span>
        <span class="rf-st-stop" style=""></span>
    </span>

    @include('partials.header')

    @include('partials.topnav')

    <style type="text/css">
        .p-main-transactions .status a {
            background: none;
        }
        /*#txForm .p-main-transactions {min-height: 569px;}
        #emptyTxForm .p-main-transactions {min-height: 620px;}*/

        .notification-popup .card-campaign-notification {
            width: 100%;
            height: 150px;
            background: url(../img/card.png) 10px 20px no-repeat;
        }

        .notification-popup .popup-body .card-campaign-notification-message {
            font-size: 14px;
            float: left;
            padding: 26px 17px 16px 150px;
        }

        #cardsProgramCampaignRemindMeLaterCheckbox .remind-me-later-label {
            margin-left: 150px;
        }
    </style>

    <div class="content-wrapper">
        <div class="content">
            @include('partials.sidebar')

            <div class="p-main">


                    <div class="rightcol">
                        <div class="p-profile">
                    <span id="deposit-funds-header">
                        <ul class="p-inside-transactions-speedbar">
                            <li><a href="#" id="j_idt578" name="j_idt578" onclick="RichFaces.ajax(&quot;j_idt578&quot;,event,{&quot;incId&quot;:&quot;1&quot;} );return false;">Главная</a> /</li>
                        </ul>
                        <div class="p-inside-transactions-header">Пополнение счета</div>
                    </span>
                        </div>

                        <script type="text/javascript">
                            var index = 0;
                            $(document).bind('click', function(e) {
                                if ($(e.target).parents('.p-input-sum dd').length) {
                                    $('.p-reg-form-item-error').hide();
                                }
                            });
                            function showInternetBanking() {
                                $('.p-input-list').show();
                            }
                            function autocompleteCss() {
                                $('.rf-au').removeClass('rf-au');
                                $('.rf-au-inp').removeClass('rf-au-inp');
                                $('.rf-au-lst-dcrtn').removeClass('rf-au-lst-dcrtn');
                            }
                            $('.p-reg-form-item-error').hide();
                            var smsSendAgain = 'SMS отправлено, пожалуйста подождите несколько минут.';
                            var timerInterval;
                            var timeLeft = 0;
                            var timer;
                            function updateTimer() {
                                timeLeft -= 1000;
                                if (timeLeft > 0) {
                                    timer.text(msToTime(timeLeft));
                                } else {
                                    window.location.reload(true);
                                }
                            }
                            function msToTime(s) {
                                var ms = s % 1000;
                                s = (s - ms) / 1000;
                                var secs = s % 60;
                                s = (s - secs) / 60;
                                var mins = s % 60;
                                var zero = secs <= 9 ? '0' : '';
                                return mins + ':' + zero + secs;
                            }
                            $(document).ready(function() {
                                currentTab();
                                $('.p2-combobox-select, .p2-select').find('> select').trigger('rebuild');
                            });
                        </script>

                        <style type="text/css">
                            .p-reg-form-item-error {
                                position: static;
                                background: none;
                                top: 390px;
                                right: 290px;
                                width: 700px;
                                padding-left: 0px;
                            }

                            .swift-sepa-step2 .f-item .p-reg-form-item-error {
                                position: initial;
                                margin: -8px 0px 0px 1px;
                            }
                            .swift-sepa-step2 .p-reg-form-item-error .rf-msg-err span {
                                color: #E04444;
                            }

                            .p-reg-form-item-error i.arr {
                                display: none !important;
                            }

                            .rf-msgs {
                                display: none !important;
                            }

                            .rf-msg-ftl {
                                padding-left: 0px;
                                background-image: none;
                            }

                            .rf-msg-inf {
                                background-image: none;
                            }

                            hr {
                                margin: 20px 0;
                            }

                            .p2-payment__hr-dotted {
                                border: none;
                                border-top: 1px dashed black;
                                margin: 26px 0;
                            }

                            .tether-transport-protocol .chzn-search {
                                position: absolute !important;
                                margin-top: -25px !important;
                                left: -9000px;
                            }
                        </style>


                        <form id="mainForm" name="mainForm" method="post" action="/pages/deposit-funds" enctype="application/x-www-form-urlencoded">


                <span id="panel">

                    <div class="p2-payment">
                        <div class="p2-payment__main">

                            <div class="p2-payment__main-deposit-withdrawal-switcher">
                                <div class="p2-payment__main-field-label"></div>
                                <div class="p2-payment__main-field-value">
                                    <a href="#" id="j_idt604" name="j_idt604" onclick="RichFaces.ajax(&quot;j_idt604&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_withdrawal_switch_action')&quot;} );return false;" class="p2-payment__deposit-withdrawal-switch-button deposit_withdrawal_switch_action">
                                        <i class="i-transaction-down"></i>
                                        <span>Получить </span>
                                    </a>
                                </div>
                            </div>

                            <div class="p2-payment__main-field">
                                <div class="p2-payment__main-field-label">Сумма и валюта пополнения</div>
                                <div class="p2-payment__main-field-value">
                                    <div class="p2-combobox"><input id="wmi_amount" type="text" name="wmi_amount" value="0.00" class="input-amount wmi_amount" maxlength="10" onkeyup="RichFaces.ajax(this,event,{&quot;sourceId&quot;:this,&quot;parameters&quot;:{&quot;javax.faces.behavior.event&quot;:&quot;keyup&quot;,&quot;org.richfaces.ajax.component&quot;:&quot;wmi_amount&quot;} } )" onchange="RichFaces.ajax(this,event,{&quot;sourceId&quot;:this,&quot;parameters&quot;:{&quot;javax.faces.behavior.event&quot;:&quot;change&quot;,&quot;org.richfaces.ajax.component&quot;:&quot;wmi_amount&quot;} } )">

                                        <div class="p2-combobox-select">
                                            <div class="p2-combobox-select-value fallback">USD
                                            </div>
                                            <select id="j_idt612" name="j_idt612" size="1" onchange="RichFaces.ajax(this,event,{&quot;sourceId&quot;:this,&quot;parameters&quot;:{&quot;javax.faces.behavior.event&quot;:&quot;change&quot;,&quot;org.richfaces.ajax.component&quot;:&quot;j_idt612&quot;} } )">    <option value="USD" selected="selected">USD</option>
                                                <option value="EUR">EUR</option>
                                                <option value="RUR">RUR</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="p2-payment__main-field">
                                <div class="p2-payment__main-field-label">Страна</div>
                                <div class="p2-payment__main-field-value">
                                    <div class="p2-autocomplete">
                                        <select id="_dfcntr" name="_dfcntr" class="chzn _dfcntr chzn-done" size="1" onchange="RichFaces.ajax(this,event,{&quot;sourceId&quot;:this,&quot;parameters&quot;:{&quot;javax.faces.behavior.event&quot;:&quot;change&quot;,&quot;org.richfaces.ajax.component&quot;:&quot;_dfcntr&quot;} } )" style="display: none;">    <option value="ALL">All countries</option>
                                            <option value="AX">Aland Islands</option>
                                            <option value="AL">Albania</option>
                                            <option value="DZ">Algeria</option>
                                            <option value="AS">American Samoa</option>
                                            <option value="AD">Andorra</option>
                                            <option value="AO">Angola</option>
                                            <option value="AI">Anguilla</option>
                                            <option value="AQ">Antarctica</option>
                                            <option value="AG">Antigua and Barbuda</option>
                                            <option value="AR">Argentina</option>
                                            <option value="AM">Armenia</option>
                                            <option value="AW">Aruba</option>
                                            <option value="AU">Australia</option>
                                            <option value="AT">Austria</option>
                                            <option value="AZ">Azerbaijan</option>
                                            <option value="BS">Bahamas</option>
                                            <option value="BH">Bahrain</option>
                                            <option value="BD">Bangladesh</option>
                                            <option value="BB">Barbados</option>
                                            <option value="BY">Belarus</option>
                                            <option value="BE">Belgium</option>
                                            <option value="BJ">Benin</option>
                                            <option value="BM">Bermuda</option>
                                            <option value="BT">Bhutan</option>
                                            <option value="BO">Bolivia</option>
                                            <option value="BA">Bosnia and Herzegovina</option>
                                            <option value="BW">Botswana</option>
                                            <option value="BV">Bouvet Island</option>
                                            <option value="BR">Brazil</option>
                                            <option value="IO">British Indian Ocean Territory</option>
                                            <option value="BN">Brunei Darussalam</option>
                                            <option value="BG">Bulgaria</option>
                                            <option value="BF">Burkina Faso</option>
                                            <option value="KH">Cambodia</option>
                                            <option value="CM">Cameroon</option>
                                            <option value="CA">Canada</option>
                                            <option value="CV">Cape Verde</option>
                                            <option value="KY">Cayman Islands</option>
                                            <option value="CL">Chile</option>
                                            <option value="CN">China</option>
                                            <option value="CC">Cocos (Keeling) Islands</option>
                                            <option value="CO">Colombia</option>
                                            <option value="KM">Comoros</option>
                                            <option value="CG">Congo</option>
                                            <option value="CK">Cook Islands</option>
                                            <option value="CR">Costa Rica</option>
                                            <option value="HR">Croatia</option>
                                            <option value="CW">Curacao</option>
                                            <option value="CY">Cyprus</option>
                                            <option value="CZ">Czech Republic</option>
                                            <option value="DK">Denmark</option>
                                            <option value="DJ">Djibouti</option>
                                            <option value="DM">Dominica</option>
                                            <option value="DO">Dominican Republic</option>
                                            <option value="EC">Ecuador</option>
                                            <option value="EG">Egypt</option>
                                            <option value="SV">El Salvador</option>
                                            <option value="GQ">Equatorial Guinea</option>
                                            <option value="EE">Estonia</option>
                                            <option value="FK">Falkland Islands</option>
                                            <option value="FO">Faroe Islands</option>
                                            <option value="FJ">Fiji</option>
                                            <option value="FI">Finland</option>
                                            <option value="FR">France</option>
                                            <option value="GF">French Guiana</option>
                                            <option value="PF">French Polynesia</option>
                                            <option value="GA">Gabon</option>
                                            <option value="GM">Gambia</option>
                                            <option value="GE">Georgia</option>
                                            <option value="DE">Germany</option>
                                            <option value="GH">Ghana</option>
                                            <option value="GI">Gibraltar</option>
                                            <option value="GR">Greece</option>
                                            <option value="GL">Greenland</option>
                                            <option value="GD">Grenada</option>
                                            <option value="GP">Guadeloupe</option>
                                            <option value="GU">Guam</option>
                                            <option value="GT">Guatemala</option>
                                            <option value="GG">Guernsey</option>
                                            <option value="GW">Guinea-Bissau</option>
                                            <option value="GY">Guyana</option>
                                            <option value="VA">Holy See</option>
                                            <option value="HN">Honduras</option>
                                            <option value="HK">Hong Kong</option>
                                            <option value="HU">Hungary</option>
                                            <option value="IS">Iceland</option>
                                            <option value="IN">India</option>
                                            <option value="ID">Indonesia</option>
                                            <option value="IE">Ireland</option>
                                            <option value="IM">Isle of Man</option>
                                            <option value="IL">Israel</option>
                                            <option value="IT">Italy</option>
                                            <option value="JM">Jamaica</option>
                                            <option value="JP">Japan</option>
                                            <option value="JE">Jersey</option>
                                            <option value="JO">Jordan</option>
                                            <option value="KZ">Kazakhstan</option>
                                            <option value="KE">Kenya</option>
                                            <option value="KI">Kiribati</option>
                                            <option value="KW">Kuwait</option>
                                            <option value="KG">Kyrgyzstan</option>
                                            <option value="LA">Lao</option>
                                            <option value="LV">Latvia</option>
                                            <option value="LB">Lebanon</option>
                                            <option value="LS">Lesotho</option>
                                            <option value="LI">Liechtenstein</option>
                                            <option value="LT">Lithuania</option>
                                            <option value="LU">Luxembourg</option>
                                            <option value="MO">Macau</option>
                                            <option value="MK">Macedonia</option>
                                            <option value="MG">Madagascar</option>
                                            <option value="MW">Malawi</option>
                                            <option value="MY">Malaysia</option>
                                            <option value="MV">Maldives</option>
                                            <option value="ML">Mali</option>
                                            <option value="MT">Malta</option>
                                            <option value="MH">Marshall Islands</option>
                                            <option value="MQ">Martinique</option>
                                            <option value="MR">Mauritania</option>
                                            <option value="MU">Mauritius</option>
                                            <option value="YT">Mayotte</option>
                                            <option value="MX">Mexico</option>
                                            <option value="FM">Micronesia</option>
                                            <option value="MD">Moldova</option>
                                            <option value="MC">Monaco</option>
                                            <option value="MN">Mongolia</option>
                                            <option value="ME">Montenegro</option>
                                            <option value="MS">Montserrat</option>
                                            <option value="MA">Morocco</option>
                                            <option value="MZ">Mozambique</option>
                                            <option value="NA">Namibia</option>
                                            <option value="NR">Nauru</option>
                                            <option value="NP">Nepal</option>
                                            <option value="NL">Netherlands</option>
                                            <option value="AN">Netherlands Antilles</option>
                                            <option value="NC">New Caledonia</option>
                                            <option value="NZ">New Zealand</option>
                                            <option value="NI">Nicaragua</option>
                                            <option value="NE">Niger</option>
                                            <option value="NG">Nigeria</option>
                                            <option value="NU">Niue</option>
                                            <option value="NF">Norfolk Island</option>
                                            <option value="MP">Northern Mariana Islands</option>
                                            <option value="NO">Norway</option>
                                            <option value="OM">Oman</option>
                                            <option value="PK">Pakistan</option>
                                            <option value="PW">Palau</option>
                                            <option value="PS">Palestinian Territory</option>
                                            <option value="PA">Panama</option>
                                            <option value="PG">Papua New Guinea</option>
                                            <option value="PY">Paraguay</option>
                                            <option value="PE">Peru</option>
                                            <option value="PH">Philippines</option>
                                            <option value="PN">Pitcairn Islands</option>
                                            <option value="PL">Poland</option>
                                            <option value="PT">Portugal</option>
                                            <option value="QA">Qatar</option>
                                            <option value="RE">Reunion</option>
                                            <option value="RO">Romania</option>
                                            <option value="RU">Russia</option>
                                            <option value="RW">Rwanda</option>
                                            <option value="SH">Saint Helena</option>
                                            <option value="KN">Saint Kitts and Nevis</option>
                                            <option value="LC">Saint Lucia</option>
                                            <option value="MF">Saint Martin</option>
                                            <option value="PM">Saint Pierre and Miquelon</option>
                                            <option value="VC">Saint Vincent and the Grenadines</option>
                                            <option value="WS">Samoa</option>
                                            <option value="SM">San Marino</option>
                                            <option value="ST">Sao Tome and Principe</option>
                                            <option value="SA">Saudi Arabia</option>
                                            <option value="SN">Senegal</option>
                                            <option value="RS">Serbia</option>
                                            <option value="SC">Seychelles</option>
                                            <option value="SL">Sierra Leone</option>
                                            <option value="SG">Singapore</option>
                                            <option value="SX">Sint Maarten</option>
                                            <option value="SK">Slovakia</option>
                                            <option value="SI">Slovenia</option>
                                            <option value="SB">Solomon Islands</option>
                                            <option value="ZA">South Africa</option>
                                            <option value="KR">South Korea</option>
                                            <option value="ES">Spain</option>
                                            <option value="LK">Sri Lanka</option>
                                            <option value="SR">Suriname</option>
                                            <option value="SJ">Svalbard and Jan Mayen</option>
                                            <option value="SZ">Swaziland</option>
                                            <option value="SE">Sweden</option>
                                            <option value="CH">Switzerland</option>
                                            <option value="TW">Taiwan</option>
                                            <option value="TJ">Tajikistan</option>
                                            <option value="TZ">Tanzania</option>
                                            <option value="TH">Thailand</option>
                                            <option value="TL">Timor-Leste</option>
                                            <option value="TG">Togo</option>
                                            <option value="TK">Tokelau</option>
                                            <option value="TO">Tonga</option>
                                            <option value="TT">Trinidad and Tobago</option>
                                            <option value="TN">Tunisia</option>
                                            <option value="TR">Turkey</option>
                                            <option value="TM">Turkmenistan</option>
                                            <option value="TC">Turks and Caicos Islands</option>
                                            <option value="TV">Tuvalu</option>
                                            <option value="UA">Ukraine</option>
                                            <option value="AE">United Arab Emirates</option>
                                            <option value="GB">United Kingdom</option>
                                            <option value="UY">Uruguay</option>
                                            <option value="UZ">Uzbekistan</option>
                                            <option value="VU">Vanuatu</option>
                                            <option value="VE">Venezuela</option>
                                            <option value="VN">Vietnam</option>
                                            <option value="VG">Virgin Islands, British</option>
                                            <option value="WF">Wallis and Futuna</option>
                                            <option value="EH">Western Sahara</option>
                                            <option value="ZM">Zambia</option>
                                        </select>
                                        <div id="_dfcntr_chzn" class="chzn-container chzn-container-single "onmouseleave="selectOff(this)" onclick="openSelectList(this)" style="width: 220px;">
                                            <a href="javascript:void(0)" class="chzn-single"><span>All countries</span>
                                                <div ><b></b>
                                                </div>
                                            </a>
                                            <div class="chzn-drop" style="left: -9000px; width: 218px; top: 30px;">
                                                <div class="chzn-search">
                                                    <input type="text" autocomplete="off" style="width: 175px;">
                                                </div>
                                                <ul class="chzn-results" tabindex="-1">
                                                    <li id="_dfcntr_chzn_o_0" class="active-result result-selected" style="">All countries</li><li id="_dfcntr_chzn_o_1" class="active-result" style="">Aland Islands</li><li id="_dfcntr_chzn_o_2" class="active-result" style="">Albania</li><li id="_dfcntr_chzn_o_3" class="active-result" style="">Algeria</li><li id="_dfcntr_chzn_o_4" class="active-result" style="">American Samoa</li><li id="_dfcntr_chzn_o_5" class="active-result" style="">Andorra</li><li id="_dfcntr_chzn_o_6" class="active-result" style="">Angola</li><li id="_dfcntr_chzn_o_7" class="active-result" style="">Anguilla</li><li id="_dfcntr_chzn_o_8" class="active-result" style="">Antarctica</li><li id="_dfcntr_chzn_o_9" class="active-result" style="">Antigua and Barbuda</li><li id="_dfcntr_chzn_o_10" class="active-result" style="">Argentina</li><li id="_dfcntr_chzn_o_11" class="active-result" style="">Armenia</li><li id="_dfcntr_chzn_o_12" class="active-result" style="">Aruba</li><li id="_dfcntr_chzn_o_13" class="active-result" style="">Australia</li><li id="_dfcntr_chzn_o_14" class="active-result" style="">Austria</li><li id="_dfcntr_chzn_o_15" class="active-result" style="">Azerbaijan</li><li id="_dfcntr_chzn_o_16" class="active-result" style="">Bahamas</li><li id="_dfcntr_chzn_o_17" class="active-result" style="">Bahrain</li><li id="_dfcntr_chzn_o_18" class="active-result" style="">Bangladesh</li><li id="_dfcntr_chzn_o_19" class="active-result" style="">Barbados</li><li id="_dfcntr_chzn_o_20" class="active-result" style="">Belarus</li><li id="_dfcntr_chzn_o_21" class="active-result" style="">Belgium</li><li id="_dfcntr_chzn_o_22" class="active-result" style="">Benin</li><li id="_dfcntr_chzn_o_23" class="active-result" style="">Bermuda</li><li id="_dfcntr_chzn_o_24" class="active-result" style="">Bhutan</li><li id="_dfcntr_chzn_o_25" class="active-result" style="">Bolivia</li><li id="_dfcntr_chzn_o_26" class="active-result" style="">Bosnia and Herzegovina</li><li id="_dfcntr_chzn_o_27" class="active-result" style="">Botswana</li><li id="_dfcntr_chzn_o_28" class="active-result" style="">Bouvet Island</li><li id="_dfcntr_chzn_o_29" class="active-result" style="">Brazil</li><li id="_dfcntr_chzn_o_30" class="active-result" style="">British Indian Ocean Territory</li><li id="_dfcntr_chzn_o_31" class="active-result" style="">Brunei Darussalam</li><li id="_dfcntr_chzn_o_32" class="active-result" style="">Bulgaria</li><li id="_dfcntr_chzn_o_33" class="active-result" style="">Burkina Faso</li><li id="_dfcntr_chzn_o_34" class="active-result" style="">Cambodia</li><li id="_dfcntr_chzn_o_35" class="active-result" style="">Cameroon</li><li id="_dfcntr_chzn_o_36" class="active-result" style="">Canada</li><li id="_dfcntr_chzn_o_37" class="active-result" style="">Cape Verde</li><li id="_dfcntr_chzn_o_38" class="active-result" style="">Cayman Islands</li><li id="_dfcntr_chzn_o_39" class="active-result" style="">Chile</li><li id="_dfcntr_chzn_o_40" class="active-result" style="">China</li><li id="_dfcntr_chzn_o_41" class="active-result" style="">Cocos (Keeling) Islands</li><li id="_dfcntr_chzn_o_42" class="active-result" style="">Colombia</li><li id="_dfcntr_chzn_o_43" class="active-result" style="">Comoros</li><li id="_dfcntr_chzn_o_44" class="active-result" style="">Congo</li><li id="_dfcntr_chzn_o_45" class="active-result" style="">Cook Islands</li><li id="_dfcntr_chzn_o_46" class="active-result" style="">Costa Rica</li><li id="_dfcntr_chzn_o_47" class="active-result" style="">Croatia</li><li id="_dfcntr_chzn_o_48" class="active-result" style="">Curacao</li><li id="_dfcntr_chzn_o_49" class="active-result" style="">Cyprus</li><li id="_dfcntr_chzn_o_50" class="active-result" style="">Czech Republic</li><li id="_dfcntr_chzn_o_51" class="active-result" style="">Denmark</li><li id="_dfcntr_chzn_o_52" class="active-result" style="">Djibouti</li><li id="_dfcntr_chzn_o_53" class="active-result" style="">Dominica</li><li id="_dfcntr_chzn_o_54" class="active-result" style="">Dominican Republic</li><li id="_dfcntr_chzn_o_55" class="active-result" style="">Ecuador</li><li id="_dfcntr_chzn_o_56" class="active-result" style="">Egypt</li><li id="_dfcntr_chzn_o_57" class="active-result" style="">El Salvador</li><li id="_dfcntr_chzn_o_58" class="active-result" style="">Equatorial Guinea</li><li id="_dfcntr_chzn_o_59" class="active-result" style="">Estonia</li><li id="_dfcntr_chzn_o_60" class="active-result" style="">Falkland Islands</li><li id="_dfcntr_chzn_o_61" class="active-result" style="">Faroe Islands</li><li id="_dfcntr_chzn_o_62" class="active-result" style="">Fiji</li><li id="_dfcntr_chzn_o_63" class="active-result" style="">Finland</li><li id="_dfcntr_chzn_o_64" class="active-result" style="">France</li><li id="_dfcntr_chzn_o_65" class="active-result" style="">French Guiana</li><li id="_dfcntr_chzn_o_66" class="active-result" style="">French Polynesia</li><li id="_dfcntr_chzn_o_67" class="active-result" style="">Gabon</li><li id="_dfcntr_chzn_o_68" class="active-result" style="">Gambia</li><li id="_dfcntr_chzn_o_69" class="active-result" style="">Georgia</li><li id="_dfcntr_chzn_o_70" class="active-result" style="">Germany</li><li id="_dfcntr_chzn_o_71" class="active-result" style="">Ghana</li><li id="_dfcntr_chzn_o_72" class="active-result" style="">Gibraltar</li><li id="_dfcntr_chzn_o_73" class="active-result" style="">Greece</li><li id="_dfcntr_chzn_o_74" class="active-result" style="">Greenland</li><li id="_dfcntr_chzn_o_75" class="active-result" style="">Grenada</li><li id="_dfcntr_chzn_o_76" class="active-result" style="">Guadeloupe</li><li id="_dfcntr_chzn_o_77" class="active-result" style="">Guam</li><li id="_dfcntr_chzn_o_78" class="active-result" style="">Guatemala</li><li id="_dfcntr_chzn_o_79" class="active-result" style="">Guernsey</li><li id="_dfcntr_chzn_o_80" class="active-result" style="">Guinea-Bissau</li><li id="_dfcntr_chzn_o_81" class="active-result" style="">Guyana</li><li id="_dfcntr_chzn_o_82" class="active-result" style="">Holy See</li><li id="_dfcntr_chzn_o_83" class="active-result" style="">Honduras</li><li id="_dfcntr_chzn_o_84" class="active-result" style="">Hong Kong</li><li id="_dfcntr_chzn_o_85" class="active-result" style="">Hungary</li><li id="_dfcntr_chzn_o_86" class="active-result" style="">Iceland</li><li id="_dfcntr_chzn_o_87" class="active-result" style="">India</li><li id="_dfcntr_chzn_o_88" class="active-result" style="">Indonesia</li><li id="_dfcntr_chzn_o_89" class="active-result" style="">Ireland</li><li id="_dfcntr_chzn_o_90" class="active-result" style="">Isle of Man</li><li id="_dfcntr_chzn_o_91" class="active-result" style="">Israel</li><li id="_dfcntr_chzn_o_92" class="active-result" style="">Italy</li><li id="_dfcntr_chzn_o_93" class="active-result" style="">Jamaica</li><li id="_dfcntr_chzn_o_94" class="active-result" style="">Japan</li><li id="_dfcntr_chzn_o_95" class="active-result" style="">Jersey</li><li id="_dfcntr_chzn_o_96" class="active-result" style="">Jordan</li><li id="_dfcntr_chzn_o_97" class="active-result" style="">Kazakhstan</li><li id="_dfcntr_chzn_o_98" class="active-result" style="">Kenya</li><li id="_dfcntr_chzn_o_99" class="active-result" style="">Kiribati</li><li id="_dfcntr_chzn_o_100" class="active-result" style="">Kuwait</li><li id="_dfcntr_chzn_o_101" class="active-result" style="">Kyrgyzstan</li><li id="_dfcntr_chzn_o_102" class="active-result" style="">Lao</li><li id="_dfcntr_chzn_o_103" class="active-result" style="">Latvia</li><li id="_dfcntr_chzn_o_104" class="active-result" style="">Lebanon</li><li id="_dfcntr_chzn_o_105" class="active-result" style="">Lesotho</li><li id="_dfcntr_chzn_o_106" class="active-result" style="">Liechtenstein</li><li id="_dfcntr_chzn_o_107" class="active-result" style="">Lithuania</li><li id="_dfcntr_chzn_o_108" class="active-result" style="">Luxembourg</li><li id="_dfcntr_chzn_o_109" class="active-result" style="">Macau</li><li id="_dfcntr_chzn_o_110" class="active-result" style="">Macedonia</li><li id="_dfcntr_chzn_o_111" class="active-result" style="">Madagascar</li><li id="_dfcntr_chzn_o_112" class="active-result" style="">Malawi</li><li id="_dfcntr_chzn_o_113" class="active-result" style="">Malaysia</li><li id="_dfcntr_chzn_o_114" class="active-result" style="">Maldives</li><li id="_dfcntr_chzn_o_115" class="active-result" style="">Mali</li><li id="_dfcntr_chzn_o_116" class="active-result" style="">Malta</li><li id="_dfcntr_chzn_o_117" class="active-result" style="">Marshall Islands</li><li id="_dfcntr_chzn_o_118" class="active-result" style="">Martinique</li><li id="_dfcntr_chzn_o_119" class="active-result" style="">Mauritania</li><li id="_dfcntr_chzn_o_120" class="active-result" style="">Mauritius</li><li id="_dfcntr_chzn_o_121" class="active-result" style="">Mayotte</li><li id="_dfcntr_chzn_o_122" class="active-result" style="">Mexico</li><li id="_dfcntr_chzn_o_123" class="active-result" style="">Micronesia</li><li id="_dfcntr_chzn_o_124" class="active-result" style="">Moldova</li><li id="_dfcntr_chzn_o_125" class="active-result" style="">Monaco</li><li id="_dfcntr_chzn_o_126" class="active-result" style="">Mongolia</li><li id="_dfcntr_chzn_o_127" class="active-result" style="">Montenegro</li><li id="_dfcntr_chzn_o_128" class="active-result" style="">Montserrat</li><li id="_dfcntr_chzn_o_129" class="active-result" style="">Morocco</li><li id="_dfcntr_chzn_o_130" class="active-result" style="">Mozambique</li><li id="_dfcntr_chzn_o_131" class="active-result" style="">Namibia</li><li id="_dfcntr_chzn_o_132" class="active-result" style="">Nauru</li><li id="_dfcntr_chzn_o_133" class="active-result" style="">Nepal</li><li id="_dfcntr_chzn_o_134" class="active-result" style="">Netherlands</li><li id="_dfcntr_chzn_o_135" class="active-result" style="">Netherlands Antilles</li><li id="_dfcntr_chzn_o_136" class="active-result" style="">New Caledonia</li><li id="_dfcntr_chzn_o_137" class="active-result" style="">New Zealand</li><li id="_dfcntr_chzn_o_138" class="active-result" style="">Nicaragua</li><li id="_dfcntr_chzn_o_139" class="active-result" style="">Niger</li><li id="_dfcntr_chzn_o_140" class="active-result" style="">Nigeria</li><li id="_dfcntr_chzn_o_141" class="active-result" style="">Niue</li><li id="_dfcntr_chzn_o_142" class="active-result" style="">Norfolk Island</li><li id="_dfcntr_chzn_o_143" class="active-result" style="">Northern Mariana Islands</li><li id="_dfcntr_chzn_o_144" class="active-result" style="">Norway</li><li id="_dfcntr_chzn_o_145" class="active-result" style="">Oman</li><li id="_dfcntr_chzn_o_146" class="active-result" style="">Pakistan</li><li id="_dfcntr_chzn_o_147" class="active-result" style="">Palau</li><li id="_dfcntr_chzn_o_148" class="active-result" style="">Palestinian Territory</li><li id="_dfcntr_chzn_o_149" class="active-result" style="">Panama</li><li id="_dfcntr_chzn_o_150" class="active-result" style="">Papua New Guinea</li><li id="_dfcntr_chzn_o_151" class="active-result" style="">Paraguay</li><li id="_dfcntr_chzn_o_152" class="active-result" style="">Peru</li><li id="_dfcntr_chzn_o_153" class="active-result" style="">Philippines</li><li id="_dfcntr_chzn_o_154" class="active-result" style="">Pitcairn Islands</li><li id="_dfcntr_chzn_o_155" class="active-result" style="">Poland</li><li id="_dfcntr_chzn_o_156" class="active-result" style="">Portugal</li><li id="_dfcntr_chzn_o_157" class="active-result" style="">Qatar</li><li id="_dfcntr_chzn_o_158" class="active-result" style="">Reunion</li><li id="_dfcntr_chzn_o_159" class="active-result" style="">Romania</li><li id="_dfcntr_chzn_o_160" class="active-result" style="">Russia</li><li id="_dfcntr_chzn_o_161" class="active-result" style="">Rwanda</li><li id="_dfcntr_chzn_o_162" class="active-result" style="">Saint Helena</li><li id="_dfcntr_chzn_o_163" class="active-result" style="">Saint Kitts and Nevis</li><li id="_dfcntr_chzn_o_164" class="active-result" style="">Saint Lucia</li><li id="_dfcntr_chzn_o_165" class="active-result" style="">Saint Martin</li><li id="_dfcntr_chzn_o_166" class="active-result" style="">Saint Pierre and Miquelon</li><li id="_dfcntr_chzn_o_167" class="active-result" style="">Saint Vincent and the Grenadines</li><li id="_dfcntr_chzn_o_168" class="active-result" style="">Samoa</li><li id="_dfcntr_chzn_o_169" class="active-result" style="">San Marino</li><li id="_dfcntr_chzn_o_170" class="active-result" style="">Sao Tome and Principe</li><li id="_dfcntr_chzn_o_171" class="active-result" style="">Saudi Arabia</li><li id="_dfcntr_chzn_o_172" class="active-result" style="">Senegal</li><li id="_dfcntr_chzn_o_173" class="active-result" style="">Serbia</li><li id="_dfcntr_chzn_o_174" class="active-result" style="">Seychelles</li><li id="_dfcntr_chzn_o_175" class="active-result" style="">Sierra Leone</li><li id="_dfcntr_chzn_o_176" class="active-result" style="">Singapore</li><li id="_dfcntr_chzn_o_177" class="active-result" style="">Sint Maarten</li><li id="_dfcntr_chzn_o_178" class="active-result" style="">Slovakia</li><li id="_dfcntr_chzn_o_179" class="active-result" style="">Slovenia</li><li id="_dfcntr_chzn_o_180" class="active-result" style="">Solomon Islands</li><li id="_dfcntr_chzn_o_181" class="active-result" style="">South Africa</li><li id="_dfcntr_chzn_o_182" class="active-result" style="">South Korea</li><li id="_dfcntr_chzn_o_183" class="active-result" style="">Spain</li><li id="_dfcntr_chzn_o_184" class="active-result" style="">Sri Lanka</li><li id="_dfcntr_chzn_o_185" class="active-result" style="">Suriname</li><li id="_dfcntr_chzn_o_186" class="active-result" style="">Svalbard and Jan Mayen</li><li id="_dfcntr_chzn_o_187" class="active-result" style="">Swaziland</li><li id="_dfcntr_chzn_o_188" class="active-result" style="">Sweden</li><li id="_dfcntr_chzn_o_189" class="active-result" style="">Switzerland</li><li id="_dfcntr_chzn_o_190" class="active-result" style="">Taiwan</li><li id="_dfcntr_chzn_o_191" class="active-result" style="">Tajikistan</li><li id="_dfcntr_chzn_o_192" class="active-result" style="">Tanzania</li><li id="_dfcntr_chzn_o_193" class="active-result" style="">Thailand</li><li id="_dfcntr_chzn_o_194" class="active-result" style="">Timor-Leste</li><li id="_dfcntr_chzn_o_195" class="active-result" style="">Togo</li><li id="_dfcntr_chzn_o_196" class="active-result" style="">Tokelau</li><li id="_dfcntr_chzn_o_197" class="active-result" style="">Tonga</li><li id="_dfcntr_chzn_o_198" class="active-result" style="">Trinidad and Tobago</li><li id="_dfcntr_chzn_o_199" class="active-result" style="">Tunisia</li><li id="_dfcntr_chzn_o_200" class="active-result" style="">Turkey</li><li id="_dfcntr_chzn_o_201" class="active-result" style="">Turkmenistan</li><li id="_dfcntr_chzn_o_202" class="active-result" style="">Turks and Caicos Islands</li><li id="_dfcntr_chzn_o_203" class="active-result" style="">Tuvalu</li><li id="_dfcntr_chzn_o_204" class="active-result" style="">Ukraine</li><li id="_dfcntr_chzn_o_205" class="active-result" style="">United Arab Emirates</li><li id="_dfcntr_chzn_o_206" class="active-result" style="">United Kingdom</li><li id="_dfcntr_chzn_o_207" class="active-result" style="">Uruguay</li><li id="_dfcntr_chzn_o_208" class="active-result" style="">Uzbekistan</li><li id="_dfcntr_chzn_o_209" class="active-result" style="">Vanuatu</li><li id="_dfcntr_chzn_o_210" class="active-result" style="">Venezuela</li><li id="_dfcntr_chzn_o_211" class="active-result" style="">Vietnam</li><li id="_dfcntr_chzn_o_212" class="active-result" style="">Virgin Islands, British</li><li id="_dfcntr_chzn_o_213" class="active-result" style="">Wallis and Futuna</li><li id="_dfcntr_chzn_o_214" class="active-result" style="">Western Sahara</li><li id="_dfcntr_chzn_o_215" class="active-result" style="">Zambia</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="p-reg-form-item-error">
                                <p>
                                    <b><span class="rf-msg " id="j_idt619"></span></b>
                                </p>
                            </div>
                            <div class="global-error-container">
                                <div class="p-reg-form-item-error" style="position: relative; top: -25px; margin-left: 7px;">
                                    <i class="arr" style="width: 7px; height: 14px; background: url(../img/reg-tip-arrows.png) -7px 0px; top: 17px; left: -7px; margin-left: 0px;"></i>
                                    <p>
                                        <b><span class="rf-msgs " id="j_idt621"></span>
                                        </b>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <span id="lastUsedInputMethod"></span>
                        <span id="inputMethods">

                            <div class="p2-payment__hr"></div>
                            <div class="p2-payment__block active">
                                <div id="block-title0" class="p2-payment__block-title">Банковские карты
                                    <i></i>
                                </div>
                                <div class="p2-payment__block-contents" style="display: block;">
                                    <table class="p2-payment__block-table p2-payment__block-systems">
                                        <tbody>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Комиссия %</th>
                                                <th>Комиссия</th>
                                                <th>Срок зачисления</th>
                                                <th class="right">Будет списано</th>
                                                <th></th>
                                            </tr>
                                            <tr class="">
												<td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/visa-logo.png') }}" style="height: 14px;" /></td>
												<td class="p2-payment__block-systems-title">VISA</td>
												<td class="p2-payment__block-systems-percent"><span class="green">3.50%</span></td>
												<td class="p2-payment__block-systems-comission">0.00 USD</td>
												<td class="p2-payment__block-systems-time"><span class="green">Моментально</span></td>
												<td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:0:j_idt648:0:j_idt681" name="j_idt634:0:j_idt648:0:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:0:j_idt648:0:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_0_0')&quot;} );return false;" class="p2-button deposit_action_0_0">ПОПОЛНИТЬ</a>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/mastercard-logo.png') }}" style="height: 34px;">
                                                </td>
                                                <td class="p2-payment__block-systems-title">Mastercard</td>
                                                <td class="p2-payment__block-systems-percent">
                                                    <span class="green">3.50%</span>
                                                </td>
                                                <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                <td class="p2-payment__block-systems-time">
                                                    <span class="green">Моментально</span>
                                                </td>
                                                <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:0:j_idt648:1:j_idt681" name="j_idt634:0:j_idt648:1:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:0:j_idt648:1:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_0_1')&quot;} );return false;" class="p2-button deposit_action_0_1">ПОПОЛНИТЬ</a>
                                                </td>
                                            </tr>
                                            <tr class="">
                                                <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/national-payment-system-mir.png') }}" style="height: 14px;">
                                                </td>
                                                <td class="p2-payment__block-systems-title">МИР</td>
                                                <td class="p2-payment__block-systems-percent">
                                                    <span class="green">3.50%</span>
                                                </td>
                                                <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                <td class="p2-payment__block-systems-time">
                                                    <span class="green">Моментально</span>
                                                </td>
                                                <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:0:j_idt648:6:j_idt681" name="j_idt634:0:j_idt648:6:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:0:j_idt648:6:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_0_6')&quot;} );return false;" class="p2-button deposit_action_0_6">ПОПОЛНИТЬ</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="p2-payment__hr"></div>
                            <div class="p2-payment__block active">
                                <div id="block-title1" class="p2-payment__block-title">Быстрая покупка криптовалюты
                                    <i></i>
                                </div>
                                <div class="p2-payment__block-contents" style="display: block;">
                                    <table class="p2-payment__block-table p2-payment__block-systems">
                                        <tbody>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Комиссия %</th>
                                                <th>Комиссия</th>
                                                <th>Срок зачисления</th>
                                                <th class="right">Будет списано</th>
                                                <th></th>
                                            </tr>
                                            <tr class="">
												<td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/cryptocurrency-logo.png') }}" style="height: 34px;" /></td>
												<td class="p2-payment__block-systems-title">Купить криптовалюту с карты</td>
												<td class="p2-payment__block-systems-percent"><span class="green">0.00%</span></td>
												<td class="p2-payment__block-systems-comission">0.00 USD</td>
												<td class="p2-payment__block-systems-time"><span class="green">Моментально</span></td>
												<td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:1:j_idt648:0:j_idt681" name="j_idt634:1:j_idt648:0:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:1:j_idt648:0:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_1_0')&quot;} );return false;" class="p2-button deposit_action_1_0">ПОПОЛНИТЬ</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="p2-payment__hr"></div>
                            <div class="p2-payment__block active">
                                <div id="block-title2" class="p2-payment__block-title">Банковские переводы
                                    <i></i>
                                </div>
                                <div class="p2-payment__block-contents" style="display: block;">
                                    <table class="p2-payment__block-table p2-payment__block-systems">
                                        <tbody>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>Комиссия %</th>
                                                <th>Комиссия</th>
                                                <th>Срок зачисления</th>
                                                <th class="right">Будет списано</th>
                                                <th></th>
                                            </tr>
                                            <tr class="">
                                                <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/sepa-logo.png') }}" style="height: 16px;"></td>
												<td class="p2-payment__block-systems-title">SEPA</td>
												<td class="p2-payment__block-systems-percent"><span class="green">0.30%</span> + $3.26</td>
												<td class="p2-payment__block-systems-comission">3.27 USD</td>
												<td class="p2-payment__block-systems-time">3 рабочих дня</td>
												<td class="p2-payment__block-systems-sum"><b>3.27</b> USD</td> 
												<td class="p2-payment__block-table-action"><a href="#" id="j_idt634:2:j_idt648:0:j_idt681" name="j_idt634:2:j_idt648:0:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:2:j_idt648:0:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_2_0')&quot;} );return false;" class="p2-button deposit_action_2_0">ПОПОЛНИТЬ</a>
                                                </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="p2-payment__hr"></div>
                                <div class="p2-payment__block active">
                                    <div id="block-title4" class="p2-payment__block-title">Интернет-банкинг
                                        <i></i>
                                    </div>
                                    <div class="p2-payment__block-contents" style="display: block;">
                                        <table class="p2-payment__block-table p2-payment__block-systems">
                                            <tbody>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th>Комиссия %</th>
                                                    <th>Комиссия</th>
                                                    <th>Срок зачисления</th>
                                                    <th class="right">Будет списано</th>
                                                    <th></th>
                                                </tr>
                                                <tr class="">
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/alfaclick_logo.png') }}" style="height: 29px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Alfa-Click (Казахстан)</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">3.00%</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">
                                                        <span class="green">Моментально</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:4:j_idt648:0:j_idt681" name="j_idt634:4:j_idt648:0:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:4:j_idt648:0:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_4_0')&quot;} );return false;" class="p2-button deposit_action_4_0">ПОПОЛНИТЬ</a>
                                                    </td>
                                                </tr>
                                                <tr class="">
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/atf24_logo.png') }}" style="height: 24px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">ATF24</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">3.00%</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">
                                                        <span class="green">Моментально</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:4:j_idt648:1:j_idt681" name="j_idt634:4:j_idt648:1:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:4:j_idt648:1:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_4_1')&quot;} );return false;" class="p2-button deposit_action_4_1">ПОПОЛНИТЬ</a>
                                                    </td>
                                                </tr>
                                                <tr class="">
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/BankRBK_logo.png') }}" style="height: 14px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Bank RBK 24</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">3.00%</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">
                                                        <span class="green">Моментально</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:4:j_idt648:2:j_idt681" name="j_idt634:4:j_idt648:2:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:4:j_idt648:2:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_4_2')&quot;} );return false;" class="p2-button deposit_action_4_2">ПОПОЛНИТЬ</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="p2-payment__hr"></div>
                                <div class="p2-payment__block active">
                                    <div id="block-title5" class="p2-payment__block-title">Электронные деньги
                                        <i></i>
                                    </div>
                                    <div class="p2-payment__block-contents" style="display: block;">
                                        <table class="p2-payment__block-table p2-payment__block-systems">
                                            <tbody>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th>Комиссия %</th>
                                                    <th>Комиссия</th>
                                                    <th>Срок зачисления</th>
                                                    <th class="right">Будет списано</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/bitcoin.png') }}" style="height: 26px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Bitcoin</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">0.00%</span>
                                                    </td>

                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">6 подтверждений<br>сети Bitcoin</td>

                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:5:j_idt648:0:j_idt664" name="j_idt634:5:j_idt648:0:j_idt664" onclick="RichFaces.ajax(&quot;j_idt634:5:j_idt648:0:j_idt664&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_0');&quot;} );return false;" class="p2-button deposit_action_5_0">Пополнить</a>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/litecoin.png') }}" style="height: 26px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Litecoin</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">0.00%</span>
                                                    </td>

                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">6 подтверждений<br>сети Litecoin</td>

                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:5:j_idt648:1:j_idt664" name="j_idt634:5:j_idt648:1:j_idt664" onclick="RichFaces.ajax(&quot;j_idt634:5:j_idt648:1:j_idt664&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_1');&quot;} );return false;" class="p2-button deposit_action_5_1">Пополнить</a>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/bitcoin-cash.png') }}" style="height: 26px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Bitcoin Cash</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">0.00%</span>
                                                    </td>

                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">6 подтверждений<br>сети Bitcoin Cash</td>

                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:5:j_idt648:2:j_idt664" name="j_idt634:5:j_idt648:2:j_idt664" onclick="RichFaces.ajax(&quot;j_idt634:5:j_idt648:2:j_idt664&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_2');&quot;} );return false;" class="p2-button deposit_action_5_2">Пополнить</a>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/ripple-logo.png') }}" style="height: 26px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Ripple</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">0.00%</span>
                                                    </td>

                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">
                                                        <span class="green">Моментально</span>
                                                    </td>

                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:5:j_idt648:3:j_idt664" name="j_idt634:5:j_idt648:3:j_idt664" onclick="RichFaces.ajax(&quot;j_idt634:5:j_idt648:3:j_idt664&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_3');&quot;} );return false;" class="p2-button deposit_action_5_3">Пополнить</a>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/ethereum-logo.png') }}" style="height: 26px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Ethereum</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">0.00%</span>
                                                    </td>

                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">35 подтверждений<br>сети Ethereum</td>

                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:5:j_idt648:4:j_idt664" name="j_idt634:5:j_idt648:4:j_idt664" onclick="RichFaces.ajax(&quot;j_idt634:5:j_idt648:4:j_idt664&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_4');&quot;} );return false;" class="p2-button deposit_action_5_4">Пополнить</a>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/zcash-logo.png') }}" style="height: 26px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Zcash</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">0.00%</span>
                                                    </td>

                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">18 подтверждений<br>сети Zcash</td>

                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:5:j_idt648:5:j_idt664" name="j_idt634:5:j_idt648:5:j_idt664" onclick="RichFaces.ajax(&quot;j_idt634:5:j_idt648:5:j_idt664&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_5');&quot;} );return false;" class="p2-button deposit_action_5_5">Пополнить</a>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/tether-logo.png') }}" style="height: 26px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Tether</td>
                                                    <td class="p2-payment__block-systems-percent">0.50%</td>

                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">6 подтверждений<br>сети Bitcoin<br>18 подтверждений<br>сети Ethereum</td>

                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:5:j_idt648:6:j_idt664" name="j_idt634:5:j_idt648:6:j_idt664" onclick="RichFaces.ajax(&quot;j_idt634:5:j_idt648:6:j_idt664&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_6');&quot;} );return false;" class="p2-button deposit_action_5_6">Пополнить</a>

                                                    </td>
                                                </tr>
												<tr>
													<td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/brztoken.png') }}" style="height: 26px;" /></td>
													<td class="p2-payment__block-systems-title">BRZ Token</td>
													<td class="p2-payment__block-systems-percent"><span class="green">3.00%</span> + $0.91</td>

													<td class="p2-payment__block-systems-comission">0.94 USD</td>
													<td class="p2-payment__block-systems-time">16 подтверждений<br/>сети Ethereum</td>

													<td class="p2-payment__block-systems-sum"><b>0.94</b> USD</td>
													<td class="p2-payment__block-table-action"><a href="#" id="j_idt277:5:j_idt291:7:j_idt307" name="j_idt277:5:j_idt291:7:j_idt307" onclick="RichFaces.ajax(&quot;j_idt277:5:j_idt291:7:j_idt307&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_7');&quot;} );return false;" class="p2-button deposit_action_5_7">Пополнить</a>

													</td>
												</tr>
                                                <tr class="">
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/yandex-money.png') }}" style="height: 34px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">Яндекс.Деньги</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">3.50%</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">
                                                        <span class="green">Моментально</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:5:j_idt648:7:j_idt681" name="j_idt634:5:j_idt648:7:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:5:j_idt648:7:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_7')&quot;} );return false;" class="p2-button deposit_action_5_7">ПОПОЛНИТЬ</a>
                                                    </td>
                                                </tr>
                                                <tr class="">
                                                    <td class="p2-payment__block-systems-logo"><img src="{{ asset('assets/cab/images/qiwi_wallet.png') }}" style="height: 24px;">
                                                    </td>
                                                    <td class="p2-payment__block-systems-title">QIWI</td>
                                                    <td class="p2-payment__block-systems-percent">
                                                        <span class="green">3.00%</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-comission">0.00 USD</td>
                                                    <td class="p2-payment__block-systems-time">
                                                        <span class="green">Моментально</span>
                                                    </td>
                                                    <td class="p2-payment__block-systems-sum"><b>0.00</b> USD</td>
                                                    <td class="p2-payment__block-table-action"><a href="#" id="j_idt634:5:j_idt648:8:j_idt681" name="j_idt634:5:j_idt648:8:j_idt681" onclick="RichFaces.ajax(&quot;j_idt634:5:j_idt648:8:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_action_5_8')&quot;} );return false;" class="p2-button deposit_action_5_8">ПОПОЛНИТЬ</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="p2-payment__hr"></div>
                                <div class="p2-payment__block active">
                                    <div id="block-title5" class="p2-payment__block-title">Наличные
                                        <i></i>
                                    </div>
                                    <div class="p2-payment__block-contents" style="display: block;">
                                        <div class="p2-payment__alert" style="margin: 21px 0 37px;">
                                            <i></i>
                                            <p>Все операции с наличностью осуществляются сторонним сервисом, за работу которого мы не несём ответственности.
                                            </p>
                                        </div>
                                        <ul class="p2-payment__block-cash-tabs">
                                            <li><a class="cp">Россия</a></li>
                                            <li class="active"><a class="cp">Украина</a></li>
                                            <li><a class="cp">Турция</a></li>
                                            <li><a class="cp">ОАЭ</a></li>
                                            <li><a class="cp">Молдова</a></li>
                                            <li><a class="cp">Азербайджан</a></li>
                                            <li><a class="cp">Грузия</a></li>
                                            <li><a class="cp">Великобритания</a></li>
                                            <li><a class="cp">Китай</a></li>
                                            <li><a class="cp">Узбекистан</a></li>
                                            <li><a class="cp">Другие страны</a></li>
                                        </ul>
                                        <div class="p2-payment__block-cash-tabs-content">
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                                RUB в ADV
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                2% (минимум 1000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                            USD в ADV</td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                2% (минимум 1000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                            EUR в ADV</td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                7% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Благодаря нашему сотрудничеству с одним из крупнейших обменных сервисов на территории России и Украины, у вас появилась возможность пополнять ADV кошелёк наличными в рублях, долларах и евро на территории практически всей Российской Федерации.
                                                    </p>
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt><dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>
                                                        <dl>
                                                            <dt>Телефон:</dt><dd>+7 (495) 204-14-49</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            10:00 - 20:00<br>(время работы по Москве UTC/GMT +3)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item active">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                                USD в ADV
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                1% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Благодаря нашему сотрудничеству с одним из крупнейших обменных сервисов на территории России и Украины, у вас появилась возможность пополнять ADV кошелек наличными в гривне, долларах и евро на территории всей Украины, кроме временно неподконтрольных территорий т.н. ДНР и ЛНР.
                                                    </p>
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt><dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>
                                                        <dl>
                                                            <dt>Телефон:</dt><dd>+38 (044) 379-31-93</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            10:00 - 20:00<br>(время работы по Киеву UTC/GMT +2)
                                                        </dd>
                                                    </dl>
                                                </div>
                                                <div class="p2-payment__hr-dotted"></div>
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                                USD в ADV
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                0% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">5 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                                UAH в ADV USD (по курсу Forex)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                0% (минимум 2000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">5 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                                EUR в ADV USD (по курсу Forex)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                0% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">5 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Обмен доступен на территории всей Украины, кроме временно неподконтрольных территорий т.н. ДНР и ЛНР.
                                                    </p>
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:
                                                    </p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt> Мессенджеры:</dt>
                                                            <dd>Skype:
                                                                <a href="skype:cc.media.network?call">Позвонить/Написать</a><br>Telegram:
                                                                <a href="https://t.me/ccmedianetwork" target="_blank" rel="noopener noreferrer nofollow">Написать</a>
                                                            </dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>&nbsp;  </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            10:00 - 20:00<br>(время работы по Киеву UTC/GMT +2)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                            USD в ADV</td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                1% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt><dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>&nbsp;  </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            10:00 - 20:00<br>(время работы по Стамбулу UTC/GMT +2)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                                AED в ADV
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                1% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Благодаря нашему сотрудничеству с одним из крупнейших обменных сервисов на территории ОАЭ, у вас появилась возможность пополнять ADV кошелек наличными в долларах и евро.
                                                    </p>
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt>
                                                            <dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>  </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            11:00 - 21:00<br>(время работы по Абу-Даби, UTC/GMT +4)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                                USD в ADV
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                2% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Благодаря нашему сотрудничеству с одним из крупнейших обменных сервисов на территории Молдовы, у вас появилась возможность пополнять ADV кошелек наличными в долларах.
                                                    </p>
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt><dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>
                                                    </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            10:00 - 20:00<br>(время работы по Кишиневу, UTC/GMT +2)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                            USD в ADV</td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                1% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt>
                                                            <dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>&nbsp;
                                                    </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            11:00 - 21:00<br>(время работы по Баку, UTC/GMT +4)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                            USD в ADV</td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                1% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt><dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>&nbsp;
                                                    </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            11:00 - 21:00<br>(время работы по Тбилиси, UTC/GMT +4)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                            USD в ADV</td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                3% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt><dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>&nbsp;
                                                    </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            07:00 - 17:00<br>(время работы по Лондону, UTC/GMT 0)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                            USD в ADV</td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                2% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt><dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>&nbsp;
                                                    </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            15:00 - 01:00<br>(время работы по Шанхаю, UTC/GMT +8)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <table class="p2-payment__block-cash-table">
                                                    <tbody>
                                                        <tr>
                                                            <th>Валюта</th>
                                                            <th>Комиссия %</th>
                                                            <th>Срок исполнения</th>
                                                            <th>Тип обмена</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="p2-payment__block-cash-table-currency">
                                                            USD в ADV</td>
                                                            <td class="p2-payment__block-cash-table-comission">
                                                                3% (минимум 5000 USD)
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-time">15 мин. - 24 часа
                                                            </td>
                                                            <td class="p2-payment__block-cash-table-method">РУЧНОЙ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div class="p2-payment__block-cash-terms">
                                                    <p>Свяжитесь с контакт-центром для получения инструкций и проведения транзакций:</p>
                                                </div>
                                                <div class="p2-payment__block-cash-contacts">
                                                    <dl>
                                                        <dl>
                                                            <dt>Мессенджеры:</dt><dd>ICQ: 691286007<br>Skype: <a href="skype:adv.in.out?call">Позвонить/Написать</a><br>Telegram: <a href="https://t.me/advinout" target="_blank" rel="noopener noreferrer nofollow">Написать</a><br>Jabber: cash_in_out@jabb.im</dd>
                                                        </dl>
                                                    </dl>
                                                    <dl>&nbsp;
                                                    </dl>
                                                    <dl>
                                                        <dt>Время работы:</dt>
                                                        <dd>
                                                            08:00 - 18:00<br>(время работы по Ташкенту, UTC/GMT +5)
                                                        </dd>
                                                    </dl>
                                                </div>
                                            </div>
                                            <div class="p2-payment__block-cash-tabs-item ">
                                                <div class="p2-payment__block-text">
                                                    <p>Мы активно развиваемся и ищем партнёров для расширения присутствия Advanced Cash в других странах. Если вы хорошо знаете рынок электронной коммерции, у вас есть юридическое лицо, и вы можете предоставить профессиональные рекомендации, возможно, нам по пути. Свяжитесь с нами для получения более подробной информации: <a class="cp" href="mailto:business@advcash.com">business@advcash.com</a>.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="p2-payment__hr"></div>
                                <div class="p2-payment__block active">
                                    <div id="block-title4" class="p2-payment__block-title">Обменники
                                        <i></i>
                                    </div>
                                    <div class="p2-payment__block-contents" style="display: block;">
                                        <div class="p2-payment__block-text">
                                            <p>Пополните ваш счёт, обменяв удобную вам электронную валюту на ADV с помощью независимых обменных сервисов.</p>
                                            <p>
                                                - Задайте параметры обмена, нажмите “Поиск” и выберите подходящий обменный сервис.<br>
                                                - Нажмите по ссылке “Перейти”, после чего сайт выбранного сервиса откроется в новом окне.<br>
                                                - Совершите обмен на сайте обменного сервиса, используя данные вашего аккаунта Advanced Cash:<br>
                                            </p>
                                            <p>В случае возникновения каких-либо вопросов свяжитесь со службой поддержки используемого вами обменного сервиса.</p>
                                        </div>
                                        <div class="p2-payment__alert">
                                            <i></i>
                                            <p>Мы не несём ответственности за работу обменных сервисов. Информация в этом разделе предоставляется на основе данных независимого мониторинга.</p>
                                        </div>
                                        <table class="p2-payment__block-finder">
                                            <tbody>
                                                <tr>
                                                    <th>Платежная система</th>
                                                    <th>Валюта</th>
                                                    <th>ADVCASH Кошелек</th>
                                                    <th>Минимальный резерв</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <td class="p2-payment__block-finder-system">
                                                        <div class="p2-select">
                                                            <div class="p2-select-value fallback">Bitcoin
                                                            </div>
                                                            <select id="ePaymentSystemId" name="ePaymentSystemId" size="1" onchange="RichFaces.ajax(this,event,{&quot;sourceId&quot;:this,&quot;parameters&quot;:{&quot;javax.faces.behavior.event&quot;:&quot;change&quot;,&quot;org.richfaces.ajax.component&quot;:&quot;ePaymentSystemId&quot;} } )"> <option value="36" selected="selected">Bitcoin</option>
                                                                <option value="40">Litecoin</option>
                                                                <option value="14">Perfect Money</option>
                                                                <option value="52">Exmo</option>
                                                                <option value="34">Advanced Cash</option>
                                                                <option value="3">PrivatBank</option>
                                                                <option value="7">Sberbank</option>
                                                                <option value="18">QIWI Wallet</option>
                                                                <option value="13">Visa/MasterCard</option>
                                                                <option value="48">Payeer</option>
                                                                <option value="57">Ethereum</option>
                                                                <option value="4">Yandex.Money</option>
                                                                <option value="15">PayPal</option>
                                                                <option value="74">Tether</option>
                                                                <option value="1">WebMoney</option>
                                                                <option value="73">Zcash</option>
                                                                <option value="72">Ripple</option>
                                                                <option value="88">TRON</option>
                                                                <option value="8">Skrill</option>
                                                                <option value="10">Neteller</option>
                                                                <option value="45">TCS Bank</option>
                                                                <option value="51">Dogecoin</option>
                                                                <option value="58">Dash</option>
                                                                <option value="65">Monero</option>
                                                                <option value="23">Wire transfer</option>
                                                                <option value="67">PaySera</option>
                                                                <option value="90">Waves</option>
                                                                <option value="75">Alipay</option>
                                                                <option value="21">WU</option>
                                                                <option value="89">Monobank</option>
                                                                <option value="35">Cash</option>
                                                                <option value="93">Kaspi Bank</option>
                                                                <option value="71">Ethereum Classic</option>
                                                                <option value="12">Alfabank</option>
                                                                <option value="11">VTB</option>
                                                                <option value="77">Bitcoin Cash ABC</option>
                                                                <option value="99">TrueUSD</option>
                                                                <option value="98">USD Coin</option>
                                                                <option value="61">Binance</option>
                                                                <option value="39">ePayments</option>
                                                                <option value="87">Bitcoin Gold</option>
                                                                <option value="85">Stellar</option>
                                                                <option value="59">Paymer</option>
                                                                <option value="31">Elecsnet</option>
                                                                <option value="60">Capitalist</option>
                                                                <option value="68">Epay</option>
                                                                <option value="64">Mobile balance</option>
                                                                <option value="30">LiveCoin</option>
                                                                <option value="53">RNCB</option>
                                                                <option value="17">Post Bank</option>
                                                                <option value="2">Belarusbank</option>
                                                                <option value="43">Kazkom Bank</option>
                                                                <option value="92">Mir</option>
                                                                <option value="47">Golden Crown</option>
                                                                <option value="55">Bitcoin SV</option>
                                                                <option value="56">Peercoin</option>
                                                                <option value="105">Paxos</option>
                                                                <option value="78">NEM</option>
                                                                <option value="79">Augur</option>
                                                                <option value="80">NEO</option>
                                                                <option value="81">EOS</option>
                                                                <option value="82">IOTA</option>
                                                                <option value="83">Lisk</option>
                                                                <option value="84">Cardano</option>
                                                                <option value="86">Bytecoin</option>
                                                                <option value="6">OmiseGO</option>
                                                                <option value="91">Verge</option>
                                                                <option value="76">0xProject</option>
                                                                <option value="44">ICON</option>
                                                                <option value="54">Komodo</option>
                                                                <option value="102">BitTorrent</option>
                                                                <option value="62">BAT</option>
                                                                <option value="107">Ontology</option>
                                                                <option value="108">Qtum</option>
                                                                <option value="26">Idram</option>
                                                                <option value="25">Paxum E-Wallet</option>
                                                                <option value="24">PaySafeCard</option>
                                                                <option value="28">SolidTrust Pay</option>
                                                                <option value="49">NixMoney</option>
                                                                <option value="101">GlobalMoney</option>
                                                                <option value="104">VelesPay</option>
                                                                <option value="9">LiqPay</option>
                                                                <option value="69">E-kzt</option>
                                                                <option value="63">Cryptocurrency exchanges</option>
                                                                <option value="106">Cryptex</option>
                                                                <option value="103">Kuna Exchange</option>
                                                                <option value="19">RSBank</option>
                                                                <option value="33">Avangard</option>
                                                                <option value="16">Promsvyazbank</option>
                                                                <option value="38">Gazprombank</option>
                                                                <option value="37">Kukuruza</option>
                                                                <option value="70">RaiffeisenBank</option>
                                                                <option value="95">Openbank</option>
                                                                <option value="100">Rocketbank</option>
                                                                <option value="5">Russian Agricultural Bank</option>
                                                                <option value="22">Oschadbank</option>
                                                                <option value="97">UkrSibbank</option>
                                                                <option value="42">PUMB</option>
                                                                <option value="50">Halyk Bank</option>
                                                                <option value="27">ForteBank</option>
                                                                <option value="94">American Express</option>
                                                                <option value="41">China UnionPay</option>
                                                                <option value="20">UZCARD</option>
                                                                <option value="29">MoneyGram</option>
                                                                <option value="46">Contact</option>
                                                                <option value="32">UniStream</option>
                                                                <option value="66">Ria Money Transfer</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td class="p2-payment__block-finder-currency">
                                                        <span id="externalCurrency">
                                                            <div class="p2-select">
                                                                <div class="p2-select-value fallback">BTC
                                                                </div>
                                                                <select id="ePaymentSystemCurrency" name="ePaymentSystemCurrency" size="1">  <option value="BTC">BTC</option>
                                                                </select>
                                                            </div>
                                                        </span>
                                                    </td>
                                                    <td class="p2-payment__block-finder-wallet">
                                                        <div class="p2-select">
                                                            <div class="p2-select-value fallback">USD
                                                            </div>
                                                            <select id="advcashCurrency" name="advcashCurrency" size="1" onchange="RichFaces.ajax(this,event,{&quot;sourceId&quot;:this,&quot;parameters&quot;:{&quot;javax.faces.behavior.event&quot;:&quot;change&quot;,&quot;org.richfaces.ajax.component&quot;:&quot;advcashCurrency&quot;} } )">    <option value="USD" selected="selected">USD</option>
                                                                <option value="EUR">EUR</option>
                                                                <option value="RUB">RUB</option>
                                                                <option value="UAH">UAH</option>
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td class="p2-payment__block-finder-reserve"><input id="reserve" type="text" name="reserve" autocomplete="off" value="0.00" class="input-amount">
                                                        <span class="rf-msg dn" id="j_idt1143"></span>
                                                    </td>
                                                    <td class="p2-payment__block-finder-button"><a href="#" id="j_idt1145" name="j_idt1145" onclick="RichFaces.ajax(&quot;j_idt1145&quot;,event,{&quot;incId&quot;:&quot;1&quot;,&quot;begin&quot;:&quot;ajaxStatusLoader('deposit_exchangers_action')&quot;} );return false;" class="p2-button deposit_exchangers_action">Поиск</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <span id="exchangersListPanel"></span>
                                    </div>
                                </div>
                                <div class="p2-payment__hr"></div>
                                <div class="p2-payment__block active">
                                    <div id="block-title6" class="p2-payment__block-title">Мобильные телефоны
                                        <i></i>
                                    </div>
                                    <div class="p2-payment__block-contents" style="display: block;">
                                        <div class="p2-payment__block-phones">
											<label class="p2-payment__block-phones-item"><img src="{{ asset('assets/cab/images/phone-beeline.png') }}" style="height: 46px;" />
												<input type="radio" name="phone" checked="checked" onchange="" />
												<u></u>
											</label>
											<label class="p2-payment__block-phones-item"><img src="{{ asset('assets/cab/images/phone-mts.png') }}" style="height: 46px;" />
												<input type="radio" name="phone" onchange="" />
												<u></u>
											</label>
											<label class="p2-payment__block-phones-item"><img src="{{ asset('assets/cab/images/phone-megafon.png') }}" style="height: 56px;" />
												<input type="radio" name="phone" onchange="" />
												<u></u>
											</label>
                                        </div>
                                        <span id="mobileSection">
                                            <ul class="p2-payment__block-phones-info">
                                                <li>Срок зачисления: <span>Моментально</span></li>
                                                <li>Комиссия: <span><i><span class="green">10.99%</span></i></span></li>
                                            </ul>
                                            <div class="p2-payment__block-phones-fee">
                                                <span class="red">Данный способ пополнения счета доступен только <a class="cp" href="http://wallet.advcash.com/pages/profile/verification">верифицированным</a> пользователям
                                                </span>
                                            </div>
                                            <div class="p2-payment__block-phones-fee">
                                                <span class="red">На данный момент пополнение доступно только для российских мобильных операторов.</span></div>

                                                <div class="p2-payment__block-phones-fee  ">С каждого успешного платежа мобильный оператор взимает с абонента комиссию в размере 10 руб.</div>
                                            </span>
                                        </div>
                                    </div>
                                </span>
                            </div>
                        </span>
                        </form>
                        <div id="addNewCardPopup" style="visibility: hidden;">
                            <div  class="rf-pp-shade" id="addNewCardPopup_shade" style="z-index:100;">
                                <button class="rf-pp-btn" id="addNewCardPopupFirstHref"></button>
                            </div>
                            <div class="rf-pp-cntr " id="addNewCardPopup_container" style="position: fixed; z-index:100; ">
                                <div class="rf-pp-shdw" id="addNewCardPopup_shadow" style="opacity: 0.1;">
                                </div>
                                <div class="rf-pp-cnt-scrlr" id="addNewCardPopup_content_scroller">
                                    <div class="rf-pp-cnt" id="addNewCardPopup_content">
                                        <form id="j_idt1584" name="j_idt1584" method="post" action="/pages/deposit-funds" enctype="application/x-www-form-urlencoded">


                                            <div class="popup-reg-sms">
                                                <div class="popup-header">
                                                    <i></i><i></i><i></i>
                                                </div>
                                                <div class="popup-content">
                                                    <i></i><i></i>
                                                    <div class="popup-body">
                                                        <div class="popup-reg-sms-body">
                                                            <div class="popup-reg-sms-header">
                                                                <div class="popup-reg-sms-title">Добавить новую карту</div>
                                                                <div class="popup-reg-sms-close">
                                                                    <a class="cp" onclick="RichFaces.$('addNewCardPopup').hide();"></a>
                                                                </div>
                                                            </div>
                                                            <div class="popup-reg-sms-form popup-content">
                                                                <p>Для быстрой покупки криптовалюты нужна карта, которой вы ранее пополняли счет.<br><br><a class="cp" rel="noopener noreferrer nofollow" href="/pages/deposit-funds">Пополните счет</a> на любую сумму этой картой и пройдите несложную однократную верификацию. После этого карта станет доступна для быстрой покупки криптовалюты.
                                                                </p>
                                                            </div>
                                                            <div class="popup-reg-sms-actions">
                                                                <div class="button red h38 cancel">
                                                                    <a class="cp" onclick="RichFaces.$('addNewCardPopup').hide();">Отмена</a>
                                                                </div>
                                                                <div class="button green h38 confirm"><a href="#" onclick="jsf.util.chain(this,event,'ajaxStatus($(this))','mojarra.jsfcljs(document.getElementById(\'j_idt1584\'),{\'j_idt1588\':\'j_idt1588\'},\'\')');return false">Пополнение счета</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="popup-footer">
                                                    <i></i><i></i><i></i>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="additionalPopup" style="visibility: hidden;">
                            <div  class="rf-pp-shade" id="additionalPopup_shade" style="z-index:100;"><button class="rf-pp-btn" id="additionalPopupFirstHref"></button>
                            </div>
                            <div class="rf-pp-cntr " id="additionalPopup_container" style="position: fixed; z-index:100; ">
                                <div  class="rf-pp-shdw" id="additionalPopup_shadow" style="opacity: 0.1;">
                                </div>
                                <div class="rf-pp-cnt-scrlr" id="additionalPopup_content_scroller">
                                    <div  class="rf-pp-cnt" id="additionalPopup_content">

                                        <div class="popup-reg-sms" style="top: 15%;">
                                            <div class="popup-header">
                                                <i></i><i></i><i></i>
                                            </div>
                                            <div class="popup-content">
                                                <i></i> <i></i>
                                                <div class="popup-body">
                                                    <form id="j_idt1591" name="j_idt1591" method="post" action="/pages/deposit-funds" enctype="application/x-www-form-urlencoded">

                                                    <span id="additionalPopupPanel">
                                                        <div class="popup-reg-sms-body">
                                                        </div>
                                                    </span>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="popup-footer">
                                                <i></i><i></i><i></i>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>

            </div>
        </div>
        <div class="cb"></div>
    </div>

    <div class="footer">
        <form id="j_idt1909" name="j_idt1909" method="post" action="/index_page" enctype="application/x-www-form-urlencoded">

            <div class="footer-wrap">
                <div class="footer-copy">
                    ©

                    <script type="text/javascript">
                        document.write(new Date().getFullYear());

                    </script>&nbsp;<a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt1909'),{'j_idt1909:j_idt1911':'j_idt1909:j_idt1911'},'');return false">AdvCash</a>
                </div>
                <div class="footer-copy" style="float: right; width:100px;">
                    <a href="https://advcash.com/solutions/developers/" target="_blank">Для разработчиков</a>
                </div>
            </div>

        </form>
    </div>

    <div id="ajax-status">
        <img src="{{ asset('assets/cab/index_files/ajax_status.gif') }}">
    </div>

    <div class="dn">
    <span id="tx-cache">
        <table id="tx-cache-table">
        </table>
        <span id="tx-cache-show-more">
            <div class="p-main-transactions-loader" align="center">
                <span id="j_idt2539">
                    <span style="display:none" class="rf-st-start"><img src="{{ asset('assets/cab/index_files/loader.gif') }}" alt="ai"></span>
                    <span class="rf-st-stop"></span></span>
                </div>

                <div class="p-main-loadtransactions" id="showMoreTransactions"><a href="#" id="j_idt2543" name="j_idt2543" onclick='jsf.util.chain(this,event,"animateNewTx();","RichFaces.ajax(\"j_idt2543\",event,{\"incId\":\"1\",\"begin\":\"blockLink()\",\"status\":\"txLoading\"} )");return false;' class="collapse-all-link">Свернуть все ↑</a>
                    <a class="cursor-default collapse-all-link-fake" onclick="return false;" style="display: none;">Свернуть все
                    ↑</a>

                </div>
            </span>
        </span>
        <span id="new-tx-cache">
            <table id="new-tx-cache-table">
            </table></span>

    </div>
    <div id="confirmPanel" style="visibility: hidden;">
        <div class="rf-pp-shade" id="confirmPanel_shade" style="z-index:100;">
            <button class="rf-pp-btn" id="confirmPanelFirstHref"></button>
        </div>
        <div class="rf-pp-cntr " id="confirmPanel_container" style="position: fixed; z-index:100; ">
            <div class="rf-pp-shdw" id="confirmPanel_shadow" style="opacity: 0.1;"></div>
            <div class="rf-pp-cnt-scrlr" id="confirmPanel_content_scroller">
                <div class="rf-pp-cnt" id="confirmPanel_content">
                    <form id="j_idt3175" name="j_idt3175" method="post" action="/index_page" enctype="application/x-www-form-urlencoded">


                            <span id="confirmPanelGroup">
                                <div class="popup-header">
                                    <i></i><i></i><i></i>
                                </div>
                                <div class="popup-content">
                                    <i></i> <i></i>
                                    <div class="popup-body">
                                        <div class="popup-profile-body">
                                            <div class="fr">
                                                <a class="close-popup" onclick="RichFaces.$('confirmPanel').hide();"></a>
                                            </div>
                                            <div class="popup-profile-title">Вы уверены, что хотите отменить транзакцию?</div>
                                            <div class="popup-profile-button">
                                                <div class="button red"><input id="j_idt3177" name="j_idt3177" onclick='RichFaces.ajax("j_idt3177",event,{"incId":"1"} );return false;' value="Да" type="submit">
                                                </div>
                                                <div class="button black">
                                                    <input onclick="RichFaces.$('confirmPanel').hide();return false;" value="Нет" type="submit">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="popup-footer">
                                    <i></i><i></i><i></i>
                                </div>
                            </span>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <div id="tips">
        <div id="tip"><i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i><i class="arr"></i></div>
    </div>
    {{--<iframe data-product="web_widget" title="No content" tabindex="-1" aria-hidden="true" style="width: 0px; height: 0px; border: 0px none; position: absolute; top: -9999px;" src="index_files/a_004.html"></iframe>--}}

	@include('partials.iframe')

@endsection