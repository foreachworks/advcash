@extends('layouts.account')

@section('content')

    <span   id="j_idt44">
        <span   style="display: none;" class="rf-st-start"> </span>

        <span   class="rf-st-stop" style="">  </span>

    </span>



    @include('partials.header')

    @include('partials.topnav')

    <div  class="content-wrapper">
        <div  class="content">
            @include('partials.sidebar')

            <div  class="p-main">
                <!-- ------------------------------------ -->
                <div class="rightcol" id="rightCol">
                    <div id="p-main" class="p-profile">

                        <ul class="p-inside-transactions-speedbar">
                            <li>
                                <a href="#" id="j_idt609" name="j_idt609">Главная</a> /
                            </li>
                        </ul>
						
						
<form id="latestNewsForm" name="latestNewsForm" method="post" action="/pages/transaction" enctype="application/x-www-form-urlencoded">
<input type="hidden" name="latestNewsForm" value="latestNewsForm">


		

		
				<div class="x-block -gap-40">
					<div class="x-bg-bar">
						<div class="x-bg-bar__limited">
							<div class="x-block -gap-16">
								<div class="x-grid-row -align-justify -valign-top">
									<div class="x-grid-cell -size-auto">
										<b class="x-typo-tertiary x-color-grey x-typo-upper"></b>
									</div>
									<div class="x-grid-cell -size-auto"><a href="#" onclick="mojarra.jsfcljs(document.getElementById('latestNewsForm'),{'j_idt397':'j_idt397'},'');return false" class="x-typo-secondary x-color-green x-link-tdn">Все новости</a>
									</div>
								</div>
							</div>
							<div class="x-block -gap-8 x-typo-tertiary x-color-grey">11 марта 2021
							</div>
							<div class="x-block -gap-8">
								<h1>Важное о переводах на карты в USD, EUR, RUB</h1>
							</div>
							<div class="x-block -gap-16 x-typo-secondary x-color-grey"><p>Если вы переводите средства на банковские карты в долларах, евро и рублях, вы скорее всего заметили, что в последнее время эти возможности часто не работают. Это связано с тем, что для этих выплат мы используем несколько партнерских шлюзов, которые обслуживают не только нашу систему, но и многие другие аналогичные платформы. К сожалению, все эти шлюзы в последнее время работают с перебоями. Если мы не получаем работающий канал от наших провайдеров, мы вынуждены отключить эту возможность для пользователей.</p>
								<p>&nbsp;</p>
								<p>Мы понимаем, что для вас как для клиента крайне важно, чтобы все без исключения возможности платформы работали бесперебойно. Мы делаем все от нас зависящее, чтобы это обеспечить. Приносим извинения, если в последнее время вы столкнулись с какими-либо сложностями. Сейчас мы усиленно работаем над тем, чтобы добавить новые шлюзы для обработки этих выплат. Мы ожидаем, что вскоре ситуация стабилизируется, и эти возможности вновь будут активны в вашем аккаунте.</p>
								<p>&nbsp;</p>
								<p>При необходимости вы можете найти альтернативные варианты вывода средств в многочисленных обменных сервисах, работающих с нашей платежной системой. Это можно сделать как в вашем аккаунте в <a href="../../../pages/transfer/exchangers" target="_blank">разделе вывода</a>, так и на сайтах-мониторингах, например, <a href="https://www.bestchange.ru/" target="_blank">Bestchange.ru</a> или <a href="https://www.okchanger.ru/" target="_blank">OkChanger.ru</a>.&nbsp;</p>
								<p>&nbsp;</p>
								<p>Спасибо за понимание и за то, что используете ADV.</p>
							</div>
						</div>
						<div class="x-bg-bar__fader" style="margin-top: 8px; position: relative;">
							<button type="button" class="x-inline-button -skin-black" style="font-family: inherit; cursor: pointer;" data-text-closed="Больше" data-text-open="Меньше">
								Больше
							</button>
						</div>
					</div>
				</div><input type="hidden" name="javax.faces.ViewState" id="javax.faces.ViewState" value="-1279676345856933169:7248313254542677004" autocomplete="off">
</form>						
						
						
						
						
                        <div class="p-inside-transactions-header">
                            История операций
                        </div>



                        <script>
                            function search(event) {
                                if (event.keyCode == 13) {
                                    $('#startSearch').click();
                                    return false;
                                }
                            }
                        </script>
                        <form id="j_idt630" name="j_idt630" method="post" action="/pages/transaction" enctype="application/x-www-form-urlencoded">
                            <span id="txFilterForm">
                                <span id="advancedSearchPanel">

                                    <div class="p-history-advanced">
                                        <ul class="p-history-advanced-tab">
                                            <li>
                                                <a href="#" id="j_idt632" name="j_idt632" onclick="RichFaces.ajax(&quot;j_idt632&quot;,event,{&quot;incId&quot;:&quot;1&quot;} );return false;">Расширенный поиск</a>
                                            </li>
                                        </ul>
                                    </div>
                                </span>

					<div class="p-history-period">
						<div class="p-history-advanced-select-wrap">
							<div class="p-history-advanced-select-name">Период</div>
							<dl class="p-history-advanced-select">
								<dt>
									<a onclick="History.Advanced.Select(this)" class="cp">Месяц</a>
								</dt>
								<dd style="display: none; width: 308px; z-index: 10000;">
									<ul>
										<li><a href="#" id="j_idt370" name="j_idt370" onclick="#">Последняя неделя
												<span>24 — 31 мая 2020</span></a></li>
										<li><a href="#" id="j_idt373" name="j_idt373" onclick="#">Две недели
												<span>17 — 31 мая 2020</span></a></li>
										<li><a href="#" id="j_idt376" name="j_idt376" onclick="#">Месяц
												<span>30 апреля — 31 мая 2020</span></a></li>
										<li><a href="#" id="j_idt379" name="j_idt379" onclick="#">Три месяца
												<span>29 февраля — 31 мая 2020</span></a></li>
										<li><a href="#" id="j_idt382" name="j_idt382" onclick="#">Конкретный период</a></li>
									</ul>
								</dd>
							</dl>
						</div>
					</div><span id="daterangePanel"></span>
                                <dl class="p-history-search">
                                    <dt>Поиск</dt>
                                    <dd>
                                        <div class="select-input">
                                            <dl>
                                                <dt>
                                                    <a id="searchStringType" onclick="SelInp.Toggle(this)" class="cp">по ключевому слову</a>
                                                </dt>
                                                <dd style="display: none; width: 183px;">
                                                    <ul>
                                                        <li>
                                                            <a href="#" id="j_idt752" name="j_idt752" onclick="jsf.util.chain(this,event,&quot;SelInp.Set('\u043F\u043E \u043A\u043B\u044E\u0447\u0435\u0432\u043E\u043C\u0443 \u0441\u043B\u043E\u0432\u0443', 1)&quot;,&quot;RichFaces.ajax(\&quot;j_idt752\&quot;,event,{\&quot;incId\&quot;:\&quot;1\&quot;} )&quot;);return false;">по ключевому слову</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" id="j_idt756" name="j_idt756" onclick="jsf.util.chain(this,event,&quot;SelInp.Set('\u043F\u043E \u043A\u043E\u0440\u0440\u0435\u0441\u043F\u043E\u043D\u0434\u0435\u043D\u0442\u0443', 1)&quot;,&quot;RichFaces.ajax(\&quot;j_idt756\&quot;,event,{\&quot;incId\&quot;:\&quot;1\&quot;} )&quot;);return false;">по корреспонденту</a>
                                                        </li>
                                                        <li>
                                                            <a href="#" id="j_idt759" name="j_idt759" onclick="jsf.util.chain(this,event,&quot;SelInp.Set('\u043F\u043E \u043D\u043E\u043C\u0435\u0440\u0443 \u0442\u0440\u0430\u043D\u0437\u0430\u043A\u0446\u0438\u0438', 4)&quot;,&quot;RichFaces.ajax(\&quot;j_idt759\&quot;,event,{\&quot;incId\&quot;:\&quot;1\&quot;} )&quot;);return false;">по номеру транзакции</a>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                            <input id="searchInput" type="text" name="searchInput" onkeypress="return search(event);">
                                        </div>
                                        <div class="button green">
                                            <input id="startSearch" name="startSearch" onclick="jsf.util.chain(this,event,&quot;startSearch();&quot;,&quot;RichFaces.ajax(\&quot;startSearch\&quot;,event,{\&quot;incId\&quot;:\&quot;1\&quot;} )&quot;);return false;" value="Найти" type="submit">
                                        </div>
                                    </dd>
                                </dl>
                                <span id="txExport">
                                    <ul class="history-export">
                                        <li class="print">
                                            <a onclick="return printPage();" class="cp"> Распечатать </a>
                                        </li>
                                        <li class="csv">
                                            <a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt630'),{'j_idt770':'j_idt770'},'');return false">Сохранить Excel</a>
                                        </li>
                                        <li class="csv">
                                            <a href="#" onclick="mojarra.jsfcljs(document.getElementById('j_idt630'),{'j_idt775':'j_idt775'},'');return false">Сохранить CSV</a>
                                        </li>
                                    </ul>
                                </span>
                            </span>
                        </form>
                        <form id="txForm" name="txForm" method="post" action="/pages/transaction" enctype="application/x-www-form-urlencoded">
                            <span id="txList">

                                <div class="p-main-transactions tab">
                                    <span id="tx-container">
                                        <table id="txListDataTable">
                                            <tbody id="load_more">
                                                <tr>
                                                    <th class="date">Дата</th>
                                                    <th class="type">Тип операции</th>
                                                    <th class="corr">Корреспондент</th>
                                                    <th class="sum">Сумма</th>
                                                    <th class="status">Статус</th>
                                                </tr>
												<? $i=1 ?>
                                                @foreach($transactions as $transaction)
                                                    <tr id="tr_<?=$i;?>" style="display:none;">
                                                    <td class="date" style="background: rgb(255, 255, 255);">{{Carbon\Carbon::parse($transaction->date_done)->isoFormat('D MMM, H:mm')}}</td>
                                                    <td class="type" style="background: rgb(255, 255, 255);">
                                                        <div>
                                                            @if($transaction->input == 0)
                                                                <a onclick="Read(this, '49150cb5-24fb-4be5-8e8b-b250ddb4913e');Popup(this);HighLight('wallet-U758724115185', 'red');">
                                                                    {{$transaction->type_operation == 0 ? 'Внутренняя транзакция' : 'x'}}
                                                                </a>
                                                            @else
                                                                <a onclick="Read(this, '49150cb5-24fb-4be5-8e8b-b250ddb4913e');Popup(this);HighLight('wallet-U758724115185', 'green');">
                                                                {{$transaction->type_operation == 0 ? 'Внутренняя транзакция' : 'x'}}
                                                            </a>
                                                            @endif




                                                            @if($transaction->input == 0)

																<div class="popup">
																	<i class="popup-arrow"></i>

																	<div class="popup-header">
																		<i></i><i></i><i></i>
																	</div>
																	<div class="popup-content">
																		<i></i> <i></i>

																			<div class="popup-body">
																			<div class="popup-transaction-header">
																				<a href="javascript:void(0)" onclick="ClosePopup()">
																					<span >{{$transaction->type_operation == 0 ? 'Внутренняя транзакция' : 'x'}}
																					</span>
																				</a>
																				<a href="javascript:void(0)" onclick="ClosePopup()"></a>
																			</div>

																			<ul class="popup-transaction-fields">

																				<li class="id">
																					<span>Transaction ID:</span>
																					<dfn>
																						<b>{{$transaction->transaction_id}}</b>
																					</dfn>
																				</li>
																				<li class="from">
																					<span>Кому:</span>
																					<dfn>
																						<b>{{$transaction->from_email}}
																						</b>
																					</dfn>
																				</li>
																				<li class="transfer">
																					<span>Снято:</span>
																					<dfn>
																						<b class="{{$transaction->input == 0 ? 'red' : 'green'}}">
																						   {{$transaction->input == 0 ? '-' : ''}}{{number_format($transaction->amount, 2, '.', '')}}
																						</b>
																						USD
																					</dfn>
																				</li>
																				<li class="comission">
																					<span>Комиссия:</span>
																					<dfn>
																						<i class="green">Без комиссии</i>
																					</dfn>
																				</li>
																				<li class="enrolled">
																					<span>Отправлено:</span>
																					<dfn>
																					<b>
																						{{number_format($transaction->amount, 2, '.', '')}}
																					</b>	
																						USD
																					</dfn>
																				</li>
																				<br><br>
																				<li class="id">
																					<span>Примечание:</span>
																					<dfn>
																						{{$transaction->comment}}
																					</dfn>
																				</li>

																			</ul>
																		</div>
																	</div>
																	<div class="popup-footer">
																		<i></i><i></i><i></i>
																	</div>
																</div>
															
                                                            @else

																<div class="popup">
																	<i class="popup-arrow"></i>

																	<div class="popup-header">
																		<i></i><i></i><i></i>
																	</div>
																	<div class="popup-content">
																		<i></i> <i></i>

																			<div class="popup-body">
																			<div class="popup-transaction-header">
																				<a href="javascript:void(0)" onclick="ClosePopup()">
																					<span >{{$transaction->type_operation == 0 ? 'Внутренняя транзакция' : 'x'}}
																					</span>
																				</a>
																				<a href="javascript:void(0)" onclick="ClosePopup()"></a>
																			</div>

																			<ul class="popup-transaction-fields">

																				<li class="id">
																					<span>Transaction ID:</span>
																					<dfn>
																						<b>{{$transaction->transaction_id}}</b>
																					</dfn>
																				</li>
																				<li class="from">
																					<span>От:</span>
																					<dfn>
																						<b>{{$transaction->from_email}}
																						</b>
																					</dfn>
																				</li>
																				<li class="enrolled">
																					<span style="color: #00AF7E">Сумма:</span>
																					<dfn style="color: #00AF7E">
																					<b>
																						{{number_format($transaction->amount, 2, '.', '')}}
																					</b>
																					</dfn>
																						USD
																				</li>
																				<br><br>
																				<li class="id">
																					<span>Примечание:</span>
																					<dfn>
																						{{$transaction->comment}}
																					</dfn>
																				</li>

																			</ul>
																		</div>
																	</div>
																	<div class="popup-footer">
																		<i></i><i></i><i></i>
																	</div>
																</div>






                                                            @endif
															
															
															
															
															
                                                        </div>
                                                    </td>
                                                    <td class="corr" style="background: rgb(255, 255, 255);">{{$transaction->number_purse}}</td>
                                                    <td class="sum" style="background: rgb(255, 255, 255);">
                                                        <b class="{{$transaction->input == 0 ? 'red' : 'green'}}"> {{$transaction->input == 0 ? '-' : ''}}{{number_format($transaction->amount, 2, '.', '')}}
                                                        </b>USD
                                                    </td>
                                                    <td class="status" style="background: rgb(255, 255, 255);">

                                                        <a class="titled" data-title="Выполнена">
                                                            <img src="{{ asset('assets/cab/images/status-ok.png') }}">

                                                        </a>
                                                    </td>
                                                </tr>
												<? $i++ ?>
                                                @endforeach

                                            </tbody>
                                        </table>
                                    </span>
                                    <span id="tx-show-more">

                                        <div class="p-main-transactions-loader" align="center">
                                            <span id="j_idt1545">
                                                <span style="display:none" class="rf-st-start"><img src="{{ asset('assets/cab/img/loader.gif') }}" alt="ai">
                                                </span>
                                                <span class="rf-st-stop">
                                                </span>
                                            </span>
                                        </div>

                                        <div class="p-main-loadtransactions" id="showMoreTransactions">

                                        </div>
                                    </span>
                                    <span id="tx-history-empty">
                                    </span>

                                </div>
                            </span>

                            <div class="p-history-searching" style="display: none;">Идет поиск...</div>

                        </form>

                    </div>
                </div>
                <!-- ------------------------------------ -->
            </div>

            <div  class="cb">
            </div>

        </div>


        <div  class="footer">
            <form id="j_idt1909" name="j_idt1909" method="post" action="/index_page" enctype="application/x-www-form-urlencoded">
                <div  class="footer-wrap">
                    <div  class="footer-copy">
                        ©
                        <script type="text/javascript">
                            document.write(new Date().getFullYear());
                        </script>&nbsp;
                        <a  href="#"  >AdvCash</a>
                    </div>

                    <div  class="footer-copy" style="float: right; width:100px;">
                        <a  href="https://advcash.com/solutions/developers/" target="_blank">Для разработчиков </a>
                    </div>
                </div>
            </form>
        </div>


        <div  id="ajax-status">
            <img src="{{ asset('assets/cab/index_files/ajax_status.gif') }}">

        </div>

        <div  class="dn">
            <span id="tx-cache">
                <table id="tx-cache-table">
                </table>
                <span   id="tx-cache-show-more">
                    <div  class="p-main-transactions-loader" align="center">
                        <span   id="j_idt2539">
                            <span   style="display:none" class="rf-st-start"><img src="{{ asset('assets/cab/index_files/loader.gif') }}" alt="ai"> </span>
                            <span   class="rf-st-stop">  </span>
                        </span>
                    </div>
                    <div  class="p-main-loadtransactions" id="showMoreTransactions">
                        {{--<a  href="#" id="j_idt2543" name="j_idt2543" onclick='jsf.util.chain(this,event,"animateNewTx();","RichFaces.ajax(\"j_idt2543\",event,{\"incId\":\"1\",\"begin\":\"blockLink()\",\"status\":\"txLoading\"} )");return false;' class="collapse-all-link">Свернуть все ↑--}}
                        {{--</a>--}}
                        @if(count($transactions) >= 10)
							@if($count)
	<!--							<a  class="cursor-default collapse-all-link-fake" href="{{'https://' . $_SERVER['HTTP_HOST'] . "?q=$count"  }}">Показать еще ↓</a>  -->
									<a  class="cursor-default collapse-all-link-fake" id="show_m" onclick="ShowMore(<?=$trans;?>)">Показать еще ↓</a>
									<input type="hidden" id="start" value="11">
									<a  class="cursor-default collapse-all-link-fake" id="hide_all" onclick="$('#hide_all').hide();" style="display:none;" href="#">Свернуть все ↑</a>
							@else
								<a  class="cursor-default collapse-all-link-fake" href="#">Свернуть все ↑</a>
							@endif
                        @endif

                    </div>
                </span>
            </span>
            <span   id="new-tx-cache">
                <table id="new-tx-cache-table">
                </table>
            </span>
        </div>

        <div  id="confirmPanel" style="visibility: hidden;">

            <div  class="rf-pp-shade" id="confirmPanel_shade" style="z-index:100;">
                <button class="rf-pp-btn" id="confirmPanelFirstHref"></button>

            </div>

            <div  class="rf-pp-cntr " id="confirmPanel_container" style="position: fixed; z-index:100; ">

                <div  class="rf-pp-shdw" id="confirmPanel_shadow" style="opacity: 0.1;">
                </div>

                <div  class="rf-pp-cnt-scrlr" id="confirmPanel_content_scroller">

                    <div  class="rf-pp-cnt" id="confirmPanel_content">
                        <form id="j_idt3175" name="j_idt3175" method="post" action="/index_page" enctype="application/x-www-form-urlencoded">



                            <span   id="confirmPanelGroup">

                                <div  class="popup-header">
                                    <i></i><i></i><i></i>

                                </div>

                                <div  class="popup-content">
                                    <i></i> <i></i>

                                    <div  class="popup-body">

                                        <div  class="popup-profile-body">

                                            <div  class="fr">

                                                <a  class="close-popup" onclick="RichFaces.$('confirmPanel').hide();">
                                                </a>

                                            </div>

                                            <div  class="popup-profile-title">Вы уверены, что хотите отменить транзакцию?
                                            </div>

                                            <div  class="popup-profile-button">

                                                <div  class="button red">
                                                    <input id="j_idt3177" name="j_idt3177" onclick='RichFaces.ajax("j_idt3177",event,{"incId":"1"} );return false;' value="Да" type="submit">

                                                </div>

                                                <div  class="button black">
                                                    <input onclick="RichFaces.$('confirmPanel').hide();return false;" value="Нет" type="submit">

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                                <div  class="popup-footer">
                                    <i></i><i></i><i></i>

                                </div>

                            </span>

                        </form>

                    </div>

                </div>

            </div>



        </div>


        <div  id="tips">

            <div  id="tip"><i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i><i class="arr"></i>
            </div>

        </div>
        {{--<iframe data-product="web_widget" title="No content" tabindex="-1" aria-hidden="true" style="width: 0px; height: 0px; border: 0px none; position: absolute; top: -9999px;" src="index_files/a_004.html"></iframe>--}}

	@include('partials.iframe')

<script>

$( document ).ready(function(){

	for (var i = 1; i < 11; i++) {

		$('#tr_'+i).show();

	}

  tippy('[data-tippy-template]', {
    appendTo: function () {
      return document.body;
    },
    content(reference) {
      var id = reference.getAttribute('data-tippy-template');
      var template = document.getElementById(id);

      return template.innerHTML;
    },
    theme: 'interactive',
    trigger: 'click',
    allowHTML: true,
    maxWidth: 404,
    // showOnCreate: true,
  });

  tippy('[data-tippy-content]', {
    appendTo: function () {
      return document.body;
    },
    onShow: function (instance) {
      instance.reference.classList.add('-active');
    },
    onHide: function (instance) {
      instance.reference.classList.remove('-active');
    },
    theme: 'default',
    trigger: 'click',
    maxWidth: 224,
    // showOnCreate: true,
  });

  $('.x-card-ref__toggler').on('click', function () {
    $('.x-card-ref').toggleClass('-is-open');
  });

  $('.x-card-offer__toggler').on('click', function () {
    var block = $(this).parents('.x-card-offer');
    var isOpen = block.hasClass('-is-open');

    if (isOpen) {
      block.removeClass('-is-open').removeAttr('style');
    } else {
      var height = $('.x-card-offer__long', block).height();

      block
          .toggleClass('-is-open')
          .attr('style', 'max-height: ' + height + 'px;');
    }
  });

  $('.x-big-tabs__tab[data-for]').on('click', function (e) {
    var button = e.currentTarget;
    var tabId = button.dataset.for;
    var tab = document.getElementById(tabId);
    var selectors = [];

    if (button) selectors.push(button);
    if (tab) selectors.push(tab);

    $(selectors)
        .addClass('-active')
        .siblings('.-active')
        .removeClass('-active');
  });

  $('.x-header__profile-handle').on('click', function () {
    $(this).next().toggleClass('-open');
  });

  $(window).on('click', function (e) {
    if (
        $(e.target).closest('.x-header__profile-handle').length ||
        $(e.target).closest('.x-header__profile-dropdown').length
    ) {
      return;
    }

    $('.x-header__profile-dropdown').removeClass('-open');
  });

  $('.x-bg-bar__fader button').on('click', function () {
    var toggler = $(this);
    var block = toggler.parents('.x-bg-bar');
    var fader = block.find('.x-bg-bar__fader');
    var container = block.find('.x-bg-bar__limited');

    if (container.attr('style')) {
      container.removeAttr('style');
      toggler.html(toggler.data().textClosed);
    } else {
      container.css('max-height', $('.x-bg-bar__limited')[0].scrollHeight);
      toggler.html(toggler.data().textOpen);
    }

    fader.toggleClass('-is-hidden');
  });

  $('.cookie-policy-banner button').on('click', function () {
    document.cookie =
        'adv_cookie_policy=1; path=/; domain=.advcash.com; max-age=' +
        60 * 60 * 24 * 365 * 10;
    $('.cookie-policy-banner').removeClass('-active');
  });


});


function ShowMore(count)
{
	var start = $('#start').val();
	
	for (var i = start; i < Number(start)+10; i++) {

		$('#tr_'+i).show();

	}
	
	$('#start').val(i);
	
	if(i > count)
	{
		$('#show_m').hide();
		$('#hide_all').show();
	}
}

</script>

@endsection