@extends('layouts.account')

@section('content')

    <span id="j_idt44">
        <span style="display: none;" class="rf-st-start"> </span>
        <span class="rf-st-stop" style=""> </span>
    </span>



    @include('partials.header')

    @include('partials.topnav')

    <div class="content-wrapper">

        <div class="content">

            @include('partials.sidebar')


            <div class="p-main">

                <!-- ------------------------------------ -->
                <div class="rightcol">
                    <div class="p-transfer">


                        <div class="global-error-container">

                            <div class="global-error">
                                <p>
                                    <b>
                                    <span class="rf-msgs " id="j_idt576">
                                    </span>
                                    </b>
                                </p>

                            </div>
                            <i class="arr"></i>

                        </div>
                        <script type="text/javascript" src="{{ asset('assets/cab/index_files/jquery.tooltip.js') }}"></script>
                        <script type="text/javascript" src="{{ asset('assets/cab/index_files/tooltip.js') }}"></script>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                setTimeout(initializeTooltipsForWithdrawalSection, 500);
                            });
                            $(document).bind('click', function (e) {
                                initializeTooltipsForWithdrawalSection();
                            });
                        </script>
                        <form id="j_idt580" name="j_idt580" method="post" action="/pages/transfer/wallet"
                              enctype="application/x-www-form-urlencoded">

                            <table class="p-transfer-nav">
                                <tbody>
                                <tr>
                                    <td class="active"><a href="#"
                                                          onclick="mojarra.jsfcljs(document.getElementById('j_idt580'),{'j_idt580:j_idt582':'j_idt580:j_idt582'},'');return false">На
                                            кошелек<br>ADVcash</a>
                                    </td>
                                    <td><a href="#"
                                           onclick="mojarra.jsfcljs(document.getElementById('j_idt580'),{'j_idt580:j_idt597':'j_idt580:j_idt597'},'');return false">На
                                            банковскую<br>карту</a>
                                    </td>
                                    <td><a href="#"
                                           onclick="mojarra.jsfcljs(document.getElementById('j_idt580'),{'j_idt580:j_idt608':'j_idt580:j_idt608'},'');return false">Банковским<br>переводом</a>
                                    </td>
                                    <td><a href="#"
                                           onclick="mojarra.jsfcljs(document.getElementById('j_idt580'),{'j_idt580:j_idt616':'j_idt580:j_idt616'},'');return false">В
                                            эл.<br>валюту</a>
                                    </td>
                                    <td><a href="#"
                                           onclick="mojarra.jsfcljs(document.getElementById('j_idt580'),{'j_idt580:j_idt619':'j_idt580:j_idt619'},'');return false">На
                                            e-mail</a>
                                    </td>
                                    <td><a href="#"
                                           onclick="mojarra.jsfcljs(document.getElementById('j_idt580'),{'j_idt580:j_idt624':'j_idt580:j_idt624'},'');return false">Наличные</a>
                                    </td>
                                    <td><a href="#"
                                           onclick="mojarra.jsfcljs(document.getElementById('j_idt580'),{'j_idt580:j_idt629':'j_idt580:j_idt629'},'');return false">Обменники</a>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </form>


                        <form id="mainForm" name="mainForm" method="post" action="/pages/transfer/wallet"
                              enctype="application/x-www-form-urlencoded">

                        <span id="panel">

                            <div class="p-transfer-tip">
                                <p class="processing">
                                    Срок исполнения: <b>Моментально</b>
                                </p>
                                <p class="fee">
                                    Комиссия: <b>
                                        <span id="commission">Без комиссии
                                        </span>
                                    </b>
                                </p>
                                <p class="desc">Переводы другим клиентам ADVcash. <br>Переводы на свои кошельки. Обмен валюты.
                                </p>

                            </div>
                            <span id="highRiskNote">
                            </span>



                            <div class="p-transfer-form">
                                <fieldset>
                                    <dl class="f-item">
                                        <dt>Выберите кошелек</dt>

                                        <dd>

                                            <div class="combobox-account">

                                                <div class="combobox-account-header" id="wallet-1"
                                                     onclick="Popup(this)">

                                                    <span onclick="Popup(this)">Доллар

                                                    </span>

                                                    <del onclick="Popup(this)">USD</del>
                                                    <b onclick="Popup(this)">{{number_format(auth()->user()->balance, 2, '.', '')}}
                                                    </b>

                                                </div>

                                                <div class="popup combobox-account-list" style="display: none;">

                                                    <div class="popup-header">
                                                        <i></i><i></i><i></i>

                                                    </div>

                                                    <div class="popup-content">
                                                        <i></i><i></i>

                                                        <div class="popup-body">
                                                            <ul class="rf-ulst">
                                                                <li class="rf-ulst-itm"><a href="#"
                                                                                           id="j_idt648:0:j_idt649"
                                                                                           name="j_idt648:0:j_idt649"
                                                                                           onclick="jsf.util.chain(this,event,&quot;SetMyWallet(this, ['.l b', '.l span', '.r b', '.r span'\u005D, '#wallet\u002D1')&quot;,&quot;RichFaces.ajax(\&quot;j_idt648:0:j_idt649\&quot;,event,{\&quot;incId\&quot;:\&quot;1\&quot;} )&quot;);return false;">
                                                                    <del class="l">
                                                                        <b>Доллар </b> <span>{{auth()->user()->number_purse}}

                                                                        </span>

                                                                    </del>
                                                                    <del class="r">
                                                                        <b>{{number_format(auth()->user()->balance, 2, '.', '')}}
                                                                        </b> <span>USD
                                                                        </span>

                                                                    </del></a>
                                                                </li>

                                                                <li class="rf-ulst-itm"><a href="#"
                                                                                           id="j_idt648:1:j_idt649"
                                                                                           name="j_idt648:1:j_idt649"
                                                                                           onclick="jsf.util.chain(this,event,&quot;SetMyWallet(this, ['.l b', '.l span', '.r b', '.r span'\u005D, '#wallet\u002D1')&quot;,&quot;RichFaces.ajax(\&quot;j_idt648:1:j_idt649\&quot;,event,{\&quot;incId\&quot;:\&quot;1\&quot;} )&quot;);return false;">
                                                                    <del class="l">
                                                                        <b>Евро </b> <span>E 0467 3855 8220

                                                                        </span>

                                                                    </del>
                                                                    <del class="r">
                                                                        <b>{{number_format(auth()->user()->balance_e, 2, '.', '')}}
                                                                        </b> <span>EUR
                                                                        </span>

                                                                    </del></a>
                                                                </li>

                                                                <li class="rf-ulst-itm"><a href="#"
                                                                                           id="j_idt648:2:j_idt649"
                                                                                           name="j_idt648:2:j_idt649"
                                                                                           onclick="jsf.util.chain(this,event,&quot;SetMyWallet(this, ['.l b', '.l span', '.r b', '.r span'\u005D, '#wallet\u002D1')&quot;,&quot;RichFaces.ajax(\&quot;j_idt648:2:j_idt649\&quot;,event,{\&quot;incId\&quot;:\&quot;1\&quot;} )&quot;);return false;">
                                                                    <del class="l">
                                                                        <b>Рубль </b> <span>R 9388 6607 5020

                                                                        </span>

                                                                    </del>
                                                                    <del class="r">
                                                                        <b>{{number_format(auth()->user()->balance_r, 2, '.', '')}}
                                                                        </b> <span>RUR
                                                                        </span>

                                                                    </del></a>
                                                                </li>

                                                            </ul>


                                                        </div>

                                                    </div>

                                                    <div class="popup-footer">
                                                        <i></i><i></i><i></i>

                                                    </div>

                                                </div>

                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="f-item">
                                        <dt>Сумма отправления</dt>

                                        <dd>

                                            <div class="input-currency">
                                                <input id="srcAmount" type="text" name="srcAmount" value="0.00"
                                                       class="input-amount" maxlength="10"
                                                       onchange="RichFaces.ajax(this,event,{&quot;sourceId&quot;:this,&quot;parameters&quot;:{&quot;javax.faces.behavior.event&quot;:&quot;change&quot;,&quot;org.richfaces.ajax.component&quot;:&quot;srcAmount&quot;} } )">
                                                <span id="srcCurrency">USD
                                                </span>

                                                <span class="rf-msg dn" id="j_idt659">
                                                </span>


                                            </div>
                                            <span id="amountPlusCommission">

                                                <div class="p-transfer-comission">

                                                </div>
                                            </span>

                                        </dd>
                                    </dl>
                                </fieldset>
                                <fieldset>

                                    <div class="p-transfer-form-tip" style="color: #6C7583;">
                                        Если у получателя ещё нет аккаунта, вы можете сделать
                                        <a href="#"
                                           onclick="mojarra.jsfcljs(document.getElementById('mainForm'),{'j_idt668':'j_idt668'},'');return false">перевод на email</a>.

                                    </div>
                                    <dl class="f-item">
                                        <dt>Укажите кошелек или e-mail получателя</dt>

                                        <dd>

                                            <div class="combobox-number">

                                                <div class="combobox-number-header">
                                                    <span id="destLogin">

                                                        <div class="combobox-number-header-placeholder block-personal-user"
                                                             style="display: block; overflow: visible;">
                                                            <b> <i></i> <a></a>
                                                            </b>

                                                        </div>
                                                    </span>
                                                    <input id="destWalletId" type="text" name="destWalletId"
                                                           autocomplete="off" class="" maxlength="50"
                                                           onfocus="Combo('.combobox-number-big', $(this))"
                                                           onkeyup="accountNumberAutocomplete(this)"
                                                           onblur="RichFaces.ajax(this,event,{&quot;sourceId&quot;:this,&quot;parameters&quot;:{&quot;javax.faces.behavior.event&quot;:&quot;blur&quot;,&quot;org.richfaces.ajax.component&quot;:&quot;destWalletId&quot;} } )">
                                                    <span class="rf-msg dn" id="j_idt674">
                                                    </span>



                                                </div>

                                                <div class="popup combobox-number-big select-recipient"
                                                     style="display: none;">

                                                    <div class="popup-header">
                                                        <i></i><i></i><i></i>

                                                    </div>

                                                    <div class="popup-content">
                                                        <i></i><i></i>

                                                        <div class="popup-body">
                                                            <ul class="rf-ulst" id="accountNumberAutocomplete">
                                                                <li style="display:none">
                                                                </li>

                                                            </ul>


                                                        </div>

                                                    </div>

                                                    <div class="popup-footer">
                                                        <i></i><i></i><i></i>

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="selector-wallet">
                                                <a class="cp"></a>

                                                <div class="popup combobox-account-list select-wallet"
                                                     style="display: none;">

                                                    <div class="popup-header">
                                                        <i></i><i></i><i></i>

                                                    </div>

                                                    <div class="popup-content">
                                                        <i></i><i></i>

                                                        <div class="popup-body">
                                                            <ul class="rf-ulst" id="secondWalletsForCurrencyExchange">
                                                                <li id="secondWalletsForCurrencyExchange:0"
                                                                    class="rf-ulst-itm"><a href="#"
                                                                                           id="secondWalletsForCurrencyExchange:0:j_idt681"
                                                                                           name="secondWalletsForCurrencyExchange:0:j_idt681"
                                                                                           onclick="RichFaces.ajax(&quot;secondWalletsForCurrencyExchange:0:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;} );return false;">
                                                                    <del class="l">
                                                                        <b>Евро </b> <span>E 0467 3855 8220

                                                                        </span>

                                                                    </del>
                                                                    <del class="r">
                                                                        <b>0.00
                                                                        </b> <span>EUR
                                                                        </span>

                                                                    </del></a>

                                                                </li>


                                                                <li id="secondWalletsForCurrencyExchange:1"
                                                                    class="rf-ulst-itm"><a href="#"
                                                                                           id="secondWalletsForCurrencyExchange:1:j_idt681"
                                                                                           name="secondWalletsForCurrencyExchange:1:j_idt681"
                                                                                           onclick="RichFaces.ajax(&quot;secondWalletsForCurrencyExchange:1:j_idt681&quot;,event,{&quot;incId&quot;:&quot;1&quot;} );return false;">
                                                                    <del class="l">
                                                                        <b>Рубль </b> <span>R 9388 6607 5020

                                                                        </span>

                                                                    </del>
                                                                    <del class="r">
                                                                        <b>0.00
                                                                        </b> <span>RUR
                                                                        </span>

                                                                    </del></a>
                                                                </li>

                                                            </ul>


                                                        </div>

                                                    </div>

                                                    <div class="popup-footer">
                                                        <i></i><i></i><i></i>

                                                    </div>

                                                </div>

                                            </div>

                                            <div class="selector-recent-disabled">
                                                <a class="cp"></a>

                                                <div class="popup combobox-number-small select-recent"
                                                     style="display: none;">
                                                    <i class="popup-arrow"></i>

                                                    <div class="popup-header">
                                                        <i></i><i></i><i></i>

                                                    </div>

                                                    <div class="popup-content">
                                                        <i></i><i></i>

                                                        <div class="popup-body">
                                                            <ul class="rf-ulst" id="paymentTemplateList">
                                                                <li style="display:none">
                                                                </li>

                                                            </ul>


                                                        </div>

                                                    </div>

                                                    <div class="popup-footer">
                                                        <i></i><i></i><i></i>

                                                    </div>

                                                </div>

                                            </div>
                                        </dd>
                                    </dl>
                                    <dl class="f-item">
                                        <dt>Сумма к зачислению</dt>

                                        <dd>

                                            <div class="input-currency">
                                                <input id="destAmount" type="text" name="destAmount" value="0.00"
                                                       class="input-amount" maxlength="10"
                                                       onchange="RichFaces.ajax(this,event,{&quot;sourceId&quot;:this,&quot;parameters&quot;:{&quot;javax.faces.behavior.event&quot;:&quot;change&quot;,&quot;org.richfaces.ajax.component&quot;:&quot;destAmount&quot;} } )">
                                                <span id="destCurrency">USD
                                                </span>

                                                <span class="rf-msg dn" id="j_idt696">
                                                </span>


                                            </div>
                                            <span id="protectionCodeLabel">
                                                <label class="p-transfer-protection-label">
                                                    <input id="useProtection" type="checkbox" name="useProtection">
                                                    <span>Код протекции </span>

                                                </label>
                                            </span>

                                        </dd>
                                    </dl>
                                    <span id="protectionCode">

                                        <div class="p-transfer-protection">
                                            <dl class="f-item">
                                                <dt>Код протекции</dt>

                                                <dd>

                                                    <div class="input-currency default">
                                                        <input id="protectionCodeComponentId" type="text"
                                                               name="protectionCodeComponentId" maxlength="10">
                                                        <span class="rf-msg dn" id="j_idt704">
                                                        </span>


                                                    </div>
                                                </dd>
                                            </dl>
                                            <dl class="f-item">
                                                <dt>Срок действия</dt>

                                                <dd>

                                                    <div class="input-currency days">
                                                        <input id="expirationDaysComponentId" type="text"
                                                               name="expirationDaysComponentId" value="1" maxlength="3">
                                                        <span>дней
                                                        </span>

                                                        <span class="rf-msg dn" id="j_idt707">
                                                        </span>


                                                    </div>
                                                </dd>
                                            </dl>

                                        </div>
                                    </span>

                                </fieldset>
                                <dl class="p-transfer-note f-item">
                                    <dt>Примечание</dt>

                                    <dd>
                                        <textarea id="note" name="note" placeholder="Не обязательно"></textarea>
                                        <span class="rf-msg dn" id="j_idt710">
                                        </span>

                                    </dd>
                                </dl>

                                <div class="p-transfer-submit">
                                    <label class="save-data">
                                        <span id="saveData">
                                            <input type="checkbox" name="j_idt713">
                                            <span>Сохранить реквизиты получателя
                                            </span>

                                        </span>

                                    </label>

                                    <div class="button h38 green">
                                        <input id="j_idt716" name="j_idt716"
                                               onclick="RichFaces.ajax(&quot;j_idt716&quot;,event,{&quot;incId&quot;:&quot;1&quot;} );return false;"
                                               value="Продолжить" type="submit">

                                    </div>

                                </div>

                            </div>
                        </span>
                        </form>
                        <div id="emailPinPopup" style="visibility: hidden;">
                            <div class="rf-pp-shade" id="emailPinPopup_shade" style="z-index:100;">
                                <button class="rf-pp-btn" id="emailPinPopupFirstHref"></button>
                            </div>
                            <div class="rf-pp-cntr " id="emailPinPopup_container"
                                 style="position: fixed; z-index:100; ">
                                <div class="rf-pp-shdw" id="emailPinPopup_shadow" style="opacity: 0.1;">
                                </div>
                                <div class="rf-pp-cnt-scrlr" id="emailPinPopup_content_scroller">
                                    <div class="rf-pp-cnt" id="emailPinPopup_content">

                                    </div>
                                </div>
                            </div>
                            <!-- <script type="text/javascript">new RichFaces.ui.PopupPanel("emailPinPopup",{"autosized":true} );</script> -->
                        </div>
                        <div id="emailLinkPopup" style="visibility: hidden;">
                            <div class="rf-pp-shade" id="emailLinkPopup_shade" style="z-index:100;">
                                <button class="rf-pp-btn" id="emailLinkPopupFirstHref"></button>
                            </div>
                            <div class="rf-pp-cntr " id="emailLinkPopup_container"
                                 style="position: fixed; z-index:100; ">
                                <div class="rf-pp-shdw" id="emailLinkPopup_shadow" style="opacity: 0.1;">
                                </div>
                                <div class="rf-pp-cnt-scrlr" id="emailLinkPopup_content_scroller">
                                    <div class="rf-pp-cnt" id="emailLinkPopup_content">
                                        <form id="j_idt804" name="j_idt804" method="post"
                                              action="/pages/transfer/wallet"
                                              enctype="application/x-www-form-urlencoded">

                                        <span id="emailLinkPanel">

                                            <div class="popup-reg-sms">

                                                <div class="popup-header">
                                                    <i></i><i></i><i></i>

                                                </div>

                                                <div class="popup-content">
                                                    <i></i><i></i>

                                                    <div class="popup-body">

                                                        <div class="popup-reg-sms-body">

                                                            <div class="popup-reg-sms-header">

                                                                <div class="popup-reg-sms-title">Подтверждение платежа
                                                                </div>

                                                                <div class="popup-reg-sms-close">
                                                                    <a class="cp"
                                                                       onclick="RichFaces.$('emailLinkPopup').hide();"></a>

                                                                </div>

                                                            </div>

                                                            <div class="popup-reg-sms-form">
                                                                <dl>
                                                                    <dt>Пожалуйста, подтвердите этот перевод. Это займёт всего несколько секунд. Мы запрашиваем такие подтверждения очень редко и только при необходимости. Это защищает ваш аккаунт от взлома и кражи средств.<br><br>Не закрывайте это окно.<br><br>Мы отправили письмо на ваш адрес электронной почты.<br><br>Выполните инструкции в письме, вернитесь в это окно и нажмите "Продолжить".</dt>
                                                                </dl>

                                                            </div>

                                                            <div class="button green h38">
                                                                <input id="confirmEmailLinkButtonId"
                                                                       name="confirmEmailLinkButtonId"
                                                                       onclick="RichFaces.ajax(&quot;confirmEmailLinkButtonId&quot;,event,{&quot;incId&quot;:&quot;1&quot;} );return false;"
                                                                       value="Продолжить" type="submit">

                                                            </div>

                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="popup-footer">
                                                    <i></i><i></i><i></i>

                                                </div>

                                            </div>

                                        </span>


                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- <script type="text/javascript">new RichFaces.ui.PopupPanel("emailLinkPopup",{"autosized":true} );</script> -->
                        </div>
                    </div>

                </div>
                <!-- ------------------------------------ -->
            </div>

            <div class="cb">
            </div>

        </div>


        <div class="footer">
            <form id="j_idt1909" name="j_idt1909" method="post" action="/index_page"
                  enctype="application/x-www-form-urlencoded">
                <div class="footer-wrap">

                    <div class="footer-copy">
                        ©

                        <script type="text/javascript">
                            document.write(new Date().getFullYear());
                        </script>
                        &nbsp;
                        <a href="#"
                           onclick="mojarra.jsfcljs(document.getElementById('j_idt1909'),{'j_idt1909:j_idt1911':'j_idt1909:j_idt1911'},'');return false">AdvCash
                        </a>

                    </div>

                    <div class="footer-copy" style="float: right; width:100px;">

                        <a href="https://advcash.com/solutions/developers/" target="_blank">Для разработчиков
                        </a>

                    </div>

                </div>
            </form>

        </div>


        <div id="ajax-status">
            <img src="{{ asset('assets/cab/index_files/ajax_status.gif') }}">

        </div>


        <div class="dn">


        <span id="tx-cache">
            <table id="tx-cache-table">
            </table>


            <span id="tx-cache-show-more">

                <div class="p-main-transactions-loader" align="center">


                    <span id="j_idt2539">


                        <span style="display:none" class="rf-st-start"><img src="{{ asset('assets/cab/index_files/loader.gif') }}" alt="ai">

                        </span>



                        <span class="rf-st-stop">

                        </span>


                    </span>


                </div>


                <div class="p-main-loadtransactions" id="showMoreTransactions">
                    <a href="#" id="j_idt2543" name="j_idt2543"
                       onclick='jsf.util.chain(this,event,"animateNewTx();","RichFaces.ajax(\"j_idt2543\",event,{\"incId\":\"1\",\"begin\":\"blockLink()\",\"status\":\"txLoading\"} )");return false;'
                       class="collapse-all-link">Свернуть все ↑
                    </a>

                    <a class="cursor-default collapse-all-link-fake" onclick="return false;" style="display: none;">Свернуть все
                        ↑
                    </a>


                </div>


            </span>



        </span>


            <span id="new-tx-cache">
            <table id="new-tx-cache-table">
            </table>

        </span>


        </div>

        <div id="confirmPanel" style="visibility: hidden;">

            <div class="rf-pp-shade" id="confirmPanel_shade" style="z-index:100;">
                <button class="rf-pp-btn" id="confirmPanelFirstHref"></button>

            </div>

            <div class="rf-pp-cntr " id="confirmPanel_container" style="position: fixed; z-index:100; ">

                <div class="rf-pp-shdw" id="confirmPanel_shadow" style="opacity: 0.1;">
                </div>

                <div class="rf-pp-cnt-scrlr" id="confirmPanel_content_scroller">

                    <div class="rf-pp-cnt" id="confirmPanel_content">
                        <form id="j_idt3175" name="j_idt3175" method="post" action="/index_page"
                              enctype="application/x-www-form-urlencoded">



                        <span id="confirmPanelGroup">

                            <div class="popup-header">
                                <i></i><i></i><i></i>

                            </div>

                            <div class="popup-content">
                                <i></i> <i></i>

                                <div class="popup-body">

                                    <div class="popup-profile-body">

                                        <div class="fr">

                                            <a class="close-popup" onclick="RichFaces.$('confirmPanel').hide();">
                                            </a>

                                        </div>

                                        <div class="popup-profile-title">Вы уверены, что хотите отменить транзакцию?
                                        </div>

                                        <div class="popup-profile-button">

                                            <div class="button red">
                                                <input id="j_idt3177" name="j_idt3177"
                                                       onclick='RichFaces.ajax("j_idt3177",event,{"incId":"1"} );return false;'
                                                       value="Да" type="submit">

                                            </div>

                                            <div class="button black">
                                                <input onclick="RichFaces.$('confirmPanel').hide();return false;"
                                                       value="Нет" type="submit">

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                            <div class="popup-footer">
                                <i></i><i></i><i></i>

                            </div>

                        </span>

                        </form>

                    </div>

                </div>

            </div>


        </div>


        <div id="tips">

            <div id="tip"><i class="tl"></i><i class="tr"></i><i class="br"></i><i class="bl"></i><i class="arr"></i>
            </div>

        </div>
        {{--<iframe data-product="web_widget" title="No content" tabindex="-1" aria-hidden="true"--}}
                {{--style="width: 0px; height: 0px; border: 0px none; position: absolute; top: -9999px;"--}}
                {{--src="index_files/a_004.html"></iframe>--}}

	@include('partials.iframe')

@endsection